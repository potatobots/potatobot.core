﻿namespace PotatoBot.Core.Data.Database
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using NLog;
    using PotatoBot.Core.Common.Configs.Credentials;

    /// <summary>
    /// Factory Class to create the context.
    /// </summary>
    public class PotatoBotContextFactory : IDesignTimeDbContextFactory<PotatoBotContext>
    {
        /// <summary>
        /// Initializes a builder for the database context.
        /// </summary>
        /// <param name="credentials">The credentials used to log in.</param>
        /// <returns>The configured builder for the context.</returns>
        public static DbContextOptionsBuilder<PotatoBotContext> InitializeBuilder(IBotCredentials credentials)
        {
            var log = LogManager.GetCurrentClassLogger();
            var connectionString = credentials.Db.ConnectionString;

            if (string.IsNullOrEmpty(connectionString))
            {
                log.Error(
                    "Connection string is missing from credentials.json. " +
                    "Add it and restart the program.");
                if (!Console.IsInputRedirected)
                {
                    Console.ReadKey();
                }

                Environment.Exit(3);
            }

            var optionsBuilder = new DbContextOptionsBuilder<PotatoBotContext>();
            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connectionString);

            return optionsBuilder;
        }

        /// <inheritdoc/>
        public PotatoBotContext CreateDbContext(string[] args)
        {
            IBotCredentials credentials = new BotCredentials();
            var optionsBuilder = PotatoBotContextFactory.InitializeBuilder(credentials);
            var context = new PotatoBotContext(optionsBuilder.Options);
            context.Database.SetCommandTimeout(60);
            return context;
        }
    }
}
