﻿namespace PotatoBot.Core.Service
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Discord.Net;
    using Discord.WebSocket;
    using Microsoft.Extensions.DependencyInjection;
    using NLog;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Collections;
    using PotatoBot.Core.Common.Configs.Credentials;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Common.ModuleBehaviors;
    using PotatoBot.Core.Data.Database.Models;
    using PotatoBot.Core.Service.Database;

    /// <summary>
    /// A class with functions to handle commands.
    /// </summary>
    public class CommandHandler : INService
    {
        /// <summary>
        /// Global command cooldown field.
        /// </summary>
        public const int GlobalCommandsCooldown = 750;
        private const float OneThousandth = 1.0f / 1000;

        private readonly DiscordSocketClient client;
        private readonly CommandService commandService;
        private readonly Logger log;
        private readonly IBotCredentials creds;
        private readonly IServiceProvider services;
        private readonly IPotatoBot bot;
        private readonly Timer clearUsersOnShortCooldown;
        private readonly DbService db;
        private readonly IBotConfigProvider bcp;
        private readonly object errorLogLock = new object();
        private IEnumerable<IEarlyBehavior> earlyBehaviors;
        private IEnumerable<IInputTransformer> inputTransformers;
        private IEnumerable<ILateBlocker> lateBlockers;
        private IEnumerable<ILateExecutor> lateExecutors;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandHandler"/> class.
        /// </summary>
        /// <param name="client">The discord socket client used for communication with discord api.</param>
        /// <param name="db">The database service.</param>
        /// <param name="bcp">The bot config provider.</param>
        /// <param name="commandService">The command service.</param>
        /// <param name="credentials">The credentials used by the bot.</param>
        /// <param name="bot">A link to the bot startup class.</param>
        /// <param name="services">The service provider to gain access to all services.</param>
        public CommandHandler(
            DiscordSocketClient client,
            DbService db,
            IBotConfigProvider bcp,
            IPotatoBot bot,
            CommandService commandService,
            IBotCredentials credentials,
            IServiceProvider services)
        {
            this.client = client;
            this.commandService = commandService;
            this.creds = credentials;
            this.bot = bot;
            this.db = db;
            this.bcp = bcp;
            this.services = services;

            this.log = LogManager.GetCurrentClassLogger();

            this.clearUsersOnShortCooldown = new Timer(
                _ =>
            {
                this.UsersOnShortCooldown.Clear();
            },
                null,
                GlobalCommandsCooldown,
                GlobalCommandsCooldown);

            this.DefaultPrefix = bcp.BotConfig.DefaultPrefix;
            this.Prefixes = bot.AllGuildConfigs
                .Where(x => x.Prefix != null)
                .ToDictionary(x => x.GuildId, x => x.Prefix)
                .ToConcurrent();
        }

        /// <summary>
        /// Command executed event.
        /// </summary>
        public event Func<IUserMessage, CommandInfo, Task> CommandExecuted = (arg1, arg2) => { return Task.CompletedTask; };

        /// <summary>
        /// Command errored event.
        /// </summary>
        public event Func<CommandInfo, ITextChannel, string, Task> CommandErrored = (arg1, arg2, arg3) => { return Task.CompletedTask; };

        /// <summary>
        /// On message no trigger event.
        /// </summary>
        public event Func<IUserMessage, Task> OnMessageNoTrigger = arg => { return Task.CompletedTask; };

        /// <summary>
        /// Gets the default prefix.
        /// </summary>
        public string DefaultPrefix { get; private set; }

        /// <summary>
        /// Gets the User message send dictionary.
        /// "userid/msg count".
        /// </summary>
        public ConcurrentDictionary<ulong, uint> UserMessagesSent { get; } = new ConcurrentDictionary<ulong, uint>();

        /// <summary>
        /// Gets the users on short cooldown dictionary.
        /// </summary>
        public ConcurrentHashSet<ulong> UsersOnShortCooldown { get; } = new ConcurrentHashSet<ulong>();

        private ConcurrentDictionary<ulong, string> Prefixes { get; } = new ConcurrentDictionary<ulong, string>();

        /// <summary>
        /// Gets the prefix for a specific guild.
        /// </summary>
        /// <param name="guild">The guild used for retrieving the prefix.</param>
        /// <returns>A string containging the prefix.</returns>
        public string GetPrefix(IGuild guild) => this.GetPrefix(guild?.Id);

        /// <summary>
        /// Gets the prefix for a specific guild identified by id.
        /// </summary>
        /// <param name="id">The id of the guild.</param>
        /// <returns>A string containging the prefix.</returns>
        public string GetPrefix(ulong? id)
        {
            if (id == null || !this.Prefixes.TryGetValue(id.Value, out var prefix))
            {
                return this.DefaultPrefix;
            }

            return prefix;
        }

        /// <summary>
        /// Sets the default prefix.
        /// </summary>
        /// <param name="prefix">The prefix which replaces the current default prefix.</param>
        /// <returns>The new default prefix.</returns>
        public string SetDefaultPrefix(string prefix)
        {
            if (string.IsNullOrWhiteSpace(prefix))
            {
                throw new ArgumentNullException(nameof(prefix));
            }

            using (var uow = this.db.GetDbContext())
            {
                uow.BotConfig.GetOrCreate(set => set).DefaultPrefix = prefix;
                uow.SaveChanges();
            }

            return this.DefaultPrefix = prefix;
        }

        /// <summary>
        /// Sets a prefix for a certain guild.
        /// </summary>
        /// <param name="guild">The guild the new prefix is for.</param>
        /// <param name="prefix">The new prefix for the guild.</param>
        /// <returns>The newly set prefix.</returns>
        public string SetPrefix(IGuild guild, string prefix)
        {
            if (string.IsNullOrWhiteSpace(prefix))
            {
                throw new ArgumentNullException(nameof(prefix));
            }

            if (guild == null)
            {
                throw new ArgumentNullException(nameof(guild));
            }

            using (var uow = this.db.GetDbContext())
            {
                var gc = uow.GuildConfigs.ForId(guild.Id, set => set);
                gc.Prefix = prefix;
                uow.SaveChanges();
            }

            this.Prefixes.AddOrUpdate(guild.Id, prefix, (key, old) => prefix);

            return prefix;
        }

        /// <summary>
        /// Adds services to the command handler.
        /// </summary>
        /// <param name="services">The service provider.</param>
        public void AddServices(IServiceCollection services)
        {
            this.lateBlockers = services.Where(x => x.ImplementationType?.GetInterfaces().Contains(typeof(ILateBlocker)) ?? false)
                .Select(x => services.BuildServiceProvider().GetService(x.ImplementationType) as ILateBlocker)
                .ToArray();

            this.lateExecutors = services.Where(x => x.ImplementationType?.GetInterfaces().Contains(typeof(ILateExecutor)) ?? false)
                .Select(x => services.BuildServiceProvider().GetService(x.ImplementationType) as ILateExecutor)
                .ToArray();

            this.inputTransformers = services.Where(x => x.ImplementationType?.GetInterfaces().Contains(typeof(IInputTransformer)) ?? false)
                .Select(x => services.BuildServiceProvider().GetService(x.ImplementationType) as IInputTransformer)
                .ToArray();

            this.earlyBehaviors = services.Where(x => x.ImplementationType?.GetInterfaces().Contains(typeof(IEarlyBehavior)) ?? false)
                .Select(x => services.BuildServiceProvider().GetService(x.ImplementationType) as IEarlyBehavior)
                .ToArray();
        }

        /// <summary>
        /// Execute a command externally.
        /// </summary>
        /// <param name="guildId">The guild id needed for executing the command.</param>
        /// <param name="channelId">The channel id where the command needs to be executed.</param>
        /// <param name="commandText">The command itself.</param>
        /// <returns>A <see cref="Task"/> for this async process.</returns>
        public async Task ExecuteExternal(ulong? guildId, ulong channelId, string commandText)
        {
            if (guildId != null)
            {
                var guild = this.client.GetGuild(guildId.Value);
                if (!(guild?.GetChannel(channelId) is SocketTextChannel channel))
                {
                    this.log.Warn("Channel for external execution not found.");
                    return;
                }

                try
                {
                    IUserMessage msg = await channel.SendMessageAsync(commandText).ConfigureAwait(false);
                    msg = (IUserMessage)await channel.GetMessageAsync(msg.Id).ConfigureAwait(false);
                    await this.TryRunCommand(guild, channel, msg).ConfigureAwait(false);

                    // msg.DeleteAfter(5);
                }
                catch (Exception)
                {
                    // ignored.
                }
            }
        }

        /// <summary>
        /// Start the command handling service.
        /// </summary>
        /// <returns>A <see cref="Task"/> for this async process.</returns>
        public Task StartHandling()
        {
            this.client.MessageReceived += (msg) =>
            {
                Task.Run(() => this.MessageReceivedHandler(msg));
                return Task.CompletedTask;
            };
            return Task.CompletedTask;
        }

        /// <summary>
        /// Try to run a command.
        /// </summary>
        /// <param name="guild">The guild where the command needs to be executed.</param>
        /// <param name="channel">The channel of the guild where the command is executed.</param>
        /// <param name="usrMsg">The command that needs to be executed.</param>
        /// <returns>A <see cref="Task"/> of the async process.</returns>
        public async Task TryRunCommand(SocketGuild guild, ISocketMessageChannel channel, IUserMessage usrMsg)
        {
            var execTime = Environment.TickCount;

            // its nice to have early blockers and early blocking executors separate, but
            // i could also have one interface with priorities, and just put early blockers on
            // highest priority. :thinking:
            foreach (var beh in this.earlyBehaviors)
            {
                if (await beh.RunBehavior(this.client, guild, usrMsg).ConfigureAwait(false))
                {
                    if (beh.BehaviorType == ModuleBehaviorType.Blocker)
                    {
                        this.log.Info("Blocked User: [{0}] Message: [{1}] Service: [{2}]", usrMsg.Author, usrMsg.Content, beh.GetType().Name);
                    }
                    else if (beh.BehaviorType == ModuleBehaviorType.Executor)
                    {
                        this.log.Info("User [{0}] executed [{1}] in [{2}]", usrMsg.Author, usrMsg.Content, beh.GetType().Name);
                    }

                    return;
                }
            }

            var exec2 = Environment.TickCount - execTime;

            string messageContent = usrMsg.Content;
            foreach (var exec in this.inputTransformers)
            {
                string newContent;
                if ((newContent = await exec.TransformInput(guild, usrMsg.Channel, usrMsg.Author, messageContent).ConfigureAwait(false)) != messageContent.ToLowerInvariant())
                {
                    messageContent = newContent;
                    break;
                }
            }

            var prefix = this.GetPrefix(guild?.Id);
            var isPrefixCommand = messageContent.StartsWith(".prefix", StringComparison.InvariantCultureIgnoreCase);

            // execute the command and measure the time it took
            if (messageContent.StartsWith(prefix, StringComparison.InvariantCulture) || isPrefixCommand)
            {
                var (success, error, info) = await this.ExecuteCommandAsync(
                    new CommandContext(this.client, usrMsg),
                    messageContent,
                    isPrefixCommand ? 1 : prefix.Length,
                    this.services,
                    MultiMatchHandling.Best)
                    .ConfigureAwait(false);
                execTime = Environment.TickCount - execTime;

                if (success)
                {
                    await this.LogSuccessfulExecution(usrMsg, channel as ITextChannel, exec2, execTime).ConfigureAwait(false);
                    await this.CommandExecuted(usrMsg, info).ConfigureAwait(false);
                    return;
                }
                else if (error != null)
                {
                    this.LogErroredExecution(error, usrMsg, channel as ITextChannel, exec2, execTime);
                    if (guild != null)
                    {
                        await this.CommandErrored(info, channel as ITextChannel, error).ConfigureAwait(false);
                    }
                }
            }
            else
            {
                await this.OnMessageNoTrigger(usrMsg).ConfigureAwait(false);
            }

            foreach (var exec in this.lateExecutors)
            {
                await exec.LateExecute(this.client, guild, usrMsg).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Executes a command async.
        /// </summary>
        /// <param name="context">The command context.</param>
        /// <param name="input">The command input.</param>
        /// <param name="argPos">Argument position.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="multiMatchHandling">MultiMatchHandling.</param>
        /// <returns>A <see cref="Task"/> of the async process.</returns>
        public Task<(bool Success, string Error, CommandInfo Info)> ExecuteCommandAsync(CommandContext context, string input, int argPos, IServiceProvider serviceProvider, MultiMatchHandling multiMatchHandling = MultiMatchHandling.Exception)
            => this.ExecuteCommand(context, input.Substring(argPos), serviceProvider, multiMatchHandling);

        private async Task<(bool Success, string Error, CommandInfo Info)> ExecuteCommand(CommandContext context, string input, IServiceProvider services, MultiMatchHandling multiMatchHandling = MultiMatchHandling.Exception)
        {
            var searchResult = this.commandService.Search(context, input);
            if (!searchResult.IsSuccess)
            {
                return (false, null, null);
            }

            var commands = searchResult.Commands;
            var preconditionResults = new Dictionary<CommandMatch, PreconditionResult>();

            foreach (var match in commands)
            {
                preconditionResults[match] = await match.Command.CheckPreconditionsAsync(context, services).ConfigureAwait(false);
            }

            var successfulPreconditions = preconditionResults
                .Where(x => x.Value.IsSuccess)
                .ToArray();

            if (successfulPreconditions.Length == 0)
            {
                // All preconditions failed, return the one from the highest priority command
                var bestCandidate = preconditionResults
                    .OrderByDescending(x => x.Key.Command.Priority)
                    .FirstOrDefault(x => !x.Value.IsSuccess);
                return (false, bestCandidate.Value.ErrorReason, commands[0].Command);
            }

            var parseResultsDict = new Dictionary<CommandMatch, ParseResult>();
            foreach (var pair in successfulPreconditions)
            {
                var parseResult = await pair.Key.ParseAsync(context, searchResult, pair.Value, services).ConfigureAwait(false);

                if (parseResult.Error == CommandError.MultipleMatches)
                {
                    IReadOnlyList<TypeReaderValue> argList, paramList;
                    switch (multiMatchHandling)
                    {
                        case MultiMatchHandling.Best:
                            argList = parseResult.ArgValues.Select(x => x.Values.OrderByDescending(y => y.Score).First()).ToImmutableArray();
                            paramList = parseResult.ParamValues.Select(x => x.Values.OrderByDescending(y => y.Score).First()).ToImmutableArray();
                            parseResult = ParseResult.FromSuccess(argList, paramList);
                            break;
                    }
                }

                parseResultsDict[pair.Key] = parseResult;
            }

            // Calculates the 'score' of a command given a parse result
            float CalculateScore(CommandMatch match, ParseResult parseResult)
            {
                float argValuesScore = 0, paramValuesScore = 0;

                if (match.Command.Parameters.Count > 0)
                {
                    var argValuesSum = parseResult.ArgValues?.Sum(x => x.Values.OrderByDescending(y => y.Score).FirstOrDefault().Score) ?? 0;
                    var paramValuesSum = parseResult.ParamValues?.Sum(x => x.Values.OrderByDescending(y => y.Score).FirstOrDefault().Score) ?? 0;

                    argValuesScore = argValuesSum / match.Command.Parameters.Count;
                    paramValuesScore = paramValuesSum / match.Command.Parameters.Count;
                }

                var totalArgsScore = (argValuesScore + paramValuesScore) / 2;
                return match.Command.Priority + (totalArgsScore * 0.99f);
            }

            // Order the parse results by their score so that we choose the most likely result to execute
            var parseResults = parseResultsDict
                .OrderByDescending(x => CalculateScore(x.Key, x.Value));

            var successfulParses = parseResults
                .Where(x => x.Value.IsSuccess)
                .ToArray();

            if (successfulParses.Length == 0)
            {
                // All parses failed, return the one from the highest priority command, using score as a tie breaker
                var bestMatch = parseResults
                    .FirstOrDefault(x => !x.Value.IsSuccess);
                return (false, bestMatch.Value.ErrorReason, commands[0].Command);
            }

            var cmd = successfulParses[0].Key.Command;

            // Bot will ignore commands which are ran more often than what specified by
            // GlobalCommandsCooldown constant (miliseconds)
            if (!this.UsersOnShortCooldown.Add(context.Message.Author.Id))
            {
                return (false, null, cmd);
            }

            // return SearchResult.FromError(CommandError.Exception, "You are on a global cooldown.");
            var commandName = cmd.Aliases.First();
            foreach (var exec in this.lateBlockers)
            {
                if (await exec.TryBlockLate(this.client, context.Message, context.Guild, context.Channel, context.User, cmd.Module.GetTopLevelModule().Name, commandName).ConfigureAwait(false))
                {
                    this.log.Info("Late blocking User [{0}] Command: [{1}] in [{2}]", context.User, commandName, exec.GetType().Name);
                    return (false, null, cmd);
                }
            }

            // If we get this far, at least one parse was successful. Execute the most likely overload.
            var chosenOverload = successfulParses[0];
            var execResult = (ExecuteResult)await chosenOverload.Key.ExecuteAsync(context, chosenOverload.Value, services).ConfigureAwait(false);

            if (execResult.Exception != null && (!(execResult.Exception is HttpException he) || he.DiscordCode != 50013))
            {
                lock (this.errorLogLock)
                {
                    var now = DateTime.Now;
                    File.AppendAllText(
                        $"./command_errors_{now:yyyy-MM-dd}.txt",
                        $"[{now:HH:mm-yyyy-MM-dd}]" + Environment.NewLine + execResult.Exception.ToString() + Environment.NewLine + "------" + Environment.NewLine);
                    this.log.Warn(execResult.Exception);
                }
            }

            return (true, null, cmd);
        }

        /// <summary>
        /// Logs a successful execution.
        /// </summary>
        /// <param name="usrMsg">Represents a generic message send by a user.</param>
        /// <param name="channel">Represents a generic channel in a guild capable of sending and receiving messages.</param>
        /// <param name="execPoints">Parameters for the execution.</param>
        /// <returns>A <see cref="Task"/> for the log process.</returns>
        private Task LogSuccessfulExecution(IUserMessage usrMsg, ITextChannel channel, params int[] execPoints)
        {
            if (this.bcp.BotConfig.ConsoleOutputType == ConsoleOutputType.Normal)
            {
                this.log.Info(
                    $"Command Executed after " + string.Join("/", execPoints.Select(x => (x * OneThousandth).ToString("F3"))) + "s\n\t" +
                        "User: {0}\n\t" +
                        "Server: {1}\n\t" +
                        "Channel: {2}\n\t" +
                        "Message: {3}",
                    usrMsg.Author + " [" + usrMsg.Author.Id + "]", // {0}
                    channel == null ? "PRIVATE" : channel.Guild.Name + " [" + channel.Guild.Id + "]", // {1}
                    channel == null ? "PRIVATE" : channel.Name + " [" + channel.Id + "]", // {2}
                    usrMsg.Content); // {3}
            }
            else
            {
                this.log.Info(
                    "Succ | g:{0} | c: {1} | u: {2} | msg: {3}",
                    channel?.Guild.Id.ToString() ?? "-",
                    channel?.Id.ToString() ?? "-",
                    usrMsg.Author.Id,
                    usrMsg.Content.TrimTo(10));
            }

            return Task.CompletedTask;
        }

        private void LogErroredExecution(string errorMessage, IUserMessage usrMsg, ITextChannel channel, params int[] execPoints)
        {
            if (this.bcp.BotConfig.ConsoleOutputType == ConsoleOutputType.Normal)
            {
                this.log.Warn(
                    $"Command Errored after " + string.Join("/", execPoints.Select(x => (x * OneThousandth).ToString("F3"))) + "s\n\t" +
                            "User: {0}\n\t" +
                            "Server: {1}\n\t" +
                            "Channel: {2}\n\t" +
                            "Message: {3}\n\t" +
                            "Error: {4}",
                    usrMsg.Author + " [" + usrMsg.Author.Id + "]", // {0}
                    channel == null ? "PRIVATE" : channel.Guild.Name + " [" + channel.Guild.Id + "]", // {1}
                    channel == null ? "PRIVATE" : channel.Name + " [" + channel.Id + "]", // {2}
                    usrMsg.Content, // {3}
                    errorMessage);

                // exec.Result.ErrorReason // {4}
            }
            else
            {
                this.log.Warn(
                    "Err | g:{0} | c: {1} | u: {2} | msg: {3}\n\tErr: {4}",
                    channel?.Guild.Id.ToString() ?? "-",
                    channel?.Id.ToString() ?? "-",
                    usrMsg.Author.Id,
                    usrMsg.Content.TrimTo(10),
                    errorMessage);
            }
        }

        private async Task MessageReceivedHandler(SocketMessage msg)
        {
            try
            {
                // no bots, wait until bot connected and initialized
                if (msg.Author.IsBot || !this.bot.Ready.Task.IsCompleted)
                {
                    return;
                }

                if (!(msg is SocketUserMessage usrMsg))
                {
                    return;
                }

                var channel = msg.Channel;
                var guild = (msg.Channel as SocketTextChannel)?.Guild;

                await this.TryRunCommand(guild, channel, usrMsg).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this.log.Warn("Error in CommandHandler");
                this.log.Warn(ex);
                if (ex.InnerException != null)
                {
                    this.log.Warn("Inner Exception of the error in CommandHandler");
                    this.log.Warn(ex.InnerException);
                }
            }
        }
    }
}