﻿namespace PotatoBot.Core.Service
{
    using System;
    using PotatoBot.Core.Common;

    /// <summary>
    /// An interface for stats.
    /// </summary>
    public interface IStatsService : INService
    {
        /// <summary>
        /// Gets the bot version.
        /// </summary>
        string BotVersion { get; }

        /// <summary>
        /// Gets the author.
        /// </summary>
        string Author { get; }

        /// <summary>
        /// Gets the ran commands.
        /// </summary>
        long CommandsRan { get; }

        /// <summary>
        /// Gets the heap.
        /// </summary>
        string Heap { get; }

        /// <summary>
        /// Gets the library.
        /// </summary>
        string Library { get; }

        /// <summary>
        /// Gets the message counter stat.
        /// </summary>
        long MessageCounter { get; }

        /// <summary>
        /// Gets the message per second stat.
        /// </summary>
        double MessagesPerSecond { get; }

        /// <summary>
        /// Gets the text channels stat.
        /// </summary>
        long TextChannels { get; }

        /// <summary>
        /// Gets the voice channel stat.
        /// </summary>
        long VoiceChannels { get; }

        /// <summary>
        /// Get the up time.
        /// </summary>
        /// <returns>The up time as a <see cref="TimeSpan"/>.</returns>
        TimeSpan GetUptime();

        /// <summary>
        /// Gets the up time as a string.
        /// </summary>
        /// <param name="separator">A separator for the time span.</param>
        /// <returns>The up time as a string.</returns>
        string GetUptimeString(string separator = ", ");

        /// <summary>
        /// Initializes the stat service.
        /// </summary>
        void Initialize();
    }
}
