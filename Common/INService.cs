﻿namespace PotatoBot.Core.Common
{
    using System.Threading.Tasks;

    /// <summary>
    /// All services must implement this interface in order to be auto-discovered by the DI system.
    /// </summary>
    public interface INService
    {
    }

    /// <summary>
    /// All services which require cleanup after they are unloaded must implement this interface.
    /// </summary>
    public interface IUnloadableService
    {
        /// <summary>
        /// Unload the service when a cleanup is requested.
        /// </summary>
        /// <returns>A <see cref="Task"/> for the cleanup.</returns>
        Task Unload();
    }
}
