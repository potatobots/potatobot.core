﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System.Collections.Generic;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Base repository interface.
    /// </summary>
    /// <typeparam name="T">Generic type indicating the repository.</typeparam>
    public interface IRepository<T>
        where T : DbEntity
    {
        /// <summary>
        /// Get generic type by id.
        /// </summary>
        /// <param name="id">The id from the type.</param>
        /// <returns>The generic type identified by <paramref name="id"/>.</returns>
        T GetById(int id);

        /// <summary>
        /// Gets a list of all <typeparamref name="T"/>.
        /// </summary>
        /// <returns>A list with <typeparamref name="T"/>.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Add <typeparamref name="T"/> to a list.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        void Add(T obj);

        /// <summary>
        /// Add a list of <typeparamref name="T"/> to the repo list.
        /// </summary>
        /// <param name="objList">A list of <typeparamref name="T"/>.</param>
        void AddRange(params T[] objList);

        /// <summary>
        /// Remove <typeparamref name="T"/> identified by id.
        /// </summary>
        /// <param name="id">The id of <typeparamref name="T"/>.</param>
        void Remove(int id);

        /// <summary>
        /// Remove <typeparamref name="T"/>.
        /// </summary>
        /// <param name="obj">The <typeparamref name="T"/> to remove.</param>
        void Remove(T obj);

        /// <summary>
        /// Remove a list of <typeparamref name="T"/>.
        /// </summary>
        /// <param name="objList">The list that needs to be removed.</param>
        void RemoveRange(params T[] objList);

        /// <summary>
        /// Update <typeparamref name="T"/>.
        /// </summary>
        /// <param name="obj">The <typeparamref name="T"/> that needs to be updated.</param>
        void Update(T obj);

        /// <summary>
        /// Update a list of <typeparamref name="T"/>.
        /// </summary>
        /// <param name="objList">The list with <typeparamref name="T"/> items that needs to be updated.</param>
        void UpdateRange(params T[] objList);
    }
}
