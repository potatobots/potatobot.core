﻿namespace PotatoBot.Core.Common.Configs.Database
{
    /// <summary>
    /// A Class for configuring a Database.
    /// </summary>
    public class DbConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbConfig"/> class.
        /// </summary>
        /// <param name="type">The type of the db.</param>
        /// <param name="connectionString">The connectionstring to the db.</param>
        public DbConfig(string type, string connectionString)
        {
            this.Type = type;
            this.ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets the DB Type.
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        public string ConnectionString { get; }
    }
}
