﻿namespace PotatoBot.Core.Common
{
    using System;
    using System.Collections.Immutable;
    using System.Threading.Tasks;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Interface for the potato bot.
    /// </summary>
    public interface IPotatoBot
    {
        /// <summary>
        /// JoinedGuild event.
        /// </summary>
        event Func<GuildConfig, Task> JoinedGuild;

        /// <summary>
        /// Gets a list containing all guild configs.
        /// </summary>
        ImmutableArray<GuildConfig> AllGuildConfigs { get; }

        /// <summary>
        /// Gets or sets the Task completion source.
        /// </summary>
        TaskCompletionSource<bool> Ready { get; set; }
    }
}
