﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;
    using System.Runtime.CompilerServices;
    using Discord.Commands;
    using Newtonsoft.Json;

    /// <summary>
    /// Usage attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class UsageAttribute : RemarksAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsageAttribute"/> class.
        /// </summary>
        /// <param name="usages">The different usages of the command.</param>
        public UsageAttribute(string[] usages)
            : base(GetUsage(usages))
        {
        }

        /// <summary>
        /// Get a usage based on the member name.
        /// </summary>
        /// <param name="usages">The different usages of the command.</param>
        /// <returns>A string containing the usage of the member.</returns>
        public static string GetUsage(string[] usages)
        {
            return JsonConvert.SerializeObject(usages);
        }
    }
}
