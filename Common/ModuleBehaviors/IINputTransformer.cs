﻿namespace PotatoBot.Core.Common.ModuleBehaviors
{
    using System.Threading.Tasks;
    using Discord;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public interface IInputTransformer
    {
        Task<string> TransformInput(IGuild guild, IMessageChannel channel, IUser user, string input);
    }
}
