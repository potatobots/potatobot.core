﻿namespace PotatoBot.Core.Common
{
    using CommandLine;

    /// <summary>
    /// Static class for parsing commands.
    /// </summary>
    public static class OptionsParser
    {
        /// <summary>
        /// Parse the command.
        /// </summary>
        /// <typeparam name="T">A <see cref="IPotatoBotCommandOptions"/> interface implementation.</typeparam>
        /// <param name="options">The generic type <typeparamref name="T"/> used for parsing.</param>
        /// <param name="args">The arguments given with the command.</param>
        /// <returns>A tuple with the <typeparamref name="T"/> and a boolean indicating if the parsing was successful.</returns>
        public static (T, bool) ParseFrom<T>(T options, string[] args)
            where T : IPotatoBotCommandOptions
        {
            using (var p = new Parser(x =>
            {
                x.HelpWriter = null;
            }))
            {
                var res = p.ParseArguments<T>(args);
                options = res.MapResult(x => x, x => options);
                options.NormalizeOptions();
                return (options, res.Tag == ParserResultType.Parsed);
            }
        }
    }
}
