﻿namespace PotatoBot.Core.Common.ShardCom
{
    using System;
    using Discord;

    /// <summary>
    /// A Class for shard com messages.
    /// </summary>
    public class ShardComMessage
    {
        /// <summary>
        /// Gets or sets the shard id.
        /// </summary>
        public int ShardId { get; set; }

        /// <summary>
        /// Gets or sets the connection state.
        /// </summary>
        public ConnectionState ConnectionState { get; set; }

        /// <summary>
        /// Gets or sets the guilds.
        /// </summary>
        public int Guilds { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Clone this com message to a new instance of <see cref="ShardComMessage"/>.
        /// </summary>
        /// <returns>A new shard com message.</returns>
        public ShardComMessage Clone() =>
            new ShardComMessage
            {
                ShardId = this.ShardId,
                ConnectionState = this.ConnectionState,
                Guilds = this.Guilds,
                Time = this.Time,
            };
    }
}
