﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;

    /// <summary>
    /// Options Attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PotatoBotOptionsAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotOptionsAttribute"/> class.
        /// </summary>
        /// <param name="t">The type of the attribute.</param>
        public PotatoBotOptionsAttribute(Type t)
        {
            this.OptionType = t;
        }

        /// <summary>
        /// Gets or sets the option type.
        /// </summary>
        public Type OptionType { get; set; }
    }
}
