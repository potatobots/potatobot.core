﻿namespace PotatoBot.Core.Common
{
    using System;
    using System.Security.Cryptography;

    /// <summary>
    /// A class conainting methods for random numbers.
    /// </summary>
    public class PotatoBotRandom : Random
    {
        private readonly RandomNumberGenerator rng;

        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotRandom"/> class.
        /// </summary>
        public PotatoBotRandom()
            : base()
        {
            this.rng = RandomNumberGenerator.Create();
        }

        /// <inheritdoc/>
        public override int Next()
        {
            var bytes = new byte[sizeof(int)];
            this.rng.GetBytes(bytes);
            return Math.Abs(BitConverter.ToInt32(bytes, 0));
        }

        /// <inheritdoc/>
        public override int Next(int maxValue)
        {
            if (maxValue <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxValue));
            }

            var bytes = new byte[sizeof(int)];
            this.rng.GetBytes(bytes);
            return Math.Abs(BitConverter.ToInt32(bytes, 0)) % maxValue;
        }

        /// <inheritdoc/>
        public override int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(maxValue));
            }

            if (minValue == maxValue)
            {
                return minValue;
            }

            var bytes = new byte[sizeof(int)];
            this.rng.GetBytes(bytes);
            var sign = Math.Sign(BitConverter.ToInt32(bytes, 0));
            return ((sign * BitConverter.ToInt32(bytes, 0)) % (maxValue - minValue)) + minValue;
        }

        /// <summary>
        /// Returns a random long that is within the specified range.
        /// </summary>
        /// <param name="minValue">The minimum value for the random long.</param>
        /// <param name="maxValue">The maximum value for the random long.</param>
        /// <returns>A 64-bit signed integer greater than or equal to the <paramref name="minValue"/> and less than the <paramref name="maxValue"/>.</returns>
        public long NextLong(long minValue, long maxValue)
        {
            if (minValue > maxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(maxValue));
            }

            if (minValue == maxValue)
            {
                return minValue;
            }

            var bytes = new byte[sizeof(long)];
            this.rng.GetBytes(bytes);
            var sign = Math.Sign(BitConverter.ToInt64(bytes, 0));
            return ((sign * BitConverter.ToInt64(bytes, 0)) % (maxValue - minValue)) + minValue;
        }

        /// <inheritdoc/>
        public override void NextBytes(byte[] buffer)
        {
            this.rng.GetBytes(buffer);
        }

        /// <inheritdoc/>
        public override double NextDouble()
        {
            var bytes = new byte[sizeof(double)];
            this.rng.GetBytes(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }

        /// <inheritdoc/>
        protected override double Sample()
        {
            var bytes = new byte[sizeof(double)];
            this.rng.GetBytes(bytes);
            return Math.Abs((BitConverter.ToDouble(bytes, 0) / double.MaxValue) + 1);
        }
    }
}
