﻿namespace PotatoBot.Core.Modules.Help.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CommandLine;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using NLog;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.CREmbed;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Common.ModuleBehaviors;
    using PotatoBot.Core.Service;
    using PotatoBot.Core.Service.Attributes;

    /// <summary>
    /// A service class for the help commands.
    /// </summary>
    public class HelpService : ILateExecutor, INService
    {
        private readonly IBotConfigProvider bc;
        private readonly CommandHandler ch;
        private readonly PotatoBotStrings strings;
        private readonly Logger log;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpService"/> class.
        /// </summary>
        /// <param name="bc">The bot config provider.</param>
        /// <param name="ch">The command handler.</param>
        /// <param name="strings">The potato bot strings class.</param>
        public HelpService(IBotConfigProvider bc, CommandHandler ch, PotatoBotStrings strings)
        {
            this.bc = bc;
            this.ch = ch;
            this.strings = strings;
            this.log = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Gets the command option help.
        /// </summary>
        /// <param name="opt">The option you want help about.</param>
        /// <returns>The help of the option.</returns>
        public static string GetCommandOptionHelp(Type opt)
        {
            var strs = GetCommandOptionHelpList(opt);

            return string.Join("\n", strs);
        }

        /// <summary>
        /// Gets command option help list.
        /// </summary>
        /// <param name="opt">The option you need the help list about.</param>
        /// <returns>A list containing the help of the option.</returns>
        public static List<string> GetCommandOptionHelpList(Type opt)
        {
            var strs = opt.GetProperties()
                   .Select(x => x.GetCustomAttributes(true).FirstOrDefault(a => a is OptionAttribute))
                   .Where(x => x != null)
                   .Cast<OptionAttribute>()
                   .Select(x =>
                   {
                       var toReturn = $"`--{x.LongName}`";

                       if (!string.IsNullOrWhiteSpace(x.ShortName))
                       {
                           toReturn += $" (`-{x.ShortName}`)";
                       }

                       toReturn += $"   {x.HelpText}  ";
                       return toReturn;
                   })
                   .ToList();

            return strs;
        }

        /// <summary>
        /// Get the command requirements.
        /// </summary>
        /// <param name="cmd">The command in question.</param>
        /// <returns>The list of requirements needed for the usage of the command.</returns>
        public static string[] GetCommandRequirements(CommandInfo cmd) =>
            cmd.Preconditions
                  .Where(ca => ca is OwnerOnlyAttribute || ca is RequireUserPermissionAttribute)
                  .Select(ca =>
                  {
                      if (ca is OwnerOnlyAttribute)
                      {
                          return "Bot Owner Only";
                      }

                      var cau = (RequireUserPermissionAttribute)ca;
                      if (cau.GuildPermission != null)
                      {
                          return (cau.GuildPermission.ToString() + " Server Permission")
                                       .Replace("Guild", "Server", StringComparison.InvariantCulture);
                      }

                      return (cau.ChannelPermission + " Channel Permission")
                                       .Replace("Guild", "Server", StringComparison.InvariantCulture);
                  })
                .ToArray();

        /// <inheritdoc/>
        public Task LateExecute(DiscordSocketClient client, IGuild guild, IUserMessage msg)
        {
            try
            {
                if (guild == null)
                {
                    if (CREmbed.TryParse(this.bc.BotConfig.DmHelpString, out var embed))
                    {
                        return msg.Channel.EmbedAsync(embed.ToEmbed(), embed.PlainText?.SanitizeMentions() ?? string.Empty);
                    }

                    return msg.Channel.SendMessageAsync(this.bc.BotConfig.DmHelpString);
                }
            }
            catch (Exception ex)
            {
                this.log.Warn(ex);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Gets the command help.
        /// </summary>
        /// <param name="com">Discord command info class.</param>
        /// <param name="guild">The guild the command is posted in.</param>
        /// <returns>An <see cref="EmbedBuilder"/> with information about the command.</returns>
        public EmbedBuilder GetCommandHelp(CommandInfo com, IGuild guild)
        {
            var prefix = this.ch.GetPrefix(guild);

            var str = string.Format("**`{0}`**", prefix + com.Aliases.First());
            var alias = com.Aliases.Skip(1).FirstOrDefault();
            if (alias != null)
            {
                str += string.Format(" **/ `{0}`**", prefix + alias);
            }

            var em = new EmbedBuilder()
                .AddField(fb => fb.WithName(str)
                    .WithValue($"{com.RealSummary(prefix)}")
                    .WithIsInline(true));

            var reqs = GetCommandRequirements(com);
            if (reqs.Any())
            {
                em.AddField(
                    this.GetText("requires", guild),
                    string.Join("\n", reqs));
            }

            em
                .AddField(fb => fb.WithName(this.GetText("usage", guild))
                    .WithValue(com.RealRemarks(prefix))
                    .WithIsInline(false))
                .WithFooter(efb => efb.WithText(this.GetText("module", guild, com.Module.GetTopLevelModule().Name)))
                .WithColor(PotatoBotStatics.OkColor);

            var opt = ((PotatoBotOptionsAttribute)com.Attributes.FirstOrDefault(x => x is PotatoBotOptionsAttribute))?.OptionType;
            if (opt != null)
            {
                var hs = GetCommandOptionHelp(opt);
                if (!string.IsNullOrWhiteSpace(hs))
                {
                    em.AddField(this.GetText("options", guild), hs, false);
                }
            }

            return em;
        }

        private string GetText(string text, IGuild guild, params object[] replacements) =>
            this.strings.GetText(text, guild?.Id, "Help".ToLowerInvariant(), replacements);
    }
}
