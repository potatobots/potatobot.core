﻿namespace PotatoBot.Core.Modules.Help
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Newtonsoft.Json;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Configs.Credentials;
    using PotatoBot.Core.Common.CREmbed;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Modules.Common.Replacement;
    using PotatoBot.Core.Modules.Help.Common;
    using PotatoBot.Core.Modules.Help.Services;
    using PotatoBot.Core.Modules.Permissions.Services;
    using PotatoBot.Core.Service.Attributes;

    /// <summary>
    /// A class defining all available help related commands.
    /// </summary>
    public class Help : Module<HelpService>
    {
        private readonly IBotCredentials creds;
        private readonly CommandService cmds;
        private readonly GlobalPermissionService perms;
        private readonly IServiceProvider services;

        /// <summary>
        /// Initializes a new instance of the <see cref="Help"/> class.
        /// </summary>
        /// <param name="creds">The bot credentials file.</param>
        /// <param name="perms">The permissions service.</param>
        /// <param name="cmds">The command service.</param>
        /// <param name="services">The service provider.</param>
        public Help(
                    IBotCredentials creds,
                    GlobalPermissionService perms,
                    CommandService cmds,
                    IServiceProvider services)
        {
            this.creds = creds;
            this.cmds = cmds;
            this.perms = perms;
            this.services = services;
        }

        /// <summary>
        /// Gets a help string embeded with the Discord embed builder.
        /// </summary>
        /// <returns>The help string within a embed builder.</returns>
        public EmbedBuilder GetHelpStringEmbed()
        {
            var r = new ReplacementBuilder()
                .WithDefault(this.Context)
                .WithOverride("{0}", () => this.creds.ClientId.ToString())
                .WithOverride("{1}", () => this.Prefix)
                .Build();

            if (!CREmbed.TryParse(this.Bc.BotConfig.HelpString, out var embed))
            {
                return new EmbedBuilder().WithOkColor()
                    .WithDescription(string.Format(this.Bc.BotConfig.HelpString, this.creds.ClientId, this.Prefix));
            }

            r.Replace(embed);

            return embed.ToEmbed();
        }

        /// <summary>
        /// Gets help about the modules.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [PotatoBotCommand("modules")]
        [Usage(new string[] { "{0}modules" })]
        [Description("Lists all bot modules.")]
        [Aliases("mdls")]
        public async Task Modules()
        {
            var embed = new EmbedBuilder().WithOkColor()
                .WithFooter(efb => efb.WithText("ℹ️" + this.GetText("modules_footer", this.Prefix)))
                .WithTitle(this.GetText("list_of_modules"))
                .WithDescription(string.Join(
                    "\n",
                    this.cmds.Modules.GroupBy(m => m.GetTopLevelModule())
                                         .Where(m => !this.perms.BlockedModules.Contains(m.Key.Name.ToLowerInvariant()))
                                         .Select(m => "• " + m.Key.Name)
                                         .OrderBy(s => s)));
            await this.Ctx.Channel.EmbedAsync(embed).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets help about the commands.
        /// </summary>
        /// <param name="module">Specify a module for information about the module commands.</param>
        /// <param name="args">Arguments given with the command.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [PotatoBotCommand("commands")]
        [Usage(new string[] { "{0}cmds Admin", "{0}cmds Admin --view 1" })]
        [Description("List all of the bot's commands from a certain module. You can either specify the full name or only the first few letters of the module name.")]
        [Aliases("cmds")]
        [PotatoBotOptions(typeof(CommandsOptions))]
        public async Task Commands(string module = null, params string[] args)
        {
            var channel = this.Ctx.Channel;

            var (opts, _) = OptionsParser.ParseFrom(new CommandsOptions(), args);

            module = module?.Trim().ToUpperInvariant();
            if (string.IsNullOrWhiteSpace(module))
            {
                return;
            }

            // Find commands for that module
            // don't show commands which are blocked
            // order by name
            var cmds = this.cmds.Commands.Where(
                c => c.Module.GetTopLevelModule().Name.ToUpperInvariant().StartsWith(module, StringComparison.InvariantCulture))
                .Where(c => !this.perms.BlockedCommands.Contains(c.Aliases[0].ToLowerInvariant()))
                .OrderBy(c => c.Aliases[0])
                .Distinct(new CommandTextEqualityComparer());

            // check preconditions for all commands, but only if it's not 'all'
            // because all will show all commands anyway, no need to check
            var succ = new HashSet<CommandInfo>();
            if (opts.View != CommandsOptions.ViewType.All)
            {
                succ = new HashSet<CommandInfo>((await Task.WhenAll(cmds.Select(async x =>
                {
                    var pre = await x.CheckPreconditionsAsync(this.Context, this.services).ConfigureAwait(false);
                    return (Cmd: x, Succ: pre.IsSuccess);
                })).ConfigureAwait(false))
                    .Where(x => x.Succ)
                    .Select(x => x.Cmd));

                if (opts.View == CommandsOptions.ViewType.Hide)
                {
                    // if hidden is specified, completely remove these commands from the list
                    cmds = cmds.Where(x => succ.Contains(x));
                }
            }

            var cmdsWithGroup = cmds.GroupBy(c => c.Module.Name.Replace("Commands", string.Empty, StringComparison.InvariantCulture))
                .OrderBy(x => x.Key == x.First().Module.Name ? int.MaxValue : x.Count());

            if (!cmds.Any())
            {
                if (opts.View != CommandsOptions.ViewType.Hide)
                {
                    await this.ReplyErrorLocalizedAsync("module_not_found").ConfigureAwait(false);
                }
                else
                {
                    await this.ReplyErrorLocalizedAsync("module_not_found_or_cant_exec").ConfigureAwait(false);
                }

                return;
            }

            var i = 0;
            var groups = cmdsWithGroup.GroupBy(x => i++ / 48).ToArray();
            var embed = new EmbedBuilder().WithOkColor();
            foreach (var g in groups)
            {
                var last = g.Count();
                for (i = 0; i < last; i++)
                {
                    var transformed = g.ElementAt(i).Select(x =>
                    {
                        // if cross is specified, and the command doesn't satisfy the requirements, cross it out
                        if (opts.View == CommandsOptions.ViewType.Cross)
                        {
                            return $"{(succ.Contains(x) ? "✅" : "❌")}{this.Prefix + x.Aliases.First(),-15} {"[" + x.Aliases.Skip(1).FirstOrDefault() + "]",-8}";
                        }

                        return $"{this.Prefix + x.Aliases.First(),-15} {"[" + x.Aliases.Skip(1).FirstOrDefault() + "]",-8}";
                    });

                    if (i == last - 1 && (i + 1) % 2 != 0)
                    {
                        var grp = 0;
                        var count = transformed.Count();
                        transformed = transformed
                            .GroupBy(x => (grp++ % count) / 2)
                            .Select(x =>
                            {
                                if (x.Count() == 1)
                                {
                                    return $"{x.First()}";
                                }
                                else
                                {
                                    return string.Concat(x);
                                }
                            });
                    }

                    embed.AddField(g.ElementAt(i).Key, "```css\n" + string.Join("\n", transformed) + "\n```", true);
                }
            }

            embed.WithFooter(this.GetText("commands_instr", this.Prefix));
            await this.Ctx.Channel.EmbedAsync(embed).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the help information.
        /// </summary>
        /// <param name="fail">Command name used to retrieve a correpsonding <see cref="CommandInfo"/>.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [PotatoBotCommand("help")]
        [Usage(new string[] { "{0}h {0}cmds", "{0}h" })]
        [Description("Either shows a help for a single command, or DMs you help link if no parameters are specified.")]
        [Aliases("h")]
        [Priority(0)]
        public async Task HelpCommand([Leftover] string fail)
        {
            var prefixless = this.cmds.Commands.FirstOrDefault(x => x.Aliases.Any(cmdName => cmdName.ToLowerInvariant() == fail));
            if (prefixless != null)
            {
                await this.HelpCommand(prefixless).ConfigureAwait(false);
                return;
            }

            await this.ReplyErrorLocalizedAsync("command_not_found").ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the help information.
        /// </summary>
        /// <param name="com">The <see cref="CommandInfo"/> for the help command.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [PotatoBotCommand("help")]
        [Usage(new string[] { "{0}h {0}cmds", "{0}h" })]
        [Description("Either shows a help for a single command, or DMs you help link if no parameters are specified.")]
        [Aliases("h")]
        [Priority(1)]
        public async Task HelpCommand([Leftover] CommandInfo com = null)
        {
            var channel = this.Ctx.Channel;

            if (com == null)
            {
                IMessageChannel ch = channel is ITextChannel
                    ? await ((IGuildUser)this.Ctx.User).GetOrCreateDMChannelAsync().ConfigureAwait(false)
                    : channel;
                await ch.EmbedAsync(this.GetHelpStringEmbed()).ConfigureAwait(false);
                return;
            }

            var embed = this.Service.GetCommandHelp(com, this.Ctx.Guild);
            await channel.EmbedAsync(embed).ConfigureAwait(false);
        }

        /// <summary>
        /// Generates the commandlist.md file.
        /// Only for bot owners.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [PotatoBotCommand("hgit")]
        [Usage(new string[] { "{0}hgit" })]
        [Description("Generates the commandlist.md file.")]
        [Aliases("hgit")]
        [RequireContext(ContextType.Guild)]
        [OwnerOnly]
        public async Task Hgit()
        {
            Dictionary<string, List<object>> cmdData = new Dictionary<string, List<object>>();
            foreach (var com in this.cmds.Commands.OrderBy(com => com.Module.GetTopLevelModule().Name).GroupBy(c => c.Aliases.First()).Select(g => g.First()))
            {
                var module = com.Module.GetTopLevelModule();
                List<string> optHelpStr = null;
                var opt = ((PotatoBotOptionsAttribute)com.Attributes.FirstOrDefault(x => x is PotatoBotOptionsAttribute))?.OptionType;
                if (opt != null)
                {
                    optHelpStr = HelpService.GetCommandOptionHelpList(opt);
                }

                var obj = new
                {
                    Aliases = com.Aliases.Select(x => this.Prefix + x).ToArray(),
                    Description = string.Format(com.Summary, this.Prefix),
                    Usage = JsonConvert.DeserializeObject<string[]>(com.Remarks).Select(x => string.Format(x, this.Prefix)).ToArray(),
                    Submodule = com.Module.Name,
                    Module = com.Module.GetTopLevelModule().Name,
                    Options = optHelpStr,
                    Requirements = HelpService.GetCommandRequirements(com),
                };
                if (cmdData.TryGetValue(module.Name, out var cmds))
                {
                    cmds.Add(obj);
                }
                else
                {
                    cmdData.Add(module.Name, new List<object>
                    {
                        obj,
                    });
                }
            }

            File.WriteAllText("../../docs/cmds_new.json", JsonConvert.SerializeObject(cmdData, Formatting.Indented));
            await this.ReplyConfirmLocalizedAsync("commandlist_regen").ConfigureAwait(false);
        }

        /// <summary>
        /// Shows the guide in discord.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [PotatoBotCommand("guide")]
        [Usage(new string[] { "{0}readme", "{0}guide" })]
        [Description("Sends a readme and a guide links to the channel.")]
        [Aliases("readme")]
        public async Task Guide()
        {
            await this.ConfirmLocalizedAsync("guide", "No gudes to show right now").ConfigureAwait(false);

            // await this.ConfirmLocalizedAsync(
            //    "guide",
            //    "https://nadeko.bot/commands",
            //    "http://nadekobot.readthedocs.io/en/latest/").ConfigureAwait(false);
        }

        private string GetRemarks(string[] arr)
        {
            return string.Join(" or ", arr.Select(x => Format.Code(x)));
        }
    }
}
