﻿namespace NadekoBot.Core.Services.Database.Repositories.Impl
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;
    using PotatoBot.Core.Data.Database.Repositories;

    /// <summary>
    /// The implementation of the <see cref="IBotConfigRepository"/> interface.
    /// </summary>
    public class BotConfigRepository : Repository<BotConfig>, IBotConfigRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BotConfigRepository"/> class.
        /// </summary>
        /// <param name="context">The context of the database.</param>
        public BotConfigRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public BotConfig GetOrCreate(Func<DbSet<BotConfig>, IQueryable<BotConfig>> includes = null)
        {
            BotConfig config;

            if (includes == null)
            {
                config = this.Set.Include(bc => bc.RotatingStatusMessages)
                             .Include(bc => bc.Blacklist)
                             .Include(bc => bc.StartupCommands)
                             .Include(bc => bc.BlockedCommands)
                             .Include(bc => bc.BlockedModules)
                             .FirstOrDefault();
            }
            else
            {
                config = includes(this.Set).FirstOrDefault();
            }

            if (config == null)
            {
                this.Set.Add(config = new BotConfig());
                this.Context.SaveChanges();
            }

            return config;
        }
    }
}
