﻿namespace PotatoBot.Core.Data.Database.Models
{
    /// <summary>
    /// Interface for indexing.
    /// </summary>
    public interface IIndexed
    {
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        int Index { get; set; }
    }
}
