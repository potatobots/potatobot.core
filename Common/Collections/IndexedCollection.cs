﻿namespace PotatoBot.Core.Common.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// A class for supporting indexed collections.
    /// </summary>
    /// <typeparam name="T">A generic type contained within this collection.</typeparam>
    public class IndexedCollection<T> : IList<T>
        where T : class, IIndexed
    {
        private readonly object locker = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexedCollection{T}"/> class.
        /// </summary>
        public IndexedCollection()
        {
            this.Source = new List<T>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexedCollection{T}"/> class.
        /// </summary>
        /// <param name="source">A list to convert to an indexed collection.</param>
        public IndexedCollection(IEnumerable<T> source)
        {
            lock (this.locker)
            {
                this.Source = source.OrderBy(x => x.Index).ToList();
                for (var i = 0; i < this.Source.Count; i++)
                {
                    if (this.Source[i].Index != i)
                    {
                        this.Source[i].Index = i;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list containing the elements of this collection.
        /// </summary>
        public List<T> Source { get; }

        /// <inheritdoc/>
        public int Count => this.Source.Count;

        /// <inheritdoc/>
        public bool IsReadOnly => false;

        /// <inheritdoc/>
        public virtual T this[int index]
        {
            get
            {
                return this.Source[index];
            }

            set
            {
                lock (this.locker)
                {
                    value.Index = index;
                    this.Source[index] = value;
                }
            }
        }

        public static implicit operator List<T>(IndexedCollection<T> x) => x.Source;

        /// <summary>
        /// Returns a list of this collection.
        /// </summary>
        /// <returns>A list of the collection.</returns>
        public List<T> ToList() => this.Source.ToList();

        /// <inheritdoc/>
        public IEnumerator<T> GetEnumerator() =>
            this.Source.GetEnumerator();

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() =>
            this.Source.GetEnumerator();

        /// <inheritdoc/>
        public void Add(T item)
        {
            lock (this.locker)
            {
                item.Index = this.Source.Count;
                this.Source.Add(item);
            }
        }

        /// <inheritdoc/>
        public virtual void Clear()
        {
            lock (this.locker)
            {
                this.Source.Clear();
            }
        }

        /// <inheritdoc/>
        public bool Contains(T item)
        {
            lock (this.locker)
            {
                return this.Source.Contains(item);
            }
        }

        /// <inheritdoc/>
        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (this.locker)
            {
                this.Source.CopyTo(array, arrayIndex);
            }
        }

        /// <inheritdoc/>
        public virtual bool Remove(T item)
        {
            bool removed;
            lock (this.locker)
            {
                if (removed = this.Source.Remove(item))
                {
                    for (int i = 0; i < this.Source.Count; i++)
                    {
                        // hm, no idea how ef works, so I don't want to set if it's not changed,
                        // maybe it will try to update db?
                        // But most likely it just compares old to new values, meh.
                        if (this.Source[i].Index != i)
                        {
                            this.Source[i].Index = i;
                        }
                    }
                }
            }

            return removed;
        }

        /// <inheritdoc/>
        public int IndexOf(T item) => item.Index;

        /// <inheritdoc/>
        public virtual void Insert(int index, T item)
        {
            lock (this.locker)
            {
                this.Source.Insert(index, item);
                for (int i = index; i < this.Source.Count; i++)
                {
                    this.Source[i].Index = i;
                }
            }
        }

        /// <inheritdoc/>
        public virtual void RemoveAt(int index)
        {
            lock (this.locker)
            {
                this.Source.RemoveAt(index);
                for (int i = index; i < this.Source.Count; i++)
                {
                    this.Source[i].Index = i;
                }
            }
        }
    }
}
