﻿namespace PotatoBot.Core.Modules
{
    using System.Globalization;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using NLog;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Service;
    using PotatoBot.Core.Service.Extensions;

    /// <summary>
    /// Abstract base class for top level modules.
    /// </summary>
    public abstract class Module : ModuleBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Module"/> class.
        /// </summary>
        /// <param name="isTopLevelModule">Boolean indicating if module is top level.</param>
        protected Module(bool isTopLevelModule = true)
        {
            // if it's top level module
            this.ModuleTypeName = isTopLevelModule ? this.GetType().Name : this.GetType().DeclaringType.Name;
            this.LowerModuleTypeName = this.ModuleTypeName.ToLowerInvariant();
            this.Log = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Gets the module type name.
        /// </summary>
        public string ModuleTypeName { get; }

        /// <summary>
        /// Gets the lower module type name.
        /// </summary>
        public string LowerModuleTypeName { get; }

        /// <summary>
        /// Gets or sets the potato bot strings class.
        /// </summary>
        public PotatoBotStrings Strings { get; set; }

        /// <summary>
        /// Gets or sets the bot configurator provider.
        /// </summary>
        public IBotConfigProvider Bc { get; set; }

        /// <summary>
        /// Gets or sets the command handler.
        /// </summary>
        public CommandHandler CmdHandler { get; set; }

        /// <summary>
        /// Gets or sets the localization.
        /// </summary>
        public ILocalization Localization { get; set; }

        /// <summary>
        /// Gets the guild prefix.
        /// </summary>
        public string Prefix => this.CmdHandler.GetPrefix(this.Ctx.Guild);

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected Logger Log { get; }

        /// <summary>
        /// Gets or sets the culture information.
        /// </summary>
        protected CultureInfo CultureInfo { get; set; }

        /// <summary>
        /// Gets the command context.
        /// </summary>
        protected ICommandContext Ctx => this.Context;

        /// <summary>
        /// Send error async.
        /// </summary>
        /// <param name="textKey">Key for getting potato bot string.</param>
        /// <param name="replacements">Replacements for the string.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<IUserMessage> ErrorLocalizedAsync(string textKey, params object[] replacements)
        {
            var text = this.GetText(textKey, replacements);
            return this.Ctx.Channel.SendErrorAsync(text);
        }

        /// <summary>
        /// Reply to localized error async.
        /// </summary>
        /// <param name="textKey">Key for getting potato bot string.</param>
        /// <param name="replacements">Replacements for the string.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<IUserMessage> ReplyErrorLocalizedAsync(string textKey, params object[] replacements)
        {
            var text = this.GetText(textKey, replacements);
            return this.Ctx.Channel.SendErrorAsync(Format.Bold(this.Ctx.User.ToString()) + " " + text);
        }

        /// <summary>
        /// Send localized confirm async.
        /// </summary>
        /// <param name="textKey">Key for getting potato bot string.</param>
        /// <param name="replacements">Replacements for the string.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<IUserMessage> ConfirmLocalizedAsync(string textKey, params object[] replacements)
        {
            var text = this.GetText(textKey, replacements);
            return this.Ctx.Channel.SendConfirmAsync(text);
        }

        /// <summary>
        /// Reply to localized confirm async.
        /// </summary>
        /// <param name="textKey">Key for getting potato bot string.</param>
        /// <param name="replacements">Replacements for the string.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public Task<IUserMessage> ReplyConfirmLocalizedAsync(string textKey, params object[] replacements)
        {
            var text = this.GetText(textKey, replacements);
            return this.Ctx.Channel.SendConfirmAsync(Format.Bold(this.Ctx.User.ToString()) + " " + text);
        }

        /// <summary>
        /// Send promt for user confirm async.
        /// </summary>
        /// <param name="embed">The Discord embed builder.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public async Task<bool> PromptUserConfirmAsync(EmbedBuilder embed)
        {
            embed.WithOkColor()
                .WithFooter("yes/no");

            var msg = await this.Ctx.Channel.EmbedAsync(embed).ConfigureAwait(false);
            try
            {
                var input = await this.GetUserInputAsync(this.Ctx.User.Id, this.Ctx.Channel.Id).ConfigureAwait(false);
                input = input?.ToUpperInvariant();

                if (input != "YES" && input != "Y")
                {
                    return false;
                }

                return true;
            }
            finally
            {
                await Task.Run(() => msg.DeleteAsync());
            }
        }

        /// <summary>
        /// Get user input async.
        /// </summary>
        /// <param name="userId">User id needed for identifying user input.</param>
        /// <param name="channelId">The channel id the input is getted from.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public async Task<string> GetUserInputAsync(ulong userId, ulong channelId)
        {
            var userInputTask = new TaskCompletionSource<string>();
            var dsc = (DiscordSocketClient)this.Ctx.Client;
            try
            {
                dsc.MessageReceived += MessageReceived;

                if (await Task.WhenAny(userInputTask.Task, Task.Delay(10000)).ConfigureAwait(false) != userInputTask.Task)
                {
                    return null;
                }

                return await userInputTask.Task.ConfigureAwait(false);
            }
            finally
            {
                dsc.MessageReceived -= MessageReceived;
            }

            Task MessageReceived(SocketMessage arg)
            {
                Task.Run(() =>
                {
                    if (!(arg is SocketUserMessage userMsg) ||
                        !(userMsg.Channel is ITextChannel chan) ||
                        userMsg.Author.Id != userId ||
                        userMsg.Channel.Id != channelId)
                    {
                        return Task.CompletedTask;
                    }

                    if (userInputTask.TrySetResult(arg.Content))
                    {
                        userMsg.DeleteAfter(1);
                    }

                    return Task.CompletedTask;
                });
                return Task.CompletedTask;
            }
        }

        /// <inheritdoc/>
        protected override void BeforeExecute(CommandInfo cmd)
        {
            this.CultureInfo = this.Localization.GetCultureInfo(this.Ctx.Guild?.Id);
        }

        /// <summary>
        /// Gets a text identified by key.
        /// </summary>
        /// <param name="key">Identification key.</param>
        /// <returns>The string value identified by key.</returns>
        protected string GetText(string key) =>
            this.Strings.GetText(key, this.CultureInfo, this.LowerModuleTypeName);

        /// <summary>
        /// Gets a text identified by key.
        /// </summary>
        /// <param name="key">Identification key.</param>
        /// <param name="replacements">The replacements of the string.</param>
        /// <returns>The string value identified by key.</returns>
        protected string GetText(string key, params object[] replacements) =>
            this.Strings.GetText(key, this.CultureInfo, this.LowerModuleTypeName, replacements);
    }

    /// <summary>
    /// Abstract base class for top level modules including a <see cref="INService"/>.
    /// </summary>
    /// <typeparam name="TService">The service used by the module.</typeparam>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:File may only contain a single type", Justification = "It's an extension on the other class. No need to move it to separate file.")]
    public abstract class Module<TService> : Module
        where TService : INService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Module{TService}"/> class.
        /// </summary>
        /// <param name="isTopLevel">Indicator if module is top level.</param>
        protected Module(bool isTopLevel = true)
            : base(isTopLevel)
        {
        }

        /// <summary>
        /// Gets or sets the service.
        /// </summary>
        public TService Service { get; set; }
    }
}
