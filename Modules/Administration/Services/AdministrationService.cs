﻿namespace PotatoBot.Core.Modules.Administration.Services
{
    using NLog;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Service;
    using PotatoBot.Core.Service.Database;

    /// <summary>
    /// The discord administration service.
    /// </summary>
    public class AdministrationService : INService
    {
        private readonly Logger log;
        private readonly IPotatoBot bot;
        private readonly DbService db;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdministrationService"/> class.
        /// </summary>
        /// <param name="bot">The potato bot interface.</param>
        /// <param name="cmdHandler">The command handler.</param>
        /// <param name="db">the database service.</param>
        public AdministrationService(IPotatoBot bot, CommandHandler cmdHandler, DbService db)
        {
            this.log = LogManager.GetCurrentClassLogger();
            this.bot = bot;
            this.db = db;
        }
    }
}
