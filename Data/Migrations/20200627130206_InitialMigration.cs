﻿namespace PotatoBot.Core.Data.Migrations
{
    using System;
    using Microsoft.EntityFrameworkCore.Migrations;

    /// <summary>
    /// Initial class for database migration.
    /// </summary>
    public partial class InitialMigration : Migration
    {
        /// <summary>
        /// Updates the database.
        /// </summary>
        /// <param name="migrationBuilder">The migration builder.</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BotConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    BufferSize = table.Column<decimal>(nullable: false),
                    RotatingStatuses = table.Column<bool>(nullable: false),
                    DmHelpString = table.Column<string>(nullable: true),
                    HelpString = table.Column<string>(nullable: true),
                    MigrationVersion = table.Column<int>(nullable: false),
                    OkColor = table.Column<string>(nullable: true),
                    ErrorColor = table.Column<string>(nullable: true),
                    Locale = table.Column<string>(nullable: true),
                    PermissionVersion = table.Column<int>(nullable: false),
                    DefaultPrefix = table.Column<string>(nullable: true),
                    ConsoleOutputType = table.Column<int>(nullable: false),
                    UpdateString = table.Column<string>(nullable: true),
                    CheckForUpdates = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotConfigs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscordUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<decimal>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: true),
                    AvatarId = table.Column<string>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlacklistItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    ItemId = table.Column<decimal>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    BotConfigId = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlacklistItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlacklistItem_BotConfigs_BotConfigId",
                        column: x => x.BotConfigId,
                        principalTable: "BotConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlockedCmdOrMdl",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    BotConfigId = table.Column<int>(nullable: true),
                    BotConfigId1 = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockedCmdOrMdl", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlockedCmdOrMdl_BotConfigs_BotConfigId",
                        column: x => x.BotConfigId,
                        principalTable: "BotConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlockedCmdOrMdl_BotConfigs_BotConfigId1",
                        column: x => x.BotConfigId1,
                        principalTable: "BotConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlayingStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    BotConfigId = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayingStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlayingStatus_BotConfigs_BotConfigId",
                        column: x => x.BotConfigId,
                        principalTable: "BotConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StartupCommand",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    Index = table.Column<int>(nullable: false),
                    CommandText = table.Column<string>(nullable: true),
                    ChannelId = table.Column<decimal>(nullable: false),
                    ChannelName = table.Column<string>(nullable: true),
                    GuildId = table.Column<decimal>(nullable: true),
                    GuildName = table.Column<string>(nullable: true),
                    VoiceChannelId = table.Column<decimal>(nullable: true),
                    VoiceChannelName = table.Column<string>(nullable: true),
                    Interval = table.Column<int>(nullable: false),
                    BotConfigId = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StartupCommand", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StartupCommand_BotConfigs_BotConfigId",
                        column: x => x.BotConfigId,
                        principalTable: "BotConfigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    GuildConfigId = table.Column<int>(nullable: true),
                    Index = table.Column<int>(nullable: false),
                    PrimaryTarget = table.Column<int>(nullable: false),
                    PrimaryTargetId = table.Column<decimal>(nullable: false),
                    SecondaryTarget = table.Column<int>(nullable: false),
                    SecondaryTargetName = table.Column<string>(nullable: true),
                    IsCustomCommand = table.Column<bool>(nullable: false),
                    State = table.Column<bool>(nullable: false),
                    PermissionId = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GuildConfigs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateAdded = table.Column<DateTime>(nullable: true),
                    GuildId = table.Column<decimal>(nullable: false),
                    Prefix = table.Column<string>(nullable: true),
                    Locale = table.Column<string>(nullable: true),
                    TimeZoneId = table.Column<string>(nullable: true),
                    PermissionId = table.Column<int>(nullable: true),
                    VerbosePermissions = table.Column<bool>(nullable: false),
                    PermissionRole = table.Column<string>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuildConfigs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GuildConfigs_Permission_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "Permission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlacklistItem_BotConfigId",
                table: "BlacklistItem",
                column: "BotConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockedCmdOrMdl_BotConfigId",
                table: "BlockedCmdOrMdl",
                column: "BotConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockedCmdOrMdl_BotConfigId1",
                table: "BlockedCmdOrMdl",
                column: "BotConfigId1");

            migrationBuilder.CreateIndex(
                name: "IX_GuildConfigs_PermissionId",
                table: "GuildConfigs",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_PermissionId",
                table: "Permission",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayingStatus_BotConfigId",
                table: "PlayingStatus",
                column: "BotConfigId");

            migrationBuilder.CreateIndex(
                name: "IX_StartupCommand_BotConfigId",
                table: "StartupCommand",
                column: "BotConfigId");

            migrationBuilder.AddForeignKey(
                name: "FK_Permission_GuildConfigs_PermissionId",
                table: "Permission",
                column: "PermissionId",
                principalTable: "GuildConfigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <summary>
        /// Removes the update.
        /// </summary>
        /// <param name="migrationBuilder">The migration builder.</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GuildConfigs_Permission_PermissionId",
                table: "GuildConfigs");

            migrationBuilder.DropTable(
                name: "BlacklistItem");

            migrationBuilder.DropTable(
                name: "BlockedCmdOrMdl");

            migrationBuilder.DropTable(
                name: "DiscordUsers");

            migrationBuilder.DropTable(
                name: "PlayingStatus");

            migrationBuilder.DropTable(
                name: "StartupCommand");

            migrationBuilder.DropTable(
                name: "BotConfigs");

            migrationBuilder.DropTable(
                name: "Permission");

            migrationBuilder.DropTable(
                name: "GuildConfigs");
        }
    }
}
