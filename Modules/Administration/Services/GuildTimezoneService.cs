﻿namespace PotatoBot.Core.Modules.Administration.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading.Tasks;
    using Discord.WebSocket;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Data.Database.Models;
    using PotatoBot.Core.Service.Database;

    /// <summary>
    /// A service providing time zone related methods.
    /// </summary>
    public class GuildTimezoneService : INService
    {
        private readonly ConcurrentDictionary<ulong, TimeZoneInfo> timezones;
        private readonly DbService db;

        /// <summary>
        /// Initializes a new instance of the <see cref="GuildTimezoneService"/> class.
        /// </summary>
        /// <param name="client">The discord bot api client.</param>
        /// <param name="bot">This bot.</param>
        /// <param name="db">The database service.</param>
        public GuildTimezoneService(DiscordSocketClient client, IPotatoBot bot, DbService db)
        {
            this.timezones = bot.AllGuildConfigs
                .Select(GetTimzezoneTuple)
                .Where(x => x.Timezone != null)
                .ToDictionary(x => x.GuildId, x => x.Timezone)
                .ToConcurrent();

            var curUser = client.CurrentUser;
            if (curUser != null)
            {
                AllServices.TryAdd(curUser.Id, this);
            }

            this.db = db;

            bot.JoinedGuild += this.Bot_JoinedGuild;
        }

        /// <summary>
        /// Gets all services.
        /// </summary>
        public static ConcurrentDictionary<ulong, GuildTimezoneService> AllServices { get; } = new ConcurrentDictionary<ulong, GuildTimezoneService>();

        /// <summary>
        /// Get timezone or set to default and get after.
        /// </summary>
        /// <param name="guildId">The guild id where the timezone needs to be set.</param>
        /// <returns>A <see cref="TimeZoneInfo"/>.</returns>
        public TimeZoneInfo GetTimeZoneOrDefault(ulong guildId)
        {
            if (this.timezones.TryGetValue(guildId, out var tz))
            {
                return tz;
            }

            return null;
        }

        /// <summary>
        /// Set timezone to a guild.
        /// </summary>
        /// <param name="guildId">The guild id.</param>
        /// <param name="tz">The timezone that needs to be set to the guild.</param>
        public void SetTimeZone(ulong guildId, TimeZoneInfo tz)
        {
            using (var uow = this.db.GetDbContext())
            {
                var gc = uow.GuildConfigs.ForId(guildId, set => set);

                gc.TimeZoneId = tz?.Id;
                uow.SaveChanges();

                if (tz == null)
                {
                    this.timezones.TryRemove(guildId, out tz);
                }
                else
                {
                    this.timezones.AddOrUpdate(guildId, tz, (key, old) => tz);
                }
            }
        }

        /// <summary>
        /// Get timezone or default to UTC.
        /// </summary>
        /// <param name="guildId">The guild id.</param>
        /// <returns>a <see cref="TimeZoneInfo"/>.</returns>
        public TimeZoneInfo GetTimeZoneOrUtc(ulong guildId)
            => this.GetTimeZoneOrDefault(guildId) ?? TimeZoneInfo.Utc;

        private static (ulong GuildId, TimeZoneInfo Timezone) GetTimzezoneTuple(GuildConfig x)
        {
            TimeZoneInfo tz;
            try
            {
                if (x.TimeZoneId == null)
                {
                    tz = null;
                }
                else
                {
                    tz = TimeZoneInfo.FindSystemTimeZoneById(x.TimeZoneId);
                }
            }
            catch
            {
                tz = null;
            }

            return (x.GuildId, Timezone: tz);
        }

        private Task Bot_JoinedGuild(GuildConfig arg)
        {
            var (guildId, tz) = GetTimzezoneTuple(arg);
            if (tz != null)
            {
                this.timezones.TryAdd(guildId, tz);
            }

            return Task.CompletedTask;
        }
    }
}
