﻿namespace PotatoBot.Core.Modules.Permissions.Common
{
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// A class for stroring permissions.
    /// </summary>
    public class PermissionCache
    {
        /// <summary>
        /// Gets or sets the permission role.
        /// </summary>
        public string PermRole { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether verbose mode is active.
        /// </summary>
        public bool Verbose { get; set; } = true;

        /// <summary>
        /// Gets or sets the permissions collection.
        /// </summary>
        public PermissionsCollection<Permission> Permissions { get; set; }
    }
}
