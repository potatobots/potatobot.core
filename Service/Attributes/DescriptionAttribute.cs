﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;
    using Discord.Commands;

    /// <summary>
    /// Description attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class DescriptionAttribute : SummaryAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptionAttribute"/> class.
        /// </summary>
        /// <param name="description">The description of the command.</param>
        public DescriptionAttribute(string description)
            : base(description)
        {
        }
    }
}
