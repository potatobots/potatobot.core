﻿namespace PotatoBot.Core.Service
{
    using System.Collections.Concurrent;
    using System.Globalization;
    using System.Linq;
    using Discord;
    using NLog;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Service.Database;

    /// <summary>
    /// A class implementation of the <see cref="ILocalization"/> interface.
    /// </summary>
    public class Localization : ILocalization
    {
        private readonly Logger log;
        private readonly DbService db;

        /// <summary>
        /// Initializes a new instance of the <see cref="Localization"/> class.
        /// </summary>
        /// <param name="bcp">The bot config provider.</param>
        /// <param name="bot">The bot itself.</param>
        /// <param name="db">The database service.</param>
        public Localization(IBotConfigProvider bcp, IPotatoBot bot, DbService db)
        {
            this.log = LogManager.GetCurrentClassLogger();

            var cultureInfoNames = bot.AllGuildConfigs.ToDictionary(x => x.GuildId, x => x.Locale);
            var defaultCulture = bcp.BotConfig.Locale;

            this.db = db;

            if (string.IsNullOrWhiteSpace(defaultCulture))
            {
                this.DefaultCultureInfo = new CultureInfo("en-US");
            }
            else
            {
                try
                {
                    this.DefaultCultureInfo = new CultureInfo(defaultCulture);
                }
                catch
                {
                    this.log.Warn("Unable to load default bot's locale/language. Using en-US.");
                    this.DefaultCultureInfo = new CultureInfo("en-US");
                }
            }

            this.GuildCultureInfos = new ConcurrentDictionary<ulong, CultureInfo>(cultureInfoNames.ToDictionary(x => x.Key, x =>
            {
                CultureInfo cultureInfo = null;
                try
                {
                    if (x.Value == null)
                    {
                        return null;
                    }

                    cultureInfo = new CultureInfo(x.Value);
                }
                catch
                {
                }

                return cultureInfo;
            }).Where(x => x.Value != null));
        }

        /// <inheritdoc/>
        public ConcurrentDictionary<ulong, CultureInfo> GuildCultureInfos { get; }

        /// <inheritdoc/>
        public CultureInfo DefaultCultureInfo { get; private set; } = CultureInfo.CurrentCulture;

        /// <inheritdoc/>
        public void SetGuildCulture(IGuild guild, CultureInfo ci) =>
            this.SetGuildCulture(guild.Id, ci);

        /// <inheritdoc/>
        public void SetGuildCulture(ulong guildId, CultureInfo ci)
        {
            if (ci == this.DefaultCultureInfo)
            {
                this.RemoveGuildCulture(guildId);
                return;
            }

            using (var uow = this.db.GetDbContext())
            {
                var gc = uow.GuildConfigs.ForId(guildId, set => set);
                gc.Locale = ci.Name;
                uow.SaveChanges();
            }

            this.GuildCultureInfos.AddOrUpdate(guildId, ci, (id, old) => ci);
        }

        /// <inheritdoc/>
        public void RemoveGuildCulture(IGuild guild) =>
            this.RemoveGuildCulture(guild.Id);

        /// <inheritdoc/>
        public void RemoveGuildCulture(ulong guildId)
        {
            if (this.GuildCultureInfos.TryRemove(guildId, out var _))
            {
                using (var uow = this.db.GetDbContext())
                {
                    var gc = uow.GuildConfigs.ForId(guildId, set => set);
                    gc.Locale = null;
                    uow.SaveChanges();
                }
            }
        }

        /// <inheritdoc/>
        public void SetDefaultCulture(CultureInfo ci)
        {
            using (var uow = this.db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate(set => set);
                bc.Locale = ci.Name;
                uow.SaveChanges();
            }

            this.DefaultCultureInfo = ci;
        }

        /// <inheritdoc/>
        public void ResetDefaultCulture() =>
            this.SetDefaultCulture(CultureInfo.CurrentCulture);

        /// <inheritdoc/>
        public CultureInfo GetCultureInfo(IGuild guild) =>
            this.GetCultureInfo(guild?.Id);

        /// <inheritdoc/>
        public CultureInfo GetCultureInfo(ulong? guildId)
        {
            if (guildId == null)
            {
                return this.DefaultCultureInfo;
            }

            this.GuildCultureInfos.TryGetValue(guildId.Value, out CultureInfo info);
            return info ?? this.DefaultCultureInfo;
        }
    }
}
