﻿namespace PotatoBot.Core.Common
{
    using System;
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;

    /// <summary>
    /// A class functioned as event wrapper.
    /// </summary>
    public sealed class ReactionEventWrapper : IDisposable
    {
        private readonly DiscordSocketClient client;
        private bool disposing = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReactionEventWrapper"/> class.
        /// </summary>
        /// <param name="client">The discord client.</param>
        /// <param name="msg">A user massage.</param>
        public ReactionEventWrapper(DiscordSocketClient client, IUserMessage msg)
        {
            this.Message = msg ?? throw new ArgumentNullException(nameof(msg));
            this.client = client;

            this.client.ReactionAdded += this.Discord_ReactionAdded;
            this.client.ReactionRemoved += this.Discord_ReactionRemoved;
            this.client.ReactionsCleared += this.Discord_ReactionsCleared;
        }

        /// <summary>
        /// On reaction added event.
        /// </summary>
        public event Action<SocketReaction> OnReactionAdded = obj => { };

        /// <summary>
        /// On reaction removed event.
        /// </summary>
        public event Action<SocketReaction> OnReactionRemoved = obj => { };

        /// <summary>
        /// On reaction cleared event.
        /// </summary>
        public event Action OnReactionsCleared = () => { };

        /// <summary>
        /// Gets the message.
        /// </summary>
        public IUserMessage Message { get; }

        /// <summary>
        /// Unsubscribe to all.
        /// </summary>
        public void UnsubscribeAll()
        {
            this.client.ReactionAdded -= this.Discord_ReactionAdded;
            this.client.ReactionRemoved -= this.Discord_ReactionRemoved;
            this.client.ReactionsCleared -= this.Discord_ReactionsCleared;
            this.OnReactionAdded = null;
            this.OnReactionRemoved = null;
            this.OnReactionsCleared = null;
        }

        /// <summary>
        /// Dispose.
        /// </summary>
        public void Dispose()
        {
            if (this.disposing)
            {
                return;
            }

            this.disposing = true;
            this.UnsubscribeAll();
        }

        private Task Discord_ReactionsCleared(Cacheable<IUserMessage, ulong> msg, ISocketMessageChannel channel)
        {
            Task.Run(() =>
            {
                try
                {
                    if (msg.Id == this.Message.Id)
                    {
                        this.OnReactionsCleared?.Invoke();
                    }
                }
                catch
                {
                }
            });

            return Task.CompletedTask;
        }

        private Task Discord_ReactionRemoved(Cacheable<IUserMessage, ulong> msg, ISocketMessageChannel channel, SocketReaction reaction)
        {
            Task.Run(() =>
            {
                try
                {
                    if (msg.Id == this.Message.Id)
                    {
                        this.OnReactionRemoved?.Invoke(reaction);
                    }
                }
                catch
                {
                }
            });

            return Task.CompletedTask;
        }

        private Task Discord_ReactionAdded(Cacheable<IUserMessage, ulong> msg, ISocketMessageChannel channel, SocketReaction reaction)
        {
            Task.Run(() =>
            {
                try
                {
                    if (msg.Id == this.Message.Id)
                    {
                        this.OnReactionAdded?.Invoke(reaction);
                    }
                }
                catch
                {
                }
            });

            return Task.CompletedTask;
        }
    }
}
