﻿namespace PotatoBot.Core.Modules.Permissions.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Discord;
    using Discord.WebSocket;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// A permissions extension class.
    /// </summary>
    public static class PermissionExtensions
    {
        /// <summary>
        /// Check permissions.
        /// </summary>
        /// <param name="permsEnumerable">List of permissons.</param>
        /// <param name="message">Message a discord user has send.</param>
        /// <param name="commandName">The command name.</param>
        /// <param name="moduleName">The module name.</param>
        /// <param name="permIndex">The permission index.</param>
        /// <returns>A bool indicating if the permissions are valid.</returns>
        public static bool CheckPermissions(
            this IEnumerable<Permission> permsEnumerable,
            IUserMessage message,
            string commandName,
            string moduleName,
            out int permIndex)
        {
            var perms = permsEnumerable as List<Permission> ?? permsEnumerable.ToList();

            for (int i = perms.Count - 1; i >= 0; i--)
            {
                var perm = perms[i];

                var result = perm.CheckPermission(message, commandName, moduleName);

                if (result == null)
                {
                    continue;
                }

                permIndex = i;
                return result.Value;
            }

            // Defaut behaviour
            permIndex = -1;
            return true;
        }

        /// <summary>
        /// Check the permissions.
        ///
        /// null = not applicable
        /// true = applicable, allowed
        /// false = applicable, not allowed.
        /// </summary>
        /// <param name="perm">The permissions.</param>
        /// <param name="message">The user message in discord.</param>
        /// <param name="commandName">The command name.</param>
        /// <param name="moduleName">The module name.</param>
        /// <returns>A bool indicating if the user has permissions to use the command.</returns>
        public static bool? CheckPermission(this Permission perm, IUserMessage message, string commandName, string moduleName)
        {
            if (!((perm.SecondaryTarget == SecondaryPermissionType.Command &&
                    perm.SecondaryTargetName.ToLowerInvariant() == commandName.ToLowerInvariant()) ||
                (perm.SecondaryTarget == SecondaryPermissionType.Module &&
                    perm.SecondaryTargetName.ToLowerInvariant() == moduleName.ToLowerInvariant()) ||
                    perm.SecondaryTarget == SecondaryPermissionType.AllModules))
            {
                return null;
            }

            var guildUser = message.Author as IGuildUser;

            switch (perm.PrimaryTarget)
            {
                case PrimaryPermissionType.User:
                    if (perm.PrimaryTargetId == message.Author.Id)
                    {
                        return perm.State;
                    }

                    break;
                case PrimaryPermissionType.Channel:
                    if (perm.PrimaryTargetId == message.Channel.Id)
                    {
                        return perm.State;
                    }

                    break;
                case PrimaryPermissionType.Role:
                    if (guildUser == null)
                    {
                        break;
                    }

                    if (guildUser.RoleIds.Contains(perm.PrimaryTargetId))
                    {
                        return perm.State;
                    }

                    break;
                case PrimaryPermissionType.Server:
                    if (guildUser == null)
                    {
                        break;
                    }

                    return perm.State;
            }

            return null;
        }

        /// <summary>
        /// Get a command.
        /// </summary>
        /// <param name="perm">The permissions for the command.</param>
        /// <param name="prefix">The prefix of the command.</param>
        /// <param name="guild">The discord <see cref="SocketGuild"/>.</param>
        /// <returns>A command.</returns>
        public static string GetCommand(this Permission perm, string prefix, SocketGuild guild = null)
        {
            var com = string.Empty;
            switch (perm.PrimaryTarget)
            {
                case PrimaryPermissionType.User:
                    com += "u";
                    break;
                case PrimaryPermissionType.Channel:
                    com += "c";
                    break;
                case PrimaryPermissionType.Role:
                    com += "r";
                    break;
                case PrimaryPermissionType.Server:
                    com += "s";
                    break;
            }

            switch (perm.SecondaryTarget)
            {
                case SecondaryPermissionType.Module:
                    com += "m";
                    break;
                case SecondaryPermissionType.Command:
                    com += "c";
                    break;
                case SecondaryPermissionType.AllModules:
                    com = "a" + com + "m";
                    break;
            }

            var secName = perm.SecondaryTarget == SecondaryPermissionType.Command && !perm.IsCustomCommand ?
                prefix + perm.SecondaryTargetName : perm.SecondaryTargetName;
            com += " " + (perm.SecondaryTargetName != "*" ? secName + " " : string.Empty) + (perm.State ? "enable" : "disable") + " ";

            switch (perm.PrimaryTarget)
            {
                case PrimaryPermissionType.User:
                    com += guild?.GetUser(perm.PrimaryTargetId)?.ToString() ?? $"<@{perm.PrimaryTargetId}>";
                    break;
                case PrimaryPermissionType.Channel:
                    com += $"<#{perm.PrimaryTargetId}>";
                    break;
                case PrimaryPermissionType.Role:
                    com += guild?.GetRole(perm.PrimaryTargetId)?.ToString() ?? $"<@&{perm.PrimaryTargetId}>";
                    break;
                case PrimaryPermissionType.Server:
                    break;
            }

            return prefix + com;
        }

        // public static IEnumerable<Permission> AsEnumerable(this Permission perm)
        // {
        //     do
        //     {
        //         yield return perm;
        //     }
        //     while ((perm = perm.Next) != null);
        // }
    }
}
