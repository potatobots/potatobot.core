﻿namespace PotatoBot.Core.Data.Database.Models
{
    using System;

    /// <summary>
    /// A model for blocked commands.
    /// </summary>
    public class BlockedCmdOrMdl : DbEntity
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj) =>
            (obj as BlockedCmdOrMdl)?.Name?.ToUpperInvariant() == this.Name.ToUpperInvariant();

        /// <inheritdoc/>
        public override int GetHashCode() =>
            this.Name.GetHashCode(StringComparison.InvariantCulture);
    }
}
