﻿namespace PotatoBot.Core.Data.Database.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Enum defining the different primary permission types.
    /// </summary>
    public enum PrimaryPermissionType
    {
        /// <summary>
        /// User type.
        /// </summary>
        User,

        /// <summary>
        /// Channel type.
        /// </summary>
        Channel,

        /// <summary>
        /// Role type.
        /// </summary>
        Role,

        /// <summary>
        /// Server type.
        /// </summary>
        Server,
    }

    /// <summary>
    /// Enum defining the different secondary permission types.
    /// </summary>
    public enum SecondaryPermissionType
    {
        /// <summary>
        /// Module type.
        /// </summary>
        Module,

        /// <summary>
        /// Command type.
        /// </summary>
        Command,

        /// <summary>
        /// All modules type.
        /// </summary>
        AllModules,
    }

    /// <summary>
    /// A model defining the permissions.
    /// </summary>
    public class Permission : DbEntity, IIndexed
    {
        /// <summary>
        /// Gets a permission file which allows all.
        /// </summary>
        [NotMapped]
        public static Permission AllowAllPerm => new Permission()
        {
            PrimaryTarget = PrimaryPermissionType.Server,
            PrimaryTargetId = 0,
            SecondaryTarget = SecondaryPermissionType.AllModules,
            SecondaryTargetName = "*",
            State = true,
            Index = 0,
        };

        /// <summary>
        /// Gets the default permission list.
        /// </summary>
        public static List<Permission> GetDefaultPermlist =>
            new List<Permission>
            {
                AllowAllPerm,
            };

        /// <summary>
        /// Gets or sets the guild config id.
        /// </summary>
        public int? GuildConfigId { get; set; }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the primary target.
        /// </summary>
        public PrimaryPermissionType PrimaryTarget { get; set; }

        /// <summary>
        /// Gets or sets the primary target id.
        /// </summary>
        public ulong PrimaryTargetId { get; set; }

        /// <summary>
        /// Gets or sets the secondary target.
        /// </summary>
        public SecondaryPermissionType SecondaryTarget { get; set; }

        /// <summary>
        /// Gets or sets the secondary target name.
        /// </summary>
        public string SecondaryTargetName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the command is custom.
        /// </summary>
        public bool IsCustomCommand { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the permission state is active.
        /// </summary>
        public bool State { get; set; }
    }
}
