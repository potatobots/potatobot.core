﻿namespace PotatoBot.Core.Common.CREmbed
{
    using Newtonsoft.Json;

    /// <summary>
    /// A class representing a embed footer.
    /// </summary>
    public class CREmbedFooter
    {
        /// <summary>
        /// Gets or sets the Text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the icon url.
        /// </summary>
        public string IconUrl { get; set; }

        [JsonProperty("icon_url")]
        private string Icon_Url { set => this.IconUrl = value; }
    }
}
