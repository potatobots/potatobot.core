﻿namespace PotatoBot.Core.Service
{
    using System;
    using Discord;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Data.Database.Models;
    using PotatoBot.Core.Service.Database;

    /// <summary>
    /// Class implementation of the <see cref="IBotConfigProvider"/> interface.
    /// </summary>
    public class BotConfigProvider : IBotConfigProvider
    {
        private readonly DbService db;

        /// <summary>
        /// Initializes a new instance of the <see cref="BotConfigProvider"/> class.
        /// </summary>
        /// <param name="db">The context of the database.</param>
        /// <param name="bc">A bot config used for this provider.</param>
        public BotConfigProvider(DbService db, BotConfig bc)
        {
            this.db = db;
            this.BotConfig = bc;
        }

        /// <inheritdoc/>
        public BotConfig BotConfig { get; private set; }

        /// <inheritdoc/>
        public void Reload()
        {
            using (var uow = this.db.GetDbContext())
            {
                this.BotConfig = uow.BotConfig.GetOrCreate();
            }
        }

        /// <inheritdoc/>
        public bool Edit(BotConfigEditType type, string newValue)
        {
            using (var uow = this.db.GetDbContext())
            {
                var bc = uow.BotConfig.GetOrCreate();
                switch (type)
                {
                    case BotConfigEditType.DmHelpString:
                        bc.DmHelpString = string.IsNullOrWhiteSpace(newValue)
                            ? "-"
                            : newValue;
                        break;
                    case BotConfigEditType.HelpString:
                        bc.HelpString = string.IsNullOrWhiteSpace(newValue)
                            ? "-"
                            : newValue;
                        break;
                    case BotConfigEditType.OkColor:
                        try
                        {
                            newValue = newValue.Replace("#", string.Empty, StringComparison.InvariantCulture);
                            var c = new Color(Convert.ToUInt32(newValue, 16));
                            PotatoBotStatics.OkColor = c;
                            bc.OkColor = newValue;
                        }
                        catch
                        {
                            return false;
                        }

                        break;
                    case BotConfigEditType.ErrorColor:
                        try
                        {
                            newValue = newValue.Replace("#", string.Empty, StringComparison.InvariantCulture);
                            var c = new Color(Convert.ToUInt32(newValue, 16));
                            PotatoBotStatics.ErrorColor = c;
                            bc.ErrorColor = newValue;
                        }
                        catch
                        {
                            return false;
                        }

                        break;
                    case BotConfigEditType.ConsoleOutputType:
                        if (!Enum.TryParse<ConsoleOutputType>(newValue, true, out var val))
                        {
                            return false;
                        }

                        bc.ConsoleOutputType = val;
                        break;
                    case BotConfigEditType.CheckForUpdates:
                        if (!Enum.TryParse<UpdateCheckType>(newValue, true, out var up))
                        {
                            return false;
                        }

                        bc.CheckForUpdates = up;
                        break;
                }

                this.BotConfig = bc;
                uow.SaveChanges();
            }

            return true;
        }
    }
}
