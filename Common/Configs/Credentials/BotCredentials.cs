﻿namespace PotatoBot.Core.Common.Configs.Credentials
{
    using System;
    using System.Collections.Immutable;
    using System.IO;
    using Discord;
    using Newtonsoft.Json;
    using NLog;
    using PotatoBot.Core.Common.Configs.Database;
    using Formatting = Newtonsoft.Json.Formatting;

    /// <summary>
    /// A Class for retrieving bot credentials.
    /// </summary>
    public class BotCredentials : IBotCredentials
    {
        private readonly Logger log;
        private readonly string credentialsFileName = Path.Combine(Directory.GetCurrentDirectory(), "credentials.json");
        private readonly string credentialsExampleFileName = Path.Combine(Directory.GetCurrentDirectory(), "credentials_example.json");

        /// <summary>
        /// Initializes a new instance of the <see cref="BotCredentials"/> class.
        /// </summary>
        public BotCredentials()
        {
            this.log = LogManager.GetCurrentClassLogger();

            try
            {
                var exampleFile = new CredentialsModel
                {
                    ClientId = 123123123,
                    Token = string.Empty,
                    OwnerIds = new ulong[1],
                    Db = new DbConfig("mssql", "Server=(LocalDB)\\MSSQLLocalDB;Database=PotatoBots;Trusted_Connection=True;MultipleActiveResultSets=true"),
                };

                File.WriteAllText(
                    this.credentialsExampleFileName,
                    JsonConvert.SerializeObject(exampleFile, Formatting.Indented));
            }
            catch
            {
                // ignored
            }

            if (!File.Exists(this.credentialsFileName))
            {
                this.log.Warn(
                    $"credentials.json is missing. Example is in {this.credentialsExampleFileName}");
            }

            try
            {
                var credentials = JsonConvert.DeserializeObject<CredentialsModel>(File.ReadAllText(this.credentialsFileName));

                this.ClientId = credentials.ClientId;
                this.Token = credentials.Token;

                if (credentials.Token == string.Empty)
                {
                    this.log.Error(
                    "Token is missing from credentials.json. Add it and restart the program.");
                    if (!Console.IsInputRedirected)
                    {
                        Console.ReadKey();
                    }

                    Environment.Exit(3);
                }

                this.OwnerIds = credentials.OwnerIds.ToImmutableArray();

                this.Db = credentials.Db;
            }
            catch (Exception ex)
            {
                this.log.Fatal(ex.Message);
                this.log.Fatal(ex);
                throw;
            }
        }

        /// <inheritdoc/>
        public ulong ClientId { get; }

        /// <inheritdoc/>
        public string Token { get; }

        /// <inheritdoc/>
        public ImmutableArray<ulong> OwnerIds { get; }

        /// <inheritdoc/>
        public DbConfig Db { get; }

        /// <inheritdoc/>
        public bool IsOwner(IUser u) => this.OwnerIds.Contains(u.Id);

        private class CredentialsModel
        {
            public ulong ClientId { get; set; }

            public string Token { get; set; }

            public ulong[] OwnerIds { get; set; }

            public DbConfig Db { get; set; }
        }
    }
}
