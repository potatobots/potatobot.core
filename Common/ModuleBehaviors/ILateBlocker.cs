﻿namespace PotatoBot.Core.Common.ModuleBehaviors
{
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public interface ILateBlocker
    {
        Task<bool> TryBlockLate(
            DiscordSocketClient client,
            IUserMessage msg,
            IGuild guild,
            IMessageChannel channel,
            IUser user,
            string moduleName,
            string commandName);
    }
}
