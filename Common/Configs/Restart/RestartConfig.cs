﻿namespace PotatoBot.Core.Common.Configs.Restart
{
    /// <summary>
    /// A Class for configuring the restart.
    /// </summary>
    public class RestartConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestartConfig"/> class.
        /// </summary>
        /// <param name="cmd">The command for restarting.</param>
        /// <param name="args">The arguments for restarting.</param>
        public RestartConfig(string cmd, string args)
        {
            this.Cmd = cmd;
            this.Args = args;
        }

        /// <summary>
        /// Gets the command.
        /// </summary>
        public string Cmd { get; }

        /// <summary>
        /// Gets the arguments.
        /// </summary>
        public string Args { get; }
    }
}
