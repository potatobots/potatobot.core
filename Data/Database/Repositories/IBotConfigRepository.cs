﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// An interface for the bot config repository.
    /// </summary>
    public interface IBotConfigRepository : IRepository<BotConfig>
    {
        /// <summary>
        /// Gets or creates a bot config.
        /// </summary>
        /// <param name="includes">is used for searching the bot config you need.</param>
        /// <returns>A bot config defined by <paramref name="includes"/>.</returns>
        BotConfig GetOrCreate(Func<DbSet<BotConfig>, IQueryable<BotConfig>> includes = null);
    }
}
