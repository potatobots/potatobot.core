﻿namespace PotatoBot.Core.Core
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;

    /// <summary>
    /// Main application to run the bot.
    /// </summary>
    internal class Program
    {
        private static PotatoBot potatoBot;

        /// <summary>
        /// The default main function to run the application.
        /// </summary>
        /// <param name="args">Arguments given when running the application.</param>
        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(Program.OnProcessExit);
            Program.MainAsync().GetAwaiter().GetResult();
        }

        private static async Task MainAsync()
        {
            Program.potatoBot = new PotatoBot(0, Process.GetCurrentProcess().Id);
            await Program.potatoBot.RunAndBlockAsync();
        }

        private static async void OnProcessExit(object sender, EventArgs e)
        {
            await Program.potatoBot.StopAsync();
        }
    }
}