﻿namespace PotatoBot.Core.Data.Database.Models
{
    /// <summary>
    /// A model for startup commands.
    /// </summary>
    public class StartupCommand : DbEntity, IIndexed
    {
        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Gets or sets the command text.
        /// </summary>
        public string CommandText { get; set; }

        /// <summary>
        /// Gets or sets the channel id.
        /// </summary>
        public ulong ChannelId { get; set; }

        /// <summary>
        /// Gets or sets the Channel name.
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// Gets or sets the guild id.
        /// </summary>
        public ulong? GuildId { get; set; }

        /// <summary>
        /// Gets or sets the guild name.
        /// </summary>
        public string GuildName { get; set; }

        /// <summary>
        /// Gets or sets the voice channel id.
        /// </summary>
        public ulong? VoiceChannelId { get; set; }

        /// <summary>
        /// Gets or sets the voice channel name.
        /// </summary>
        public string VoiceChannelName { get; set; }

        /// <summary>
        /// Gets or sets the interval.
        /// </summary>
        public int Interval { get; set; }
    }
}
