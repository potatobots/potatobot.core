﻿namespace PotatoBot.Core.Common.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using PotatoBot.Core.Common.Extensions;

    /// <summary>
    /// Class containing message channel extensions.
    /// </summary>
    public static class MessageChannelExtensions
    {
        private static readonly IEmote ArrowLeft = new Emoji("⬅");
        private static readonly IEmote ArrowRight = new Emoji("➡");

        /// <summary>
        /// Embed message async.
        /// </summary>
        /// <param name="ch">The channel the embedded message is posted to.</param>
        /// <param name="embed">The Discord embed builder.</param>
        /// <param name="msg">The message to embed.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public static Task<IUserMessage> EmbedAsync(this IMessageChannel ch, EmbedBuilder embed, string msg = "")
        {
            return ch.SendMessageAsync(msg, embed: embed.Build(), options: new RequestOptions()
            {
                RetryMode = RetryMode.AlwaysRetry,
            });
        }

        /// <summary>
        /// Send error message async.
        /// </summary>
        /// <param name="ch">The channel the message is posted to.</param>
        /// <param name="title">The title of the message.</param>
        /// <param name="error">The error message itself.</param>
        /// <param name="url">An URL to add to the post.</param>
        /// <param name="footer">The footer of the embed.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        public static Task<IUserMessage> SendErrorAsync(this IMessageChannel ch, string title, string error, string url = null, string footer = null)
        {
            var eb = new EmbedBuilder().WithErrorColor().WithDescription(error)
                .WithTitle(title);
            if (url != null && Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                eb.WithUrl(url);
            }

            if (!string.IsNullOrWhiteSpace(footer))
            {
                eb.WithFooter(efb => efb.WithText(footer));
            }

            return ch.SendMessageAsync(string.Empty, embed: eb.Build());
        }

        /// <summary>
        /// Send error message async.
        /// </summary>
        /// <param name="ch">The channel the message is posted in.</param>
        /// <param name="error">The error message itself.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static Task<IUserMessage> SendErrorAsync(this IMessageChannel ch, string error)
             => ch.SendMessageAsync(string.Empty, embed: new EmbedBuilder().WithErrorColor().WithDescription(error).Build());

        /// <summary>
        /// Send a confirmation message async.
        /// </summary>
        /// <param name="ch">The channel the message is posted in.</param>
        /// <param name="title">The title of the message.</param>
        /// <param name="text">The message itself.</param>
        /// <param name="url">An URL to add to the embed.</param>
        /// <param name="footer">The footer of the embed.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static Task<IUserMessage> SendConfirmAsync(this IMessageChannel ch, string title, string text, string url = null, string footer = null)
        {
            var eb = new EmbedBuilder().WithOkColor().WithDescription(text)
                .WithTitle(title);
            if (url != null && Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                eb.WithUrl(url);
            }

            if (!string.IsNullOrWhiteSpace(footer))
            {
                eb.WithFooter(efb => efb.WithText(footer));
            }

            return ch.SendMessageAsync(string.Empty, embed: eb.Build());
        }

        /// <summary>
        /// Send a confirmation message async.
        /// </summary>
        /// <param name="ch">The channel the message is posted in.</param>
        /// <param name="text">The message itself.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static Task<IUserMessage> SendConfirmAsync(this IMessageChannel ch, string text)
             => ch.SendMessageAsync(string.Empty, embed: new EmbedBuilder().WithOkColor().WithDescription(text).Build());

        /// <summary>
        /// Send table async.
        /// </summary>
        /// <typeparam name="T">Generic type to create a table with.</typeparam>
        /// <param name="ch">The channel the message is posted in.</param>
        /// <param name="seed">The seed.</param>
        /// <param name="items">The items as content for the table.</param>
        /// <param name="howToPrint">Function describing how to print the table.</param>
        /// <param name="columns">Column count.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static Task<IUserMessage> SendTableAsync<T>(this IMessageChannel ch, string seed, IEnumerable<T> items, Func<T, string> howToPrint, int columns = 3)
        {
            var i = 0;
            return ch.SendMessageAsync(
                $@"{seed}```css{string.Join("\n", items.GroupBy(item => i++ / columns).Select(ig => string.Concat(ig.Select(el => howToPrint(el)))))}```");
        }

        /// <summary>
        /// Send table async.
        /// </summary>
        /// <typeparam name="T">Generic type to create a table with.</typeparam>
        /// <param name="ch">The channel the message is posted in.</param>
        /// <param name="items">The items as content for the table.</param>
        /// <param name="howToPrint">Function describing how to print the table.</param>
        /// <param name="columns">Column count.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static Task<IUserMessage> SendTableAsync<T>(this IMessageChannel ch, IEnumerable<T> items, Func<T, string> howToPrint, int columns = 3) =>
            ch.SendTableAsync(string.Empty, items, howToPrint, columns);

        /// <summary>
        /// Send a paginated confirm message async.
        /// </summary>
        /// <param name="ctx">The command context.</param>
        /// <param name="currentPage">The current page of the pagination.</param>
        /// <param name="pageFunc">Page function to build the embed.</param>
        /// <param name="totalElements">Amount of total elements in the pagination.</param>
        /// <param name="itemsPerPage">Amount of items per page.</param>
        /// <param name="addPaginatedFooter">Footer of the embed.</param>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public static Task SendPaginatedConfirmAsync(
            this ICommandContext ctx,
            int currentPage,
            Func<int, EmbedBuilder> pageFunc,
            int totalElements,
            int itemsPerPage,
            bool addPaginatedFooter = true)
        {
            return ctx.SendPaginatedConfirmAsync(
                currentPage,
                (x) => Task.FromResult(pageFunc(x)),
                totalElements,
                itemsPerPage,
                addPaginatedFooter);
        }

        /// <summary>
        /// Send a paginated confirm message async.
        /// </summary>
        /// <param name="ctx">The command context.</param>
        /// <param name="currentPage">The current page of the pagination.</param>
        /// <param name="pageFunc">Page function to build the embed.</param>
        /// <param name="totalElements">Amount of total elements in the pagination.</param>
        /// <param name="itemsPerPage">Amount of items per page.</param>
        /// <param name="addPaginatedFooter">Footer of the embed.</param>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        public static async Task SendPaginatedConfirmAsync(
            this ICommandContext ctx,
            int currentPage,
            Func<int, Task<EmbedBuilder>> pageFunc,
            int totalElements,
            int itemsPerPage,
            bool addPaginatedFooter = true)
        {
            var embed = await pageFunc(currentPage).ConfigureAwait(false);

            var lastPage = (totalElements - 1) / itemsPerPage;

            if (addPaginatedFooter)
            {
                embed.AddPaginatedFooter(currentPage, lastPage);
            }

            var msg = await ctx.Channel.EmbedAsync(embed).ConfigureAwait(false) as IUserMessage;

            if (lastPage == 0)
            {
                return;
            }

            await msg.AddReactionAsync(ArrowLeft).ConfigureAwait(false);
            await msg.AddReactionAsync(ArrowRight).ConfigureAwait(false);

            await Task.Delay(2000).ConfigureAwait(false);

            var lastPageChange = DateTime.MinValue;

            async Task ChangePage(SocketReaction r)
            {
                try
                {
                    if (r.UserId != ctx.User.Id)
                    {
                        return;
                    }

                    if (DateTime.UtcNow - lastPageChange < TimeSpan.FromSeconds(1))
                    {
                        return;
                    }

                    if (r.Emote.Name == ArrowLeft.Name)
                    {
                        if (currentPage == 0)
                        {
                            return;
                        }

                        lastPageChange = DateTime.UtcNow;
                        var toSend = await pageFunc(--currentPage).ConfigureAwait(false);
                        if (addPaginatedFooter)
                        {
                            toSend.AddPaginatedFooter(currentPage, lastPage);
                        }

                        await msg.ModifyAsync(x => x.Embed = toSend.Build()).ConfigureAwait(false);
                    }
                    else if (r.Emote.Name == ArrowRight.Name)
                    {
                        if (lastPage > currentPage)
                        {
                            lastPageChange = DateTime.UtcNow;
                            var toSend = await pageFunc(++currentPage).ConfigureAwait(false);
                            if (addPaginatedFooter)
                            {
                                toSend.AddPaginatedFooter(currentPage, lastPage);
                            }

                            await msg.ModifyAsync(x => x.Embed = toSend.Build()).ConfigureAwait(false);
                        }
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            using (msg.OnReaction((DiscordSocketClient)ctx.Client, ChangePage, ChangePage))
            {
                await Task.Delay(30000).ConfigureAwait(false);
            }

            try
            {
                if (msg.Channel is ITextChannel && ((SocketGuild)ctx.Guild).CurrentUser.GuildPermissions.ManageMessages)
                {
                    await msg.RemoveAllReactionsAsync().ConfigureAwait(false);
                }
                else
                {
                    await Task.WhenAll(msg.Reactions.Where(x => x.Value.IsMe)
                        .Select(x => msg.RemoveReactionAsync(x.Key, ctx.Client.CurrentUser)));
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}
