﻿namespace PotatoBot.Core.Service
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Diagnostics;
    using System.Globalization;
    using NLog;
    using PotatoBot.Core.Common;

    /// <summary>
    /// A class for potato bot strings.
    /// </summary>
    public class PotatoBotStrings : INService
    {
        private readonly ImmutableDictionary<string, ImmutableDictionary<string, string>> responseStrings;
        private readonly Logger log;

        /// <summary>
        /// Used as failsafe in case response key doesn't exist in the selected or default language.
        /// </summary>
        private readonly CultureInfo usCultureInfo = new CultureInfo("en-US");
        private readonly ILocalization localization;

        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotStrings"/> class.
        /// </summary>
        /// <param name="loc">The localization for the strings.</param>
        public PotatoBotStrings(ILocalization loc)
        {
            this.log = LogManager.GetCurrentClassLogger();
            this.localization = loc;

            var sw = Stopwatch.StartNew();
            var allLangsDict = new Dictionary<string, ImmutableDictionary<string, string>>(); // lang:(name:value)

            this.responseStrings = allLangsDict.ToImmutableDictionary();
            sw.Stop();

            this.log.Info(
                "Loaded {0} languages in {1:F2}s",
                this.responseStrings.Count,
                sw.Elapsed.TotalSeconds);
        }

        /// <summary>
        /// Gets a potato bot string.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <param name="guildId">The guild id of the discord server.</param>
        /// <param name="lowerModuleTypeName">Lower module type name.</param>
        /// <param name="replacements">String replacement.</param>
        /// <returns>The string value identified by the key.</returns>
        public string GetText(string key, ulong? guildId, string lowerModuleTypeName, params object[] replacements) =>
            this.GetText(key, this.localization.GetCultureInfo(guildId), lowerModuleTypeName, replacements);

        /// <summary>
        /// Gets a potato bot string.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <param name="cultureInfo">The culture information from the guild.</param>
        /// <param name="lowerModuleTypeName">Lower module type name.</param>
        /// <returns>The string value identified by the key.</returns>
        public string GetText(string key, CultureInfo cultureInfo, string lowerModuleTypeName)
        {
            var text = this.GetString(lowerModuleTypeName + "_" + key, cultureInfo);

            if (string.IsNullOrWhiteSpace(text))
            {
                LogManager.GetCurrentClassLogger().Warn(lowerModuleTypeName + "_" + key + " key is missing from " + cultureInfo + " response strings. You may ignore this message.");
                text = this.GetString(lowerModuleTypeName + "_" + key, this.usCultureInfo) ?? $"Error: dkey {lowerModuleTypeName + "_" + key} not found!";
                if (string.IsNullOrWhiteSpace(text))
                {
                    return "I can't tell you if the command is executed, because there was an error printing out the response. Key '" +
                        lowerModuleTypeName + "_" + key + "' " + "is missing from resources. You may ignore this message.";
                }
            }

            return text;
        }

        /// <summary>
        /// Gets a potato bot string.
        /// </summary>
        /// <param name="key">The key of the string.</param>
        /// <param name="cultureInfo">The culture information from the guild.</param>
        /// <param name="lowerModuleTypeName">Lower module type name.</param>
        /// <param name="replacements">String replacement.</param>
        /// <returns>The string value identified by the key.</returns>
        public string GetText(string key, CultureInfo cultureInfo, string lowerModuleTypeName, params object[] replacements)
        {
            try
            {
                return string.Format(this.GetText(key, cultureInfo, lowerModuleTypeName), replacements);
            }
            catch (FormatException)
            {
                return "I can't tell you if the command is executed, because there was an error printing out the response. Key '" +
                       lowerModuleTypeName + "_" + key + "' " + "is not properly formatted. Please report this.";
            }
        }

        private static string GetLocaleName(string fileName)
        {
            var dotIndex = fileName.IndexOf('.') + 1;
            var secondDotIndex = fileName.LastIndexOf('.');
            return fileName.Substring(dotIndex, secondDotIndex - dotIndex);
        }

        private string GetString(string text, CultureInfo cultureInfo)
        {
            if (!this.responseStrings.TryGetValue(cultureInfo.Name.ToUpperInvariant(), out ImmutableDictionary<string, string> strings))
            {
                return null;
            }

            strings.TryGetValue(text, out string val);
            return val;
        }
    }
}
