﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// A Class for getting data related to guild configs.
    /// </summary>
    public class GuildConfigRepository : Repository<GuildConfig>, IGuildConfigRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GuildConfigRepository"/> class.
        /// </summary>
        /// <param name="context">The database context used for the repository.</param>
        public GuildConfigRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public IEnumerable<GuildConfig> GetAllGuildConfigs(List<ulong> availableGuilds) =>
            this.IncludeEverything()
                .Where(gc => availableGuilds.Contains(gc.GuildId))
                .ToList();

        /// <inheritdoc/>
        public IEnumerable<GuildConfig> PermissionsForAll(List<ulong> include)
        {
            var query = this.Set
                .Where(x => include.Contains(x.GuildId))
                .Include(gc => gc.Permissions);

            return query.ToList();
        }

        /// <inheritdoc/>
        public GuildConfig GcWithPermissionsFor(ulong guildId)
        {
            var config = this.Set
                .Where(gc => gc.GuildId == guildId)
                .Include(gc => gc.Permissions)
                .FirstOrDefault();

            // if there is no guildconfig, create new one
            if (config == null)
            {
                this.Set.Add(config = new GuildConfig
                {
                    GuildId = guildId,
                    Permissions = Permission.GetDefaultPermlist,
                });
                this.Context.SaveChanges();
            }

            // if no perms, add default ones
            else if (config.Permissions == null || !config.Permissions.Any())
            {
                config.Permissions = Permission.GetDefaultPermlist;
                this.Context.SaveChanges();
            }

            return config;
        }

        /// <inheritdoc/>
        public GuildConfig ForId(ulong guildId, Func<DbSet<GuildConfig>, IQueryable<GuildConfig>> includes = null)
        {
            GuildConfig config;

            if (includes == null)
            {
                config = this.IncludeEverything()
                    .FirstOrDefault(c => c.GuildId == guildId);
            }
            else
            {
                var set = includes(this.Set);
                config = set.FirstOrDefault(c => c.GuildId == guildId);
            }

            if (config == null)
            {
                this.Set.Add(config = new GuildConfig
                {
                    GuildId = guildId,
                });
                this.Context.SaveChanges();
            }

            return config;
        }

        private IQueryable<GuildConfig> IncludeEverything()
        {
            return this.Set;
        }
    }
}
