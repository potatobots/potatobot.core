﻿namespace PotatoBot.Core.Service.TypeReaders
{
    using Discord.Commands;
    using Discord.WebSocket;

    /// <summary>
    /// Base class for type readers.
    /// </summary>
    /// <typeparam name="T">Generic type.</typeparam>
    public abstract class PotatoBotTypeReader<T> : TypeReader
    {
        private readonly DiscordSocketClient client;
        private readonly CommandService cmds;

        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotTypeReader{T}"/> class.
        /// </summary>
        /// <param name="client">Discord api client.</param>
        /// <param name="cmds">The command service.</param>
        protected PotatoBotTypeReader(DiscordSocketClient client, CommandService cmds)
        {
            this.client = client;
            this.cmds = cmds;
        }

        private PotatoBotTypeReader()
        {
        }
    }
}
