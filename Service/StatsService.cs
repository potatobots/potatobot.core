﻿namespace PotatoBot.Core.Service
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;
    using PotatoBot.Core.Common.Extensions;

    /// <summary>
    /// Class implementation of the <see cref="IStatsService"/> interface.
    /// </summary>
    public class StatsService : IStatsService
    {
        private readonly DiscordSocketClient client;
        private readonly DateTime started;

        private long textChannels;
        private long voiceChannels;
        private long messageCounter;
        private long commandsRan;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsService"/> class.
        /// </summary>
        /// <param name="client">The discord client.</param>
        /// <param name="cmdHandler">The command handler.</param>
        public StatsService(
            DiscordSocketClient client,
            CommandHandler cmdHandler)
        {
            this.client = client;

            this.started = DateTime.UtcNow;
            this.client.MessageReceived += _ => Task.FromResult(Interlocked.Increment(ref this.messageCounter));
            cmdHandler.CommandExecuted += (_, e) => Task.FromResult(Interlocked.Increment(ref this.commandsRan));

            this.client.ChannelCreated += (c) =>
            {
                Task.Run(() =>
                {
                    switch (c)
                    {
                        case ITextChannel _:
                            Interlocked.Increment(ref this.textChannels);
                            break;
                        case IVoiceChannel _:
                            Interlocked.Increment(ref this.voiceChannels);
                            break;
                    }
                });

                return Task.CompletedTask;
            };

            this.client.ChannelDestroyed += (c) =>
            {
                Task.Run(() =>
                {
                    switch (c)
                    {
                        case ITextChannel _:
                            Interlocked.Decrement(ref this.textChannels);
                            break;
                        case IVoiceChannel _:
                            Interlocked.Decrement(ref this.voiceChannels);
                            break;
                    }
                });

                return Task.CompletedTask;
            };

            this.client.GuildAvailable += (g) =>
            {
                Task.Run(() =>
                {
                    var tc = g.Channels.Count(cx => cx is ITextChannel);
                    var vc = g.Channels.Count - tc;
                    Interlocked.Add(ref this.textChannels, tc);
                    Interlocked.Add(ref this.voiceChannels, vc);
                });
                return Task.CompletedTask;
            };

            this.client.JoinedGuild += (g) =>
            {
                Task.Run(() =>
                {
                    var tc = g.Channels.Count(cx => cx is ITextChannel);
                    var vc = g.Channels.Count - tc;
                    Interlocked.Add(ref this.textChannels, tc);
                    Interlocked.Add(ref this.voiceChannels, vc);
                });
                return Task.CompletedTask;
            };

            this.client.GuildUnavailable += (g) =>
            {
                Task.Run(() =>
                {
                    var tc = g.Channels.Count(cx => cx is ITextChannel);
                    var vc = g.Channels.Count - tc;
                    Interlocked.Add(ref this.textChannels, -tc);
                    Interlocked.Add(ref this.voiceChannels, -vc);
                });

                return Task.CompletedTask;
            };

            this.client.LeftGuild += (g) =>
            {
                Task.Run(() =>
                {
                    var tc = g.Channels.Count(cx => cx is ITextChannel);
                    var vc = g.Channels.Count - tc;
                    Interlocked.Add(ref this.textChannels, -tc);
                    Interlocked.Add(ref this.voiceChannels, -vc);
                });

                return Task.CompletedTask;
            };
        }

        /// <inheritdoc/>
        public string BotVersion => "1.0.0";

        /// <inheritdoc/>
        public string Author => "Cryptic#2098";

        /// <inheritdoc/>
        public string Library => "Discord.Net";

        /// <inheritdoc/>
        public string Heap => Math.Round((double)GC.GetTotalMemory(false) / 1.MiB(), 2)
            .ToString(CultureInfo.InvariantCulture);

        /// <inheritdoc/>
        public double MessagesPerSecond => this.MessageCounter / this.GetUptime().TotalSeconds;

        /// <inheritdoc/>
        public long TextChannels => Interlocked.Read(ref this.textChannels);

        /// <inheritdoc/>
        public long VoiceChannels => Interlocked.Read(ref this.voiceChannels);

        /// <inheritdoc/>
        public long MessageCounter => Interlocked.Read(ref this.messageCounter);

        /// <inheritdoc/>
        public long CommandsRan => Interlocked.Read(ref this.commandsRan);

        /// <inheritdoc/>
        public void Initialize()
        {
            var guilds = this.client.Guilds.ToArray();
            this.textChannels = guilds.Sum(g => g.Channels.Count(cx => cx is ITextChannel));
            this.voiceChannels = guilds.Sum(g => g.Channels.Count(cx => cx is IVoiceChannel));
        }

        /// <inheritdoc/>
        public TimeSpan GetUptime() =>
            DateTime.UtcNow - this.started;

        /// <inheritdoc/>
        public string GetUptimeString(string separator = ", ")
        {
            var time = this.GetUptime();
            return $"{time.Days} days{separator}{time.Hours} hours{separator}{time.Minutes} minutes";
        }
    }
}
