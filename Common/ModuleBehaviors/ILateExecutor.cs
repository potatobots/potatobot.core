﻿namespace PotatoBot.Core.Common.ModuleBehaviors
{
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;

    /// <summary>
    /// Last thing to be executed, won't stop further executions.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public interface ILateExecutor
    {
        Task LateExecute(DiscordSocketClient client, IGuild guild, IUserMessage msg);
    }
}
