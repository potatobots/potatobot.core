﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System.Linq;
    using Discord;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Implementation of the <see cref="IDiscordUserRepository"/> interface.
    /// </summary>
    public class DiscordUserRepository : Repository<DiscordUser>, IDiscordUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DiscordUserRepository"/> class.
        /// </summary>
        /// <param name="context">The database context to use.</param>
        public DiscordUserRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void EnsureCreated(ulong userId, string username, string discriminator, string avatarId)
        {
            this.Context.Add(new DiscordUser(userId, username, discriminator, avatarId));
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public DiscordUser GetOrCreate(ulong userId, string username, string discriminator, string avatarId)
        {
            this.EnsureCreated(userId, username, discriminator, avatarId);
            return this.Set.First(u => u.UserId == userId);
        }

        /// <inheritdoc/>
        public DiscordUser GetOrCreate(IUser original)
            => this.GetOrCreate(original.Id, original.Username, original.Discriminator, original.AvatarId);
    }
}