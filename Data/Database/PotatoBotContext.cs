﻿namespace PotatoBot.Core.Data.Database
{
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Context Class for the potato bot database.
    /// </summary>
    public class PotatoBotContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotContext"/> class.
        /// </summary>
        /// <param name="options">Options for the context.</param>
        public PotatoBotContext(DbContextOptions<PotatoBotContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the guild configs.
        /// </summary>
        public DbSet<GuildConfig> GuildConfigs { get; set; }

        /// <summary>
        /// Gets or sets the discord users.
        /// </summary>
        public DbSet<DiscordUser> DiscordUsers { get; set; }

        /// <summary>
        /// Gets or sets the bot configs repo.
        /// </summary>
        public DbSet<BotConfig> BotConfigs { get; set; }
    }
}