﻿namespace PotatoBot.Core.Common
{
    using System.Collections.Generic;
    using Discord;

    /// <summary>
    /// A Class to compare <see cref="IGuildUser"/>.
    /// </summary>
    public class GuildUserComparer : IEqualityComparer<IGuildUser>
    {
        /// <summary>
        /// Checks if two guild users are equal.
        /// </summary>
        /// <param name="x">The fist <see cref="IGuildUser"/>.</param>
        /// <param name="y">The second <see cref="IGuildUser"/>.</param>
        /// <returns>A boolean indicating if the two <see cref="IGuildUser"/>'s are the same.</returns>
        public bool Equals(IGuildUser x, IGuildUser y) => x.Id == y.Id;

        /// <inheritdoc/>
        public int GetHashCode(IGuildUser obj) => obj.Id.GetHashCode();
    }
}
