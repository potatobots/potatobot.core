﻿namespace PotatoBot.Core.Common
{
    using System;

    /// <summary>
    /// Class providing processor information.
    /// </summary>
    public static class PlatformHelper
    {
        private const int ProcessorCountRefreshIntervalMs = 30000;

        private static volatile int processorCount;
        private static volatile int lastProcessorCountRefreshTicks;

        /// <summary>
        /// Gets the processor count.
        /// </summary>
        public static int ProcessorCount
        {
            get
            {
                var now = Environment.TickCount;
                if (processorCount == 0 || (now - lastProcessorCountRefreshTicks) >= PlatformHelper.ProcessorCountRefreshIntervalMs)
                {
                    processorCount = Environment.ProcessorCount;
                    lastProcessorCountRefreshTicks = now;
                }

                return processorCount;
            }
        }
    }
}
