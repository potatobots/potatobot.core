﻿namespace PotatoBot.Core.Data.Database.Models
{
    using Discord;

    /// <summary>
    /// A model for playing statuses.
    /// </summary>
    public class PlayingStatus : DbEntity
    {
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the activity type.
        /// </summary>
        public ActivityType Type { get; set; }
    }
}
