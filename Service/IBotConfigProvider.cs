﻿namespace PotatoBot.Core.Service
{
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Interface for providing bot configs.
    /// </summary>
    public interface IBotConfigProvider
    {
        /// <summary>
        /// Gets a bot config.
        /// </summary>
        BotConfig BotConfig { get; }

        /// <summary>
        /// Reload the config.
        /// </summary>
        void Reload();

        /// <summary>
        /// Edit the config.
        /// </summary>
        /// <param name="type">Set the edit type.</param>
        /// <param name="newValue">Set the new value.</param>
        /// <returns>A boolean indicating if the edit was successful.</returns>
        bool Edit(BotConfigEditType type, string newValue);
    }
}
