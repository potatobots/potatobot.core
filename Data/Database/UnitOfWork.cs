﻿namespace PotatoBot.Core.Data.Database
{
    using System;
    using System.Threading.Tasks;
    using NadekoBot.Core.Services.Database.Repositories.Impl;
    using PotatoBot.Core.Data.Database.Repositories;

    /// <summary>
    /// A configuration class for available repositories.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The db context to use.</param>
        public UnitOfWork(PotatoBotContext context)
        {
            this.Context = context;
            this.GuildConfigs = new GuildConfigRepository(this.Context);
            this.DiscordUsers = new DiscordUserRepository(this.Context);
            this.BotConfig = new BotConfigRepository(this.Context);
        }

        /// <inheritdoc/>
        public PotatoBotContext Context { get; }

        /// <inheritdoc/>
        public IGuildConfigRepository GuildConfigs { get; }

        /// <inheritdoc/>
        public IDiscordUserRepository DiscordUsers { get; }

        /// <inheritdoc/>
        public IBotConfigRepository BotConfig { get; }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Context.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc/>
        public int SaveChanges() =>
            this.Context.SaveChanges();

        /// <inheritdoc/>
        public Task<int> SaveChangesAsync() =>
            this.Context.SaveChangesAsync();
    }
}
