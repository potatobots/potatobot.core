﻿namespace PotatoBot.Core.Service
{
    using System.Collections.Concurrent;
    using System.Globalization;
    using Discord;
    using PotatoBot.Core.Common;

    /// <summary>
    /// An interface for localization.
    /// </summary>
    public interface ILocalization : INService
    {
        /// <summary>
        /// Gets the default culture information.
        /// </summary>
        CultureInfo DefaultCultureInfo { get; }

        /// <summary>
        /// Gets the guild culture information dictionary.
        /// </summary>
        ConcurrentDictionary<ulong, CultureInfo> GuildCultureInfos { get; }

        /// <summary>
        /// Gets the culture info from a guild.
        /// </summary>
        /// <param name="guild">The guild indicating which culture to get.</param>
        /// <returns><see cref="CultureInfo"/> from the guild.</returns>
        CultureInfo GetCultureInfo(IGuild guild);

        /// <summary>
        /// Gets the culture info from a guild identified by id.
        /// </summary>
        /// <param name="guildId">The guild id indicating which culture to get.</param>
        /// <returns><see cref="CultureInfo"/> from the guild.</returns>
        CultureInfo GetCultureInfo(ulong? guildId);

        /// <summary>
        /// Remove a guild culture.
        /// </summary>
        /// <param name="guild">The guild indicating which culture to delete.</param>
        void RemoveGuildCulture(IGuild guild);

        /// <summary>
        /// Removes a guild culture identified by guild id.
        /// </summary>
        /// <param name="guildId">The guild id indicating which culture to delete.</param>
        void RemoveGuildCulture(ulong guildId);

        /// <summary>
        /// Reset the default culture.
        /// </summary>
        void ResetDefaultCulture();

        /// <summary>
        /// Set a default culture.
        /// </summary>
        /// <param name="ci">The new default culture.</param>
        void SetDefaultCulture(CultureInfo ci);

        /// <summary>
        /// Set a culture to a guild.
        /// </summary>
        /// <param name="guild">The guild which needs to update.</param>
        /// <param name="ci">The <see cref="CultureInfo"/> that will be used to update the guild.</param>
        void SetGuildCulture(IGuild guild, CultureInfo ci);

        /// <summary>
        /// Set a culture to a guild identified by id.
        /// </summary>
        /// <param name="guildId">The guild id indicating the guild that needs to be updated.</param>
        /// <param name="ci">The <see cref="CultureInfo"/> that will be used to update the guild.</param>
        void SetGuildCulture(ulong guildId, CultureInfo ci);
    }
}