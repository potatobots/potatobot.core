﻿namespace PotatoBot.Core.Service.TypeReaders
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Discord.Commands;
    using Discord.WebSocket;
    using Microsoft.Extensions.DependencyInjection;
    using PotatoBot.Core.Service;

    /// <summary>
    /// A class functioning as a command type reader.
    /// </summary>
    public class CommandTypeReader : PotatoBotTypeReader<CommandInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandTypeReader"/> class.
        /// </summary>
        /// <param name="client">Discord bot api client.</param>
        /// <param name="cmds">Command service.</param>
        public CommandTypeReader(DiscordSocketClient client, CommandService cmds)
            : base(client, cmds)
        {
        }

        /// <inheritdoc/>
        public override Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, IServiceProvider services)
        {
            var cmds = services.GetService<CommandService>();
            var cmdHandler = services.GetService<CommandHandler>();
            input = input.ToUpperInvariant();
            var prefix = cmdHandler.GetPrefix(context.Guild);
            if (!input.StartsWith(prefix.ToUpperInvariant(), StringComparison.InvariantCulture))
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "No such command found."));
            }

            input = input.Substring(prefix.Length);

            var cmd = cmds.Commands.FirstOrDefault(c =>
                c.Aliases.Select(a => a.ToUpperInvariant()).Contains(input));
            if (cmd == null)
            {
                return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "No such command found."));
            }

            return Task.FromResult(TypeReaderResult.FromSuccess(cmd));
        }
    }
}
