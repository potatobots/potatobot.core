﻿namespace PotatoBot.Core.Data.Database.Models
{
    /// <summary>
    /// Enum indicating blacklist types.
    /// </summary>
    public enum BlacklistType
    {
        /// <summary>
        /// Server type.
        /// </summary>
        Server,

        /// <summary>
        /// Channel type.
        /// </summary>
        Channel,

        /// <summary>
        /// User type.
        /// </summary>
        User,
    }

    /// <summary>
    /// A model for black list items.
    /// </summary>
    public class BlacklistItem : DbEntity
    {
        /// <summary>
        /// Gets or sets the item id.
        /// </summary>
        public ulong ItemId { get; set; }

        /// <summary>
        /// Gets or sets the blacklist type.
        /// </summary>
        public BlacklistType Type { get; set; }
    }
}
