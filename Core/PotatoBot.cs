﻿namespace PotatoBot.Core.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using global::PotatoBot.Core.Common;
    using global::PotatoBot.Core.Common.Configs.Credentials;
    using global::PotatoBot.Core.Common.Extensions;
    using global::PotatoBot.Core.Common.Logging;
    using global::PotatoBot.Core.Common.ShardCom;
    using global::PotatoBot.Core.Data.Database.Models;
    using global::PotatoBot.Core.Service;
    using global::PotatoBot.Core.Service.Database;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using NLog;

    /// <summary>
    /// The Core class for starting a potato bot.
    /// </summary>
    public class PotatoBot : IPotatoBot
    {
        private readonly Logger log;
        private readonly DbService db;
        private readonly BotConfig botConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBot"/> class.
        /// </summary>
        /// <param name="shardId">Shard ID.</param>
        /// <param name="parentProcessId">Parent Process ID.</param>
        public PotatoBot(int shardId, int parentProcessId)
        {
            if (shardId < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(shardId));
            }

            LogSetup.SetupLogger(shardId);
            this.log = LogManager.GetCurrentClassLogger();

            this.Credentials = new BotCredentials();
            this.db = new DbService(this.Credentials);

            if (shardId == 0)
            {
                this.db.Setup();
            }

            this.Client = new DiscordSocketClient();

            this.CommandService = new CommandService(new CommandServiceConfig()
            {
                CaseSensitiveCommands = false,
                DefaultRunMode = RunMode.Sync,
            });

            using (var uow = this.db.GetDbContext())
            {
                this.botConfig = uow.BotConfig.GetOrCreate();
                PotatoBotStatics.OkColor = new Color(Convert.ToUInt32(this.botConfig.OkColor, 16));
                PotatoBotStatics.ErrorColor = new Color(Convert.ToUInt32(this.botConfig.ErrorColor, 16));
                uow.SaveChanges();
            }

            PotatoBot.SetupShard(parentProcessId);
            this.Client.Log += this.Client_Log;
        }

        /// <inheritdoc/>
        public event Func<GuildConfig, Task> JoinedGuild = arg => Task.CompletedTask;

        /// <inheritdoc/>
        public ImmutableArray<GuildConfig> AllGuildConfigs { get; private set; }

        /// <summary>
        /// Gets the bot credentials.
        /// </summary>
        public BotCredentials Credentials { get; }

        /// <summary>
        /// Gets the discord client socket.
        /// </summary>
        public DiscordSocketClient Client { get; }

        /// <summary>
        /// Gets the command service.
        /// </summary>
        public CommandService CommandService { get; }

        /// <summary>
        /// Gets the service provider.
        /// </summary>
        public IServiceProvider Services { get; private set; }

        /// <inheritdoc/>
        public TaskCompletionSource<bool> Ready { get; set; } = new TaskCompletionSource<bool>();

        /// <summary>
        /// Run the bot and block the task.
        /// </summary>
        /// <returns>A <see cref="Task"/>.</returns>
        public async Task RunAndBlockAsync()
        {
            await this.RunAsync(null).ConfigureAwait(false);
            await Task.Delay(-1).ConfigureAwait(false);
        }

        /// <summary>
        /// Run the bot and block the task.
        /// </summary>
        /// <param name="modules">The assembly of the additional from another project.</param>
        /// <returns>A <see cref="Task"/>.</returns>
        public async Task RunAndBlockAsync(Assembly modules)
        {
            await this.RunAsync(modules).ConfigureAwait(false);
            await Task.Delay(-1).ConfigureAwait(false);
        }

        /// <summary>
        /// Stop the bot.
        /// </summary>
        /// <returns>A <see cref="Task"/>.</returns>
        public async Task StopAsync()
        {
            await this.Client.StopAsync().ConfigureAwait(false);
        }

        private static void SetupShard(int parentProcessId)
        {
            new Thread(new ThreadStart(() =>
            {
                try
                {
                    var p = Process.GetProcessById(parentProcessId);
                    if (p == null)
                    {
                        return;
                    }

                    p.WaitForExit();
                }
                finally
                {
                    Environment.Exit(10);
                }
            })).Start();
        }

        /// <summary>
        /// Run the bot async. Beware this task does not block at the end.
        /// </summary>
        /// <returns>The task of the running bot.</returns>
        private async Task RunAsync(Assembly modules)
        {
            var sw = Stopwatch.StartNew();

            await this.LoginAsync(this.Credentials.Token).ConfigureAwait(false);

            this.log.Info($"Shard {this.Client.ShardId} loading services...");
            try
            {
                this.AddServices(modules);
            }
            catch (Exception ex)
            {
                this.log.Error(ex);
                throw;
            }

            sw.Stop();
            this.log.Info($"Shard {this.Client.ShardId} connected in {sw.Elapsed.TotalSeconds:F2}s");

            var stats = this.Services.GetService<IStatsService>();
            stats.Initialize();
            var commandHandler = this.Services.GetService<CommandHandler>();
            var commandService = this.Services.GetService<CommandService>();

            // start handling messages received in command handler
            await commandHandler.StartHandling().ConfigureAwait(false);
            var assembly = this.GetType().GetTypeInfo().Assembly;
            await commandService.AddModulesAsync(typeof(Modules.Module).Assembly, this.Services).ConfigureAwait(false);

            // Add external modules to the command service.
            if (assembly != null)
            {
                await commandService.AddModuleAsync(modules.GetType(), this.Services).ConfigureAwait(false);
            }

            // this.HandleStatusChanges();
            this.StartSendingData();
            this.Ready.TrySetResult(true);

            this.log.Info($"Shard {this.Client.ShardId} ready.");
        }

        private void StartSendingData()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    var data = new ShardComMessage()
                    {
                        ConnectionState = this.Client.ConnectionState,
                        Guilds = this.Client.ConnectionState == ConnectionState.Connected ? this.Client.Guilds.Count : 0,
                        ShardId = this.Client.ShardId,
                        Time = DateTime.UtcNow,
                    };

                    // var sub = Cache.Redis.GetSubscriber();
                    var msg = JsonConvert.SerializeObject(data);

                    await Task.Delay(7500).ConfigureAwait(false);
                }
            });
        }

        private List<ulong> GetCurrentGuildIds()
        {
            return this.Client.Guilds.Select(x => x.Id).ToList();
        }

        private async Task LoginAsync(string token)
        {
            var clientReady = new TaskCompletionSource<bool>();

            Task SetClientReady()
            {
                Task.Run(
                    async () =>
                    {
                        clientReady.TrySetResult(true);
                        try
                        {
                            foreach (var chan in await this.Client.GetDMChannelsAsync().ConfigureAwait(false))
                            {
                                await chan.CloseAsync().ConfigureAwait(false);
                            }
                        }
                        catch
                        {
                            // ignored
                        }
                    });
                return Task.CompletedTask;
            }

            // Connect.
            this.log.Info("Shard {0} logging in ...", this.Client.ShardId);

            await this.Client.LoginAsync(TokenType.Bot, token).ConfigureAwait(false);
            await this.Client.StartAsync().ConfigureAwait(false);

            this.Client.Ready += SetClientReady;
            await clientReady.Task.ConfigureAwait(false);
            this.Client.Ready -= SetClientReady;

            this.Client.JoinedGuild += this.Client_JoinedGuild;
            this.Client.LeftGuild += this.Client_LeftGuild;

            this.log.Info("Shard {0} logged in.", this.Client.ShardId);
        }

        private Task Client_LeftGuild(SocketGuild arg)
        {
            this.log.Info("Left server: {0} [{1}]", arg?.Name, arg?.Id);
            return Task.CompletedTask;
        }

        private Task Client_JoinedGuild(SocketGuild arg)
        {
            this.log.Info("Joined server: {0} [{1}]", arg?.Name, arg?.Id);
            Task.Run(async () =>
            {
                GuildConfig gc;
                using (var uow = this.db.GetDbContext())
                {
                    gc = uow.GuildConfigs.ForId(arg.Id);
                }

                await this.JoinedGuild.Invoke(gc).ConfigureAwait(false);
            });
            return Task.CompletedTask;
        }

        private void AddServices(Assembly modules)
        {
            var startingGuildIdList = this.GetCurrentGuildIds();

            // this unit of work will be used for initialization of all modules too, to prevent multiple queries from running
            using (var uow = this.db.GetDbContext())
            {
                var sw = Stopwatch.StartNew();

                var bot = this.Client.CurrentUser;

                uow.DiscordUsers.EnsureCreated(bot.Id, bot.Username, bot.Discriminator, bot.AvatarId);

                this.AllGuildConfigs = uow.GuildConfigs.GetAllGuildConfigs(startingGuildIdList).ToImmutableArray();

                IBotConfigProvider botConfigProvider = new BotConfigProvider(this.db, this.botConfig);
                IPotatoBot potatoBot = this;

                var s = new ServiceCollection()
                    .AddSingleton<IBotCredentials>(this.Credentials)
                    .AddSingleton(this.db)
                    .AddSingleton(this.Client)
                    .AddSingleton(this.CommandService)
                    .AddSingleton(botConfigProvider)
                    .AddSingleton(potatoBot)
                    .AddSingleton(uow)
                    .AddMemoryCache();

                s.AddHttpClient();

                // s.AddHttpClient("memelist").ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                // {
                //    AllowAutoRedirect = false,
                // });
                s.LoadFrom(Assembly.GetAssembly(typeof(CommandHandler)));
                s.LoadFrom(Assembly.GetAssembly(typeof(Modules.Module)));

                if (modules != null)
                {
                    s.LoadFrom(modules);
                }

                // Initialize Services
                this.Services = s.BuildServiceProvider();
                var commandHandler = this.Services.GetService<CommandHandler>();

                commandHandler.AddServices(s);
                this.LoadTypeReaders(AppDomain.CurrentDomain.GetAssemblies());

                sw.Stop();
                this.log.Info($"All services loaded in {sw.Elapsed.TotalSeconds:F2}s");
            }
        }

        private IEnumerable<object> LoadTypeReaders(Assembly[] assemblies)
        {
            Type[] allTypes;

            try
            {
                var list = new List<Type>();
                foreach (var assembly in assemblies)
                {
                    list.AddRange(assembly.GetTypes());
                }

                allTypes = list.ToArray();
            }
            catch (ReflectionTypeLoadException ex)
            {
                this.log.Warn(ex.LoaderExceptions[0]);
                return Enumerable.Empty<object>();
            }

            var filteredTypes = allTypes
                .Where(x => x.BaseType != null && x.IsSubclassOf(typeof(TypeReader))
                                                   && x.BaseType.GetGenericArguments().Length > 0
                                                   && !x.IsAbstract);

            var toReturn = new List<object>();
            foreach (var ft in filteredTypes)
            {
                var x = (TypeReader)Activator.CreateInstance(ft, this.Client, this.CommandService);
                var baseType = ft.BaseType;
                if (baseType != null)
                {
                    var typeArgs = baseType.GetGenericArguments();
                    try
                    {
                        this.CommandService.AddTypeReader(typeArgs[0], x);
                    }
                    catch (Exception ex)
                    {
                        this.log.Error(ex);
                        throw;
                    }
                }

                toReturn.Add(x);
            }

            return toReturn;
        }

        private Task Client_Log(LogMessage arg)
        {
            this.log.Warn(arg.Source + " | " + arg.Message);
            if (arg.Exception != null)
            {
                this.log.Warn(arg.Exception);
            }

            return Task.CompletedTask;
        }
    }
}
