﻿namespace PotatoBot.Core.Modules
{
    using PotatoBot.Core.Common;

    /// <summary>
    /// A class for a sub module.
    /// </summary>
    public abstract class Submodule : Module
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Submodule"/> class.
        /// </summary>
        protected Submodule()
            : base(false)
        {
        }
    }

    /// <summary>
    /// Extension on the sub module using a <see cref="INService"/>.
    /// </summary>
    /// <typeparam name="TService">The service that needs to be included in the sub module.</typeparam>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:File may only contain a single type", Justification = "It's an extension on the other class. No need to move it to separate file.")]
    public abstract class Submodule<TService> : Module<TService>
        where TService : INService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Submodule{TService}"/> class.
        /// </summary>
        protected Submodule()
            : base(false)
        {
        }
    }
}
