﻿namespace PotatoBot.Core.Service.Database
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using Microsoft.Extensions.Logging;
    using PotatoBot.Core.Common.Configs.Credentials;
    using PotatoBot.Core.Data.Database;
    using LogLevel = Microsoft.Extensions.Logging.LogLevel;

    /// <summary>
    /// A Class for providing database related services.
    /// </summary>
    public class DbService
    {
        private static readonly ILoggerFactory LoggerFactory =
            Microsoft.Extensions.Logging.LoggerFactory.Create(
                builder =>
                {
                    builder
                        .AddFilter("Microsoft", LogLevel.Warning)
                        .AddFilter("System", LogLevel.Warning)
                        .AddFilter(LoggerCategory<DbLoggerCategory.Database.Command>.Name, LogLevel.Information)
                        .AddConsole();
                });

        private readonly DbContextOptions<PotatoBotContext> options;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbService"/> class.
        /// </summary>
        /// <param name="credentials">The credentials used to access the database.</param>
        public DbService(IBotCredentials credentials)
        {
            var optionsBuilder = PotatoBotContextFactory.InitializeBuilder(credentials);
            this.options = optionsBuilder.Options;
        }

        /// <summary>
        /// Setup the database context.
        /// </summary>
        public void Setup()
        {
            using (var context = new PotatoBotContext(this.options))
            {
                if (context.Database.GetPendingMigrations().Any())
                {
                    var mContext = new PotatoBotContext(this.options);
                    mContext.Database.Migrate();
                    mContext.SaveChanges();
                    mContext.Dispose();
                }

                context.SaveChanges();
            }
        }

        /// <summary>
        /// Gets the db context.
        /// </summary>
        /// <returns>The context as a <see cref="IUnitOfWork"/>.</returns>
        public IUnitOfWork GetDbContext()
        {
            return new UnitOfWork(this.GetDbContextInternal());
        }

        private PotatoBotContext GetDbContextInternal()
        {
            var context = new PotatoBotContext(this.options);
            context.Database.SetCommandTimeout(60);
            var conn = context.Database.GetDbConnection();
            conn.Open();

            return context;
        }
    }
}
