﻿namespace PotatoBot.Core.Modules.Common.Replacement
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Modules.Administration.Services;
    using PotatoBot.Core.Service.Extensions;

    /// <summary>
    /// A class for managing the replacer.
    /// </summary>
    public class ReplacementBuilder
    {
        private static readonly Regex RngRegex = new Regex("%rng(?:(?<from>(?:-)?\\d+)-(?<to>(?:-)?\\d+))?%", RegexOptions.Compiled);
        private readonly ConcurrentDictionary<string, Func<string>> reps = new ConcurrentDictionary<string, Func<string>>();
        private readonly ConcurrentDictionary<Regex, Func<Match, string>> regex = new ConcurrentDictionary<Regex, Func<Match, string>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplacementBuilder"/> class.
        /// </summary>
        public ReplacementBuilder()
        {
            this.WithRngRegex();
        }

        /// <summary>
        /// Replace with default.
        /// </summary>
        /// <param name="usr">The discord user.</param>
        /// <param name="ch">The discord channel.</param>
        /// <param name="g">The discord api guild socket.</param>
        /// <param name="client">The discord api client.</param>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithDefault(IUser usr, IMessageChannel ch, SocketGuild g, DiscordSocketClient client)
        {
            return this.WithUser(usr)
                .WithChannel(ch)
                .WithServer(client, g)
                .WithClient(client);
        }

        /// <summary>
        /// Replace with default.
        /// </summary>
        /// <param name="ctx">The command context.</param>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithDefault(ICommandContext ctx) =>
            this.WithDefault(ctx.User, ctx.Channel, ctx.Guild as SocketGuild, (DiscordSocketClient)ctx.Client);

        /// <summary>
        /// Replace with mention.
        /// </summary>
        /// <param name="client">The Discord api client.</param>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithMention(DiscordSocketClient client)
        {
            this.reps.TryAdd("%bot.mention%", () => client.CurrentUser.Mention);
            return this;
        }

        /// <summary>
        /// Replace with client.
        /// </summary>
        /// <param name="client">The discord ap client.</param>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithClient(DiscordSocketClient client)
        {
            this.WithMention(client);

            this.reps.TryAdd("%bot.status%", () => client.Status.ToString());
            this.reps.TryAdd("%bot.latency%", () => client.Latency.ToString());
            this.reps.TryAdd("%bot.name%", () => client.CurrentUser.Username);
            this.reps.TryAdd("%bot.fullname%", () => client.CurrentUser.ToString());
            this.reps.TryAdd("%bot.time%", () => DateTime.Now.ToString("HH:mm " + TimeZoneInfo.Local.StandardName.GetInitials()));
            this.reps.TryAdd("%bot.discrim%", () => client.CurrentUser.Discriminator);
            this.reps.TryAdd("%bot.id%", () => client.CurrentUser.Id.ToString());
            this.reps.TryAdd("%bot.avatar%", () => client.CurrentUser.RealAvatarUrl()?.ToString());

            this.WithStats(client);
            return this;
        }

        /// <summary>
        /// Replace with server.
        /// </summary>
        /// <param name="client">The discord api client.</param>
        /// <param name="g">The discord guild socket.</param>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithServer(DiscordSocketClient client, SocketGuild g)
        {
            this.reps.TryAdd("%server.id%", () => g == null ? "DM" : g.Id.ToString());
            this.reps.TryAdd("%server.name%", () => g == null ? "DM" : g.Name);
            this.reps.TryAdd("%server.members%", () => g != null && g is SocketGuild sg ? sg.MemberCount.ToString() : "?");
            this.reps.TryAdd("%server.time%", () =>
            {
                TimeZoneInfo to = TimeZoneInfo.Local;
                if (g != null)
                {
                    if (GuildTimezoneService.AllServices.TryGetValue(client.CurrentUser.Id, out var tz))
                    {
                        to = tz.GetTimeZoneOrDefault(g.Id) ?? TimeZoneInfo.Local;
                    }
                }

                return TimeZoneInfo.ConvertTime(
                    DateTime.UtcNow,
                    TimeZoneInfo.Utc,
                    to).ToString("HH:mm ") + to.StandardName.GetInitials();
            });
            return this;
        }

        /// <summary>
        /// Replace with channel.
        /// </summary>
        /// <param name="ch">The discord channel.</param>
        /// <returns>a <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithChannel(IMessageChannel ch)
        {
            this.reps.TryAdd("%channel.mention%", () => (ch as ITextChannel)?.Mention ?? "#" + ch.Name);
            this.reps.TryAdd("%channel.name%", () => ch.Name);
            this.reps.TryAdd("%channel.id%", () => ch.Id.ToString());
            this.reps.TryAdd("%channel.created%", () => ch.CreatedAt.ToString("HH:mm dd.MM.yyyy"));
            this.reps.TryAdd("%channel.nsfw%", () => (ch as ITextChannel)?.IsNsfw.ToString() ?? "-");
            this.reps.TryAdd("%channel.topic%", () => (ch as ITextChannel)?.Topic ?? "-");
            return this;
        }

        /// <summary>
        /// Replace with user.
        /// </summary>
        /// <param name="user">The Discord user.</param>
        /// <returns>a <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithUser(IUser user)
        {
            this.reps.TryAdd("%user.mention%", () => user.Mention);
            this.reps.TryAdd("%user.fullname%", () => user.ToString());
            this.reps.TryAdd("%user.name%", () => user.Username);
            this.reps.TryAdd("%user.discrim%", () => user.Discriminator);
            this.reps.TryAdd("%user.avatar%", () => user.RealAvatarUrl()?.ToString());
            this.reps.TryAdd("%user.id%", () => user.Id.ToString());
            this.reps.TryAdd("%user.created_time%", () => user.CreatedAt.ToString("HH:mm"));
            this.reps.TryAdd("%user.created_date%", () => user.CreatedAt.ToString("dd.MM.yyyy"));
            this.reps.TryAdd("%user.joined_time%", () => (user as IGuildUser)?.JoinedAt?.ToString("HH:mm") ?? "-");
            this.reps.TryAdd("%user.joined_date%", () => (user as IGuildUser)?.JoinedAt?.ToString("dd.MM.yyyy") ?? "-");
            return this;
        }

        /// <summary>
        /// Replace with RNG.
        /// </summary>
        /// <returns>A <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithRngRegex()
        {
            var rng = new PotatoBotRandom();
            this.regex.TryAdd(RngRegex, (match) =>
            {
                if (!int.TryParse(match.Groups["from"].ToString(), out var from))
                {
                    from = 0;
                }

                if (!int.TryParse(match.Groups["to"].ToString(), out var to))
                {
                    to = 0;
                }

                if (from == 0 && to == 0)
                {
                    return rng.Next(0, 11).ToString();
                }

                if (from >= to)
                {
                    return string.Empty;
                }

                return rng.Next(from, to + 1).ToString();
            });
            return this;
        }

        /// <summary>
        /// Replace with override.
        /// </summary>
        /// <param name="key">Override key.</param>
        /// <param name="output">Override output.</param>
        /// <returns>a <see cref="ReplacementBuilder"/>.</returns>
        public ReplacementBuilder WithOverride(string key, Func<string> output)
        {
            this.reps.AddOrUpdate(key, output, delegate { return output; });
            return this;
        }

        /// <summary>
        /// Guild the replacer.
        /// </summary>
        /// <returns>A <see cref="Replacer"/>.</returns>
        public Replacer Build()
        {
            return new Replacer(this.reps.Select(x => (x.Key, x.Value)).ToArray(), this.regex.Select(x => (x.Key, x.Value)).ToArray());
        }

        private ReplacementBuilder WithStats(DiscordSocketClient c)
        {
            this.reps.TryAdd("%shard.servercount%", () => c.Guilds.Count.ToString());

            this.reps.TryAdd("%shard.id%", () => c.ShardId.ToString());
            return this;
        }
    }
}
