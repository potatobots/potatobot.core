﻿namespace PotatoBot.Core.Data.Database
{
    using System;
    using System.Threading.Tasks;
    using PotatoBot.Core.Data.Database.Repositories;

    /// <summary>
    /// Interface for unit of work.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Gets the database context.
        /// </summary>
        PotatoBotContext Context { get; }

        /// <summary>
        /// Gets the Guild config repo.
        /// </summary>
        IGuildConfigRepository GuildConfigs { get; }

        /// <summary>
        /// Gets the discord user repo.
        /// </summary>
        IDiscordUserRepository DiscordUsers { get; }

        /// <summary>
        /// Gets the bot config repository.
        /// </summary>
        IBotConfigRepository BotConfig { get; }

        /// <summary>
        /// Save changes.
        /// </summary>
        /// <returns>An integer indicating if it was successful or not.</returns>
        int SaveChanges();

        /// <summary>
        /// Save changes async.
        /// </summary>
        /// <returns>A task and integer indicating if it was successful or not.</returns>
        Task<int> SaveChangesAsync();
    }
}
