﻿namespace PotatoBot.Core.Common.CREmbed
{
    using Newtonsoft.Json;

    /// <summary>
    /// A class representing an embed author.
    /// </summary>
    public class CREmbedAuthor
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the icon url.
        /// </summary>
        public string IconUrl { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        [JsonProperty("icon_url")]
        private string Icon_Url { set => this.IconUrl = value; }
    }
}
