﻿namespace PotatoBot.Core.Common.Extensions
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Discord;
    using Discord.Commands;
    using Discord.WebSocket;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using NLog;

    /// <summary>
    /// Static class containing extentions methods.
    /// </summary>
    public static class Extensions
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Method to resolve conflict with <see cref="System.Linq"/> and <see cref="System.Collections"/>.
        /// </summary>
        /// <typeparam name="TEntity">Database entity.</typeparam>
        /// <param name="obj">The databse entity used for this conversion."/>.</param>
        /// <returns>A async enumerable list.</returns>
        public static IAsyncEnumerable<TEntity> AsAsyncEnumerable<TEntity>(this Microsoft.EntityFrameworkCore.DbSet<TEntity> obj)
            where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsAsyncEnumerable(obj);
        }

        /// <summary>
        /// Method to resolve conflict with <see cref="System.Linq"/> and <see cref="System.Collections"/>.
        /// </summary>
        /// <typeparam name="TEntity">Database entity.</typeparam>
        /// <param name="obj">The database set used for this function..</param>
        /// <param name="predicate">Predicate for the where statement.</param>
        /// <returns>A list of database entities where the predicate statement was true.</returns>
        public static IQueryable<TEntity> Where<TEntity>(this Microsoft.EntityFrameworkCore.DbSet<TEntity> obj, System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate)
            where TEntity : class
        {
            return Queryable.Where(obj, predicate);
        }

        /// <summary>
        /// Gets the real summary of a command.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <param name="prefix">The command prefix.</param>
        /// <returns>The real summary.</returns>
        public static string RealSummary(this CommandInfo cmd, string prefix) => string.Format(cmd.Summary, prefix);

        /// <summary>
        /// Gets the real remarks of a command.
        /// </summary>
        /// <param name="cmd">The command.</param>
        /// <param name="prefix">The command prefix.</param>
        /// <returns>The real remarks.</returns>
        public static string RealRemarks(this CommandInfo cmd, string prefix) => string.Join(" or ", JsonConvert.DeserializeObject<string[]>(cmd.Remarks).Select(x => Format.Code(string.Format(x, prefix))));

        /// <summary>
        /// Delete message after a certain amount of seconds.
        /// </summary>
        /// <param name="msg">The message that needs to be deleted.</param>
        /// <param name="seconds">The seconds interval for deleting the message.</param>
        /// <returns>The <see cref="IMessage"/> object that has been deleted.</returns>
        public static IMessage DeleteAfter(this IUserMessage msg, int seconds)
        {
            Task.Run(async () =>
            {
                await Task.Delay(seconds * 1000).ConfigureAwait(false);
                try
                {
                    await msg.DeleteAsync().ConfigureAwait(false);
                }
                catch
                {
                }
            });
            return msg;
        }

        /// <summary>
        /// Extension to the Discord Embed builder.
        /// </summary>
        /// <param name="embed">The discord Embed builder.</param>
        /// <param name="curPage">Integer indicating the current page.</param>
        /// <param name="lastPage">Integer indicating the last page.</param>
        /// <returns>An embed builder including a footer.</returns>
        public static EmbedBuilder AddPaginatedFooter(this EmbedBuilder embed, int curPage, int? lastPage)
        {
            if (lastPage != null)
            {
                return embed.WithFooter(efb => efb.WithText($"{curPage + 1} / {lastPage + 1}"));
            }

            return embed.WithFooter(efb => efb.WithText(curPage.ToString()));
        }

        /// <summary>
        /// Extension to the Discord Embed builder.
        /// </summary>
        /// <param name="eb">The discord Embed builder.</param>
        /// <returns>An embed builder including a color.</returns>
        public static EmbedBuilder WithOkColor(this EmbedBuilder eb)
        {
            return eb.WithColor(PotatoBotStatics.OkColor);
        }

        /// <summary>
        /// Extension to the Discord Embed builder.
        /// </summary>
        /// <param name="eb">The discord Embed builder.</param>
        /// <returns>An embed builder including a color.</returns>
        public static EmbedBuilder WithErrorColor(this EmbedBuilder eb)
        {
            return eb.WithColor(PotatoBotStatics.ErrorColor);
        }

        /// <summary>
        /// Extension to the discord ModuleInfo class.
        /// </summary>
        /// <param name="module">The <see cref="ModuleInfo"/> that is used to get the top level module.</param>
        /// <returns>The top level module.</returns>
        public static ModuleInfo GetTopLevelModule(this ModuleInfo module)
        {
            while (module.Parent != null)
            {
                module = module.Parent;
            }

            return module;
        }

        /// <summary>
        /// Converts to concurrent.
        /// </summary>
        /// <typeparam name="TKey">Generic Key.</typeparam>
        /// <typeparam name="TValue">Generic Value.</typeparam>
        /// <param name="dict">IEnumearble dictionary.</param>
        /// <returns>A converted <see cref="IEnumerable{T}"/> to <see cref="ConcurrentDictionary{TKey, TValue}"/>.</returns>
        public static ConcurrentDictionary<TKey, TValue> ToConcurrent<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> dict)
            => new ConcurrentDictionary<TKey, TValue>(dict);

        /// <summary>
        /// Load all services.
        /// </summary>
        /// <param name="collection">The Service collection used to add services.</param>
        /// <param name="assembly">The assembly with types to load.</param>
        /// <returns>An IEnumerable with all loaded services.</returns>
        public static IEnumerable<Type> LoadFrom(this IServiceCollection collection, Assembly assembly)
        {
            // list of all the types which are added with this method
            List<Type> addedTypes = new List<Type>();

            Type[] allTypes;
            try
            {
                // first, get all types in te assemblies
                allTypes = assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                Log.Warn(ex);
                return Enumerable.Empty<Type>();
            }

            // all types which have INService implementation are services
            // which are supposed to be loaded with this method
            // ignore all interfaces and abstract classes
            var services = new Queue<Type>(allTypes
                    .Where(x => x.GetInterfaces().Contains(typeof(INService))
                        && !x.GetTypeInfo().IsInterface && !x.GetTypeInfo().IsAbstract)
                    .ToArray());

            // we will just return those types when we're done instantiating them
            addedTypes.AddRange(services);

            // get all interfaces which inherit from INService
            // as we need to also add a service for each one of interfaces
            // so that DI works for them too
            var interfaces = new HashSet<Type>(allTypes
                    .Where(x => x.GetInterfaces().Contains(typeof(INService))
                        && x.GetTypeInfo().IsInterface));

            // keep instantiating until we've instantiated them all
            while (services.Count > 0)
            {
                // Get a type
                var serviceType = services.Dequeue();

                // If that type is already added, skip.
                if (collection.FirstOrDefault(x => x.ServiceType == serviceType) != null)
                {
                    continue;
                }

                // Also add the same type.
                var interfaceType = interfaces.FirstOrDefault(x => serviceType.GetInterfaces().Contains(x));
                if (interfaceType != null)
                {
                    addedTypes.Add(interfaceType);
                    collection.AddSingleton(interfaceType, serviceType);
                }
                else
                {
                    collection.AddSingleton(serviceType, serviceType);
                }
            }

            return addedTypes;
        }

        /// <summary>
        /// Extenstion to Reaction event wrapper.
        /// </summary>
        /// <param name="msg">The user message.</param>
        /// <param name="client">The discord client.</param>
        /// <param name="reactionAdded">Reaction added func.</param>
        /// <param name="reactionRemoved">Reaction removed func.</param>
        /// <returns>A reaction event wrapper including a reaction event.</returns>
        public static ReactionEventWrapper OnReaction(this IUserMessage msg, DiscordSocketClient client, Func<SocketReaction, Task> reactionAdded, Func<SocketReaction, Task> reactionRemoved = null)
        {
            if (reactionRemoved == null)
            {
                reactionRemoved = _ => Task.CompletedTask;
            }

            var wrap = new ReactionEventWrapper(client, msg);
            wrap.OnReactionAdded += (r) => { Task.Run(() => reactionAdded(r)); };
            wrap.OnReactionRemoved += (r) => { Task.Run(() => reactionRemoved(r)); };
            return wrap;
        }
    }
}
