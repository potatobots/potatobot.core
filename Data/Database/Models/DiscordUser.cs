﻿namespace PotatoBot.Core.Data.Database.Models
{
    /// <summary>
    /// A Model for discord users.
    /// </summary>
    public class DiscordUser : DbEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DiscordUser"/> class.
        /// </summary>
        public DiscordUser()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscordUser"/> class.
        /// </summary>
        /// <param name="userId">The id of the discord user.</param>
        /// <param name="userName">The username of the discord user.</param>
        /// <param name="discriminator">The discriminator of the discord user.</param>
        /// <param name="avatarId">The id of the avatar of the discord user.</param>
        public DiscordUser(ulong userId, string userName, string discriminator, string avatarId)
        {
            this.UserId = userId;
            this.Username = userName;
            this.Discriminator = discriminator;
            this.AvatarId = avatarId;
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public ulong UserId { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the discriminator.
        /// </summary>
        public string Discriminator { get; set; }

        /// <summary>
        /// Gets or sets the avatar id.
        /// </summary>
        public string AvatarId { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is DiscordUser du && du.UserId == this.UserId;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.UserId.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString() =>
            this.Username + "#" + this.Discriminator;
    }
}