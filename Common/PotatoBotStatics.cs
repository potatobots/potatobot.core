﻿namespace PotatoBot.Core.Common
{
    using Discord;

    /// <summary>
    /// A static class providing global static properties.
    /// </summary>
    public static class PotatoBotStatics
    {
        /// <summary>
        /// Gets or sets the Ok color.
        /// </summary>
        public static Color OkColor { get; set; }

        /// <summary>
        /// Gets or sets the Error color.
        /// </summary>
        public static Color ErrorColor { get; set; }
    }
}
