﻿namespace PotatoBot.Core.Modules.Common.Replacement
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using PotatoBot.Core.Common.CREmbed;

    /// <summary>
    /// A replacer class.
    /// </summary>
    public class Replacer
    {
        private readonly IEnumerable<(string Key, Func<string> Text)> replacements;
        private readonly IEnumerable<(Regex Regex, Func<Match, string> Replacement)> regex;

        /// <summary>
        /// Initializes a new instance of the <see cref="Replacer"/> class.
        /// </summary>
        /// <param name="replacements">Strings needed for the replacement.</param>
        /// <param name="regex">Regular expression for the replacement.</param>
        public Replacer(IEnumerable<(string, Func<string>)> replacements, IEnumerable<(Regex, Func<Match, string>)> regex)
        {
            this.replacements = replacements;
            this.regex = regex;
        }

        /// <summary>
        /// Replace the string input.
        /// </summary>
        /// <param name="input">The input string that needs replacement.</param>
        /// <returns>The string with the replaced input.</returns>
        public string Replace(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return input;
            }

            foreach (var (key, text) in this.replacements)
            {
                if (input.Contains(key))
                {
                    input = input.Replace(key, text(), StringComparison.InvariantCulture);
                }
            }

            foreach (var item in this.regex)
            {
                input = item.Regex.Replace(input, (m) => item.Replacement(m));
            }

            return input;
        }

        /// <summary>
        /// Replaces data from <see cref="CREmbed"/>.
        /// </summary>
        /// <param name="embedData">The embed data that needs to be replaced.</param>
        public void Replace(CREmbed embedData)
        {
            embedData.PlainText = this.Replace(embedData.PlainText);
            embedData.Description = this.Replace(embedData.Description);
            embedData.Title = this.Replace(embedData.Title);
            embedData.Thumbnail = this.Replace(embedData.Thumbnail);
            embedData.Image = this.Replace(embedData.Image);
            if (embedData.Author != null)
            {
                embedData.Author.Name = this.Replace(embedData.Author.Name);
                embedData.Author.IconUrl = this.Replace(embedData.Author.IconUrl);
            }

            if (embedData.Fields != null)
            {
                foreach (var f in embedData.Fields)
                {
                    f.Name = this.Replace(f.Name);
                    f.Value = this.Replace(f.Value);
                }
            }

            if (embedData.Footer != null)
            {
                embedData.Footer.Text = this.Replace(embedData.Footer.Text);
                embedData.Footer.IconUrl = this.Replace(embedData.Footer.IconUrl);
            }
        }
    }
}
