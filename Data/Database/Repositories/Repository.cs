﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// A Class for general repository purposes.
    /// </summary>
    /// <typeparam name="T">General type for repositories.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : DbEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context">The database context to use with the repositories.</param>
        protected Repository(DbContext context)
        {
            this.Context = context;
            this.Set = context.Set<T>();
        }

        /// <summary>
        /// Gets or sets the database context.
        /// </summary>
        protected DbContext Context { get; set; }

        /// <summary>
        /// Gets or sets the database set.
        /// </summary>
        protected DbSet<T> Set { get; set; }

        /// <inheritdoc/>
        public void Add(T obj) =>
            this.Set.Add(obj);

        /// <inheritdoc/>
        public void AddRange(params T[] objList) =>
            this.Set.AddRange(objList);

        /// <inheritdoc/>
        public T GetById(int id) =>
            this.Set.FirstOrDefault(e => e.Id == id);

        /// <inheritdoc/>
        public IEnumerable<T> GetAll() =>
            this.Set.ToList();

        /// <inheritdoc/>
        public void Remove(int id) =>
            this.Set.Remove(this.GetById(id));

        /// <inheritdoc/>
        public void Remove(T obj) =>
            this.Set.Remove(obj);

        /// <inheritdoc/>
        public void RemoveRange(params T[] objList) =>
            this.Set.RemoveRange(objList);

        /// <inheritdoc/>
        public void Update(T obj) =>
            this.Set.Update(obj);

        /// <inheritdoc/>
        public void UpdateRange(params T[] objList) =>
            this.Set.UpdateRange(objList);
    }
}
