﻿namespace PotatoBot.Core.Data.Database.Models
{
    using System;
    using System.Collections.Generic;
    using PotatoBot.Core.Common.Collections;

    /// <summary>
    /// Enum telling the update check type.
    /// </summary>
    public enum UpdateCheckType
    {
        /// <summary>
        /// Indicates a release.
        /// </summary>
        Release,

        /// <summary>
        /// Indicates a commit.
        /// </summary>
        Commit,

        /// <summary>
        /// Indicates nothing.
        /// </summary>
        None,
    }

    /// <summary>
    /// Enum indicating console output types.
    /// </summary>
    public enum ConsoleOutputType
    {
        /// <summary>
        /// Indicates a normal output.
        /// </summary>
        Normal,

        /// <summary>
        /// Indicates a simple output.
        /// </summary>
        Simple,
    }

    /// <summary>
    /// A model for bot configs.
    /// </summary>
    public class BotConfig : DbEntity
    {
        /// <summary>
        /// Gets or sets the blacklist.
        /// </summary>
        public virtual HashSet<BlacklistItem> Blacklist { get; set; }

        /// <summary>
        /// Gets or sets the buffer size.
        /// </summary>
        public ulong BufferSize { get; set; } = 4000000;

        /// <summary>
        /// Gets or sets a list containing playing statuses.
        /// </summary>
        public virtual List<PlayingStatus> RotatingStatusMessages { get; set; } = new List<PlayingStatus>();

        /// <summary>
        /// Gets or sets a value indicating whether a status should rotate.
        /// </summary>
        public bool RotatingStatuses { get; set; } = false;

        /// <summary>
        /// Gets or sets the DM helping string.
        /// </summary>
        public string DmHelpString { get; set; } = "Type `.h` for help.";

        /// <summary>
        /// Gets or sets the help string.
        /// </summary>
        public string HelpString { get; set; } = @"To add me to your server, use this link -> <https://discordapp.com/oauth2/authorize?client_id={0}&scope=bot&permissions=8>
You can use `{1}modules` command to see a list of all modules.
You can use `{1}commands ModuleName` to see a list of all of the commands in that module.
(for example `{1}commands Admin`) 
For a specific command help, use `{1}h CommandName` (for example {1}h {1}q)";

        /// <summary>
        /// Gets or sets the migration version.
        /// </summary>
        public int MigrationVersion { get; set; }

        /// <summary>
        /// Gets or sets the ok color.
        /// </summary>
        public string OkColor { get; set; } = "00e584";

        /// <summary>
        /// Gets or sets the error color.
        /// </summary>
        public string ErrorColor { get; set; } = "ee281f";

        /// <summary>
        /// Gets or sets the locale.
        /// </summary>
        public string Locale { get; set; } = null;

        /// <summary>
        /// Gets or sets a list of startup commands.
        /// </summary>
        public virtual IndexedCollection<StartupCommand> StartupCommands { get; set; }

        /// <summary>
        /// Gets or sets a list containing blocked commands.
        /// </summary>
        public virtual HashSet<BlockedCmdOrMdl> BlockedCommands { get; set; }

        /// <summary>
        /// Gets or sets a list containing blocked modules.
        /// </summary>
        public virtual HashSet<BlockedCmdOrMdl> BlockedModules { get; set; }

        /// <summary>
        /// Gets or sets the permission version.
        /// </summary>
        public int PermissionVersion { get; set; } = 2;

        /// <summary>
        /// Gets or sets the default prefix.
        /// </summary>
        public string DefaultPrefix { get; set; } = ".";

        /// <summary>
        /// Gets or sets the console output type.
        /// </summary>
        public ConsoleOutputType ConsoleOutputType { get; set; } = ConsoleOutputType.Normal;

        /// <summary>
        /// Gets or sets the update string.
        /// </summary>
        public string UpdateString { get; set; } = "New update has been released.";

        /// <summary>
        /// Gets or sets the update check type.
        /// </summary>
        public UpdateCheckType CheckForUpdates { get; set; } = UpdateCheckType.Release;

        /// <summary>
        /// Gets or sets the last update time.
        /// </summary>
        public DateTime LastUpdate { get; set; } = new DateTime(2018, 5, 5, 0, 0, 0, DateTimeKind.Utc);
    }
}
