﻿namespace PotatoBot.Core.Common.CREmbed
{
    using System;
    using Discord;
    using Newtonsoft.Json;
    using PotatoBot.Core.Common.Extensions;

    /// <summary>
    /// A class representing a CR embed.
    /// </summary>
    public class CREmbed
    {
        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public CREmbedAuthor Author { get; set; }

        /// <summary>
        /// Gets or sets the embed plain text.
        /// </summary>
        public string PlainText { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the embed footer.
        /// </summary>
        public CREmbedFooter Footer { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail.
        /// </summary>
        public string Thumbnail { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the fields.
        /// </summary>
        public CREmbedField[] Fields { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        public uint Color { get; set; } = 7458112;

        /// <summary>
        /// Gets a value indicating whether the embed is valid.
        /// </summary>
        public bool IsValid =>
            !string.IsNullOrWhiteSpace(this.Title) ||
            !string.IsNullOrWhiteSpace(this.Description) ||
            !string.IsNullOrWhiteSpace(this.Url) ||
            !string.IsNullOrWhiteSpace(this.Thumbnail) ||
            !string.IsNullOrWhiteSpace(this.Image) ||
            (this.Footer != null && (!string.IsNullOrWhiteSpace(this.Footer.Text) || !string.IsNullOrWhiteSpace(this.Footer.IconUrl))) ||
            (this.Fields != null && this.Fields.Length > 0);

        /// <summary>
        /// Try parse the input and return an embed.
        /// </summary>
        /// <param name="input">The input that needs to be parsed.</param>
        /// <param name="embed">The embed that is returned after the parse.</param>
        /// <returns>A boolean indicating if the parse was successful or not.</returns>
        public static bool TryParse(string input, out CREmbed embed)
        {
            embed = null;
            if (string.IsNullOrWhiteSpace(input) || !input.Trim().StartsWith('{'))
            {
                return false;
            }

            try
            {
                var crembed = JsonConvert.DeserializeObject<CREmbed>(input);

                if (crembed.Fields != null && crembed.Fields.Length > 0)
                {
                    foreach (var f in crembed.Fields)
                    {
                        f.Name = f.Name.TrimTo(256);
                        f.Value = f.Value.TrimTo(1024);
                    }
                }

                if (!crembed.IsValid)
                {
                    return false;
                }

                embed = crembed;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Converts the <see cref="CREmbed"/> to a <see cref="EmbedBuilder"/>.
        /// </summary>
        /// <returns>The converted <see cref="EmbedBuilder"/>.</returns>
        public EmbedBuilder ToEmbed()
        {
            var embed = new EmbedBuilder();

            if (!string.IsNullOrWhiteSpace(this.Title))
            {
                embed.WithTitle(this.Title);
            }

            if (!string.IsNullOrWhiteSpace(this.Description))
            {
                embed.WithDescription(this.Description);
            }

            if (this.Url != null && Uri.IsWellFormedUriString(this.Url, UriKind.Absolute))
            {
                embed.WithUrl(this.Url);
            }

            embed.WithColor(new Color(this.Color));
            if (this.Footer != null)
            {
                embed.WithFooter(efb =>
                {
                    efb.WithText(this.Footer.Text);
                    if (Uri.IsWellFormedUriString(this.Footer.IconUrl, UriKind.Absolute))
                    {
                        efb.WithIconUrl(this.Footer.IconUrl);
                    }
                });
            }

            if (this.Thumbnail != null && Uri.IsWellFormedUriString(this.Thumbnail, UriKind.Absolute))
            {
                embed.WithThumbnailUrl(this.Thumbnail);
            }

            if (this.Image != null && Uri.IsWellFormedUriString(this.Image, UriKind.Absolute))
            {
                embed.WithImageUrl(this.Image);
            }

            if (this.Author != null && !string.IsNullOrWhiteSpace(this.Author.Name))
            {
                if (!Uri.IsWellFormedUriString(this.Author.IconUrl, UriKind.Absolute))
                {
                    this.Author.IconUrl = null;
                }

                if (!Uri.IsWellFormedUriString(this.Author.Url, UriKind.Absolute))
                {
                    this.Author.Url = null;
                }

                embed.WithAuthor(this.Author.Name, this.Author.IconUrl, this.Author.Url);
            }

            if (this.Fields != null)
            {
                foreach (var f in this.Fields)
                {
                    if (!string.IsNullOrWhiteSpace(f.Name) && !string.IsNullOrWhiteSpace(f.Value))
                    {
                        embed.AddField(efb => efb.WithName(f.Name).WithValue(f.Value).WithIsInline(f.Inline));
                    }
                }
            }

            return embed;
        }
    }
}
