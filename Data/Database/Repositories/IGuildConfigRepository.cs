﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Interface for the Guild config repo.
    /// </summary>
    public interface IGuildConfigRepository : IRepository<GuildConfig>
    {
        /// <summary>
        /// Get config identified by id.
        /// </summary>
        /// <param name="guildId">The guild id of the config that needs to be found.</param>
        /// <param name="includes">List of <see cref="GuildConfig"/> that needs to be included.</param>
        /// <returns>A <see cref="GuildConfig"/> identified by <paramref name="guildId"/>.</returns>
        GuildConfig ForId(ulong guildId, Func<DbSet<GuildConfig>, IQueryable<GuildConfig>> includes = null);

        /// <summary>
        /// Get a list with all Guild configs.
        /// </summary>
        /// <param name="availableGuilds">A list with available guilds.</param>
        /// <returns>A list with Guild configs.</returns>
        IEnumerable<GuildConfig> GetAllGuildConfigs(List<ulong> availableGuilds);

        /// <summary>
        /// Get a list with all permissions.
        /// </summary>
        /// <param name="include">include ids.</param>
        /// <returns>A list containing guild id's and permissions.</returns>
        IEnumerable<GuildConfig> PermissionsForAll(List<ulong> include);

        /// <summary>
        /// Gets a <see cref="GuildConfig"/> with permissions for a guild.
        /// </summary>
        /// <param name="guildId">The guild id.</param>
        /// <returns>A <see cref="GuildConfig"/> corresponding to the guild id.</returns>
        GuildConfig GcWithPermissionsFor(ulong guildId);
    }

    /// <summary>
    /// Class representing a generating channel.
    /// </summary>
    public class GeneratingChannel
    {
        /// <summary>
        /// Gets or sets the guild id.
        /// </summary>
        public ulong GuildId { get; set; }

        /// <summary>
        /// Gets or sets the channel id.
        /// </summary>
        public ulong ChannelId { get; set; }
    }
}
