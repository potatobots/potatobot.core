﻿namespace PotatoBot.Core.Modules.Administration
{
    using PotatoBot.Core.Modules.Administration.Services;

    /// <summary>
    /// Main Adminsitration class part for commands.
    /// </summary>
    public partial class Administration : Module<AdministrationService>
    {
    }
}
