﻿namespace PotatoBot.Core.Data.Database.Repositories
{
    using Discord;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Interface for the discord user repository.
    /// </summary>
    public interface IDiscordUserRepository : IRepository<DiscordUser>
    {
        /// <summary>
        /// Function to ensure the user is created.
        /// </summary>
        /// <param name="userId">The id of the discord user.</param>
        /// <param name="username">The username of the discord user.</param>
        /// <param name="discriminator">The discriminator of the discord user.</param>
        /// <param name="avatarId">The id of the avatar of the discord user.</param>
        void EnsureCreated(ulong userId, string username, string discriminator, string avatarId);

        /// <summary>
        /// Gets the discord user. If the user is not present, the user will be created.
        /// </summary>
        /// <param name="userId">The id of the discord user.</param>
        /// <param name="username">The username of the discord user.</param>
        /// <param name="discriminator">The discriminator of the discord user.</param>
        /// <param name="avatarId">The id of the avatar of the discord user.</param>
        /// <returns>A <see cref="DiscordUser"/>.</returns>
        DiscordUser GetOrCreate(ulong userId, string username, string discriminator, string avatarId);

        /// <summary>
        /// Gets or creates a user from the given <see cref="IUser"/>.
        /// </summary>
        /// <param name="original">The user interface used to retrieve the <see cref="DiscordUser"/>.</param>
        /// <returns>A <see cref="DiscordUser"/>.</returns>
        DiscordUser GetOrCreate(IUser original);
    }
}
