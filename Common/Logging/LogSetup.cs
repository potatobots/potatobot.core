﻿namespace PotatoBot.Core.Common.Logging
{
    using NLog;
    using NLog.Config;
    using NLog.Targets;

    /// <summary>
    /// A Class for logging setup.
    /// </summary>
    public static class LogSetup
    {
        /// <summary>
        /// Setup the logger.
        /// </summary>
        /// <param name="shardId">Shard ID.</param>
        public static void SetupLogger(int shardId)
        {
            var logConfig = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget()
            {
                Layout = shardId + @" ${date:format=HH\:mm\:ss} ${logger:shortName=True} | ${message}",
            };

            logConfig.AddTarget("Console", consoleTarget);

            logConfig.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, consoleTarget));

            LogManager.Configuration = logConfig;
        }
    }
}
