﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;
    using System.Threading.Tasks;
    using Discord.Commands;
    using Microsoft.Extensions.DependencyInjection;
    using PotatoBot.Core.Common.Configs.Credentials;

    /// <summary>
    /// Attribute for owner only.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public sealed class OwnerOnlyAttribute : PreconditionAttribute
    {
        /// <inheritdoc/>
        public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo executingCommand, IServiceProvider services)
        {
            var creds = services.GetService<IBotCredentials>();

            return Task.FromResult(creds.IsOwner(context.User) || context.Client.CurrentUser.Id == context.User.Id ? PreconditionResult.FromSuccess() : PreconditionResult.FromError("Not owner"));
        }
    }
}
