﻿namespace PotatoBot.Core.Common
{
    /// <summary>
    /// Interface for command options.
    /// </summary>
    public interface IPotatoBotCommandOptions
    {
        /// <summary>
        /// Function to normalize the command options.
        /// </summary>
        void NormalizeOptions();
    }
}
