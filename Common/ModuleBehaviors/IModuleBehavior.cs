﻿namespace PotatoBot.Core.Common.ModuleBehaviors
{
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public interface IModuleBehavior
    {
        /// <summary>
        /// Gets the priority.
        /// Negative priority means it will try to apply as early as possible,
        /// Positive priority menas it will try to apply as late as possible.
        /// </summary>
        int Priority { get; }

        Task<ModuleBehaviorResult> ApplyBehavior(DiscordSocketClient client, IGuild guild, IUserMessage msg);
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public struct ModuleBehaviorResult
    {
        public bool Blocked { get; set; }

        public string NewInput { get; set; }

        public static ModuleBehaviorResult None() => new ModuleBehaviorResult
        {
            Blocked = false,
            NewInput = null,
        };

        public static ModuleBehaviorResult FromBlocked(bool blocked) => new ModuleBehaviorResult
        {
            Blocked = blocked,
            NewInput = null,
        };
    }
}
