﻿namespace PotatoBot.Core.Common.CREmbed
{
    /// <summary>
    /// A class that represents a Embed field.
    /// </summary>
    public class CREmbedField
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the embed should be inline or not.
        /// </summary>
        public bool Inline { get; set; }
    }
}
