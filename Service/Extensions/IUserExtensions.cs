﻿namespace PotatoBot.Core.Service.Extensions
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Discord;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// Extension method for <see cref="IUser"/>.
    /// </summary>
    public static class IUserExtensions
    {
        /// <summary>
        /// Extension to send confirmation async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="text">The confirmation text.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendConfirmAsync(this IUser user, string text)
             => await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendMessageAsync(string.Empty, embed: new EmbedBuilder().WithOkColor().WithDescription(text).Build()).ConfigureAwait(false);

        /// <summary>
        /// Extenstion to send confirmation async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="title">The title of the message.</param>
        /// <param name="text">The message itself.</param>
        /// <param name="url">An URL to add to the embed.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendConfirmAsync(this IUser user, string title, string text, string url = null)
        {
            var eb = new EmbedBuilder().WithOkColor().WithDescription(text).WithTitle(title);
            if (url != null && Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                eb.WithUrl(url);
            }

            return await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendMessageAsync(string.Empty, embed: eb.Build()).ConfigureAwait(false);
        }

        /// <summary>
        /// Extension to the send error message async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="title">The title of the message.</param>
        /// <param name="error">The error message itself.</param>
        /// <param name="url">An URL to add to the embed.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendErrorAsync(this IUser user, string title, string error, string url = null)
        {
            var eb = new EmbedBuilder().WithErrorColor().WithDescription(error).WithTitle(title);
            if (url != null && Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                eb.WithUrl(url);
            }

            return await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendMessageAsync(string.Empty, embed: eb.Build()).ConfigureAwait(false);
        }

        /// <summary>
        /// Extension to the send error message async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="error">The error message itself.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendErrorAsync(this IUser user, string error)
             => await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendMessageAsync(string.Empty, embed: new EmbedBuilder().WithErrorColor().WithDescription(error).Build()).ConfigureAwait(false);

        /// <summary>
        /// Send a file async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="filePath">The path to the file.</param>
        /// <param name="caption">The caption of the file.</param>
        /// <param name="text">The text added to the message.</param>
        /// <param name="isTTS">Boolean indicating if the message should be text to speach.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendFileAsync(this IUser user, string filePath, string caption = null, string text = null, bool isTTS = false)
        {
            using (var file = File.Open(filePath, FileMode.Open))
            {
                return await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendFileAsync(file, caption ?? "x", text, isTTS).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Send a file async.
        /// </summary>
        /// <param name="user">The discord user.</param>
        /// <param name="fileStream">The file stream.</param>
        /// <param name="fileName">The file name.</param>
        /// <param name="caption">The caption of the file.</param>
        /// <param name="isTTS">Boolean indicating if the message should be text to speach.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<IUserMessage> SendFileAsync(this IUser user, Stream fileStream, string fileName, string caption = null, bool isTTS = false) =>
            await (await user.GetOrCreateDMChannelAsync().ConfigureAwait(false)).SendFileAsync(fileStream, fileName, caption, isTTS).ConfigureAwait(false);

        /// <summary>
        /// Get the real avatar url.
        /// </summary>
        /// <param name="usr">The discord user the avatar needs is from.</param>
        /// <param name="size">The size of the avatar.</param>
        /// <returns>A <see cref="Uri"/> to the avatar.</returns>
        public static Uri RealAvatarUrl(this IUser usr, int size = 0)
        {
            var append = size <= 0
                ? string.Empty
                : $"?size={size}";

            return usr.AvatarId == null
                ? null
                : new Uri(usr.AvatarId.StartsWith("a_", StringComparison.InvariantCulture)
                    ? $"{DiscordConfig.CDNUrl}avatars/{usr.Id}/{usr.AvatarId}.gif" + append
                    : usr.GetAvatarUrl(ImageFormat.Auto) + append);
        }

        /// <summary>
        /// Get the real avatar url.
        /// </summary>
        /// <param name="usr">The discord database user the avatar needs is from.</param>
        /// <param name="size">The size of the avatar.</param>
        /// <returns>A <see cref="Uri"/> to the avatar.</returns>
        public static Uri RealAvatarUrl(this DiscordUser usr, int size = 0)
        {
            var append = size <= 0
                ? string.Empty
                : $"?size={size}";

            return usr.AvatarId == null
                ? null
                : new Uri(usr.AvatarId.StartsWith("a_", StringComparison.InvariantCulture)
                    ? $"{DiscordConfig.CDNUrl}avatars/{usr.UserId}/{usr.AvatarId}.gif" + append
                    : $"{DiscordConfig.CDNUrl}avatars/{usr.UserId}/{usr.AvatarId}.png" + append);
        }
    }
}
