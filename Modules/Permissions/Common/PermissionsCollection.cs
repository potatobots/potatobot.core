﻿namespace PotatoBot.Core.Modules.Permissions.Common
{
    using System;
    using System.Collections.Generic;
    using PotatoBot.Core.Common.Collections;
    using PotatoBot.Core.Data.Database.Models;

    /// <summary>
    /// New collection class designed for permissions.
    /// </summary>
    /// <typeparam name="T">The generic type which is used as source object for this collection.</typeparam>
    public class PermissionsCollection<T> : IndexedCollection<T>
        where T : class, IIndexed
    {
        private readonly object localLocker = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionsCollection{T}"/> class.
        /// </summary>
        /// <param name="source">The source list which is to be converted to a <see cref="PermissionsCollection{T}"/>.</param>
        public PermissionsCollection(IEnumerable<T> source)
            : base(source)
        {
        }

        /// <inheritdoc/>
        public override T this[int index]
        {
            get => this.Source[index];
            set
            {
                lock (this.localLocker)
                {
                    // can't set first element. It's always allow all
                    if (index == 0)
                    {
                        throw new IndexOutOfRangeException(nameof(index));
                    }

                    base[index] = value;
                }
            }
        }

        public static implicit operator List<T>(PermissionsCollection<T> x)
        {
            return x.Source;
        }

        /// <inheritdoc/>
        public override void Clear()
        {
            lock (this.localLocker)
            {
                var first = this.Source[0];
                base.Clear();
                this.Source[0] = first;
            }
        }

        /// <inheritdoc/>
        public override bool Remove(T item)
        {
            bool removed;
            lock (this.localLocker)
            {
                if (this.Source.IndexOf(item) == 0)
                {
                    throw new ArgumentException("You can't remove first permsission (allow all)");
                }

                removed = base.Remove(item);
            }

            return removed;
        }

        /// <inheritdoc/>
        public override void Insert(int index, T item)
        {
            lock (this.localLocker)
            {
                // can't insert on first place. Last item is always allow all.
                if (index == 0)
                {
                    throw new IndexOutOfRangeException(nameof(index));
                }

                base.Insert(index, item);
            }
        }

        /// <inheritdoc/>
        public override void RemoveAt(int index)
        {
            lock (this.localLocker)
            {
                // you can't remove first permission (allow all)
                if (index == 0)
                {
                    throw new IndexOutOfRangeException(nameof(index));
                }

                base.RemoveAt(index);
            }
        }
    }
}
