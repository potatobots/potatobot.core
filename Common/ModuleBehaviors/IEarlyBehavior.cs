﻿namespace PotatoBot.Core.Common.ModuleBehaviors
{
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1602:Enumeration items should be documented", Justification = "Don't know what this does. Copied.")]
    public enum ModuleBehaviorType
    {
        Blocker,
        Executor,
    }

    /// <summary>
    /// Implemented by modules which block execution before anything is executed.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "Don't know what this does. Copied.")]
    public interface IEarlyBehavior
    {
        int Priority { get; }

        ModuleBehaviorType BehaviorType { get; }

        Task<bool> RunBehavior(DiscordSocketClient client, IGuild guild, IUserMessage msg);
    }
}
