﻿namespace PotatoBot.Core.Modules.Help.Common
{
    using System;
    using System.Collections.Generic;
    using Discord.Commands;

    /// <summary>
    /// Helper class for comparing command texts.
    /// </summary>
    public class CommandTextEqualityComparer : IEqualityComparer<CommandInfo>
    {
        /// <inheritdoc/>
        public bool Equals(CommandInfo x, CommandInfo y) => x.Aliases[0] == y.Aliases[0];

        /// <inheritdoc/>
        public int GetHashCode(CommandInfo obj) => obj.Aliases[0].GetHashCode(StringComparison.InvariantCulture);
    }
}
