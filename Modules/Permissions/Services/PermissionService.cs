﻿namespace PotatoBot.Core.Modules.Permissions.Services
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;
    using Microsoft.EntityFrameworkCore;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Extensions;
    using PotatoBot.Core.Common.ModuleBehaviors;
    using PotatoBot.Core.Data.Database.Models;
    using PotatoBot.Core.Modules.Permissions.Common;
    using PotatoBot.Core.Service;
    using PotatoBot.Core.Service.Database;
    using PotatoBot.Core.Service.Extensions;

    /// <summary>
    /// A service class for permissions.
    /// </summary>
    public class PermissionService : ILateBlocker, INService
    {
        private readonly DbService db;
        private readonly CommandHandler cmd;
        private readonly PotatoBotStrings strings;

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionService"/> class.
        /// </summary>
        /// <param name="client">The discord bot client.</param>
        /// <param name="db">The database service.</param>
        /// <param name="cmd">The command handler.</param>
        /// <param name="strings">The potato bot strings.</param>
        public PermissionService(DiscordSocketClient client, DbService db, CommandHandler cmd, PotatoBotStrings strings)
        {
            this.db = db;
            this.cmd = cmd;
            this.strings = strings;

            using (var uow = this.db.GetDbContext())
            {
                foreach (var x in uow.GuildConfigs.PermissionsForAll(client.Guilds.ToArray().Select(x => x.Id).ToList()))
                {
                    this.Cache.TryAdd(x.GuildId, new PermissionCache()
                    {
                        Verbose = x.VerbosePermissions,
                        PermRole = x.PermissionRole,
                        Permissions = new PermissionsCollection<Permission>(x.Permissions),
                    });
                }
            }
        }

        /// <summary>
        /// Gets the permission cache.
        /// guildid, root permission.
        /// </summary>
        public ConcurrentDictionary<ulong, PermissionCache> Cache { get; } =
            new ConcurrentDictionary<ulong, PermissionCache>();

        /// <summary>
        /// Get a permission cache for a guild.
        /// </summary>
        /// <param name="guildId">The guild id.</param>
        /// <returns>A <see cref="PermissionCache"/> corresponding to the guild id.</returns>
        public PermissionCache GetCacheFor(ulong guildId)
        {
            if (!this.Cache.TryGetValue(guildId, out var pc))
            {
                using (var uow = this.db.GetDbContext())
                {
                    var config = uow.GuildConfigs.ForId(
                        guildId,
                        set => set.Include(x => x.Permissions));
                    this.UpdateCache(config);
                }

                this.Cache.TryGetValue(guildId, out pc);
                if (pc == null)
                {
                    throw new Exception("Cache is null.");
                }
            }

            return pc;
        }

        /// <summary>
        /// Add permissions to a guild.
        /// </summary>
        /// <param name="guildId">The guild id.</param>
        /// <param name="perms">The permissions to add to the guild.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task AddPermissions(ulong guildId, params Permission[] perms)
        {
            using (var uow = this.db.GetDbContext())
            {
                var config = uow.GuildConfigs.GcWithPermissionsFor(guildId);

                // var orderedPerms = new PermissionsCollection<Permissionv2>(config.Permissions);

                // have to set its index to be the highest
                var max = config.Permissions.Max(x => x.Index);
                foreach (var perm in perms)
                {
                    perm.Index = ++max;
                    config.Permissions.Add(perm);
                }

                await uow.SaveChangesAsync();
                this.UpdateCache(config);
            }
        }

        /// <summary>
        /// Update the cache.
        /// </summary>
        /// <param name="config">The <see cref="GuildConfig"/> used to update the cache.</param>
        public void UpdateCache(GuildConfig config)
        {
            this.Cache.AddOrUpdate(
                config.GuildId,
                new PermissionCache()
                {
                    Permissions = new PermissionsCollection<Permission>(config.Permissions),
                    PermRole = config.PermissionRole,
                    Verbose = config.VerbosePermissions,
                },
                (id, old) =>
            {
                old.Permissions = new PermissionsCollection<Permission>(config.Permissions);
                old.PermRole = config.PermissionRole;
                old.Verbose = config.VerbosePermissions;
                return old;
            });
        }

        /// <summary>
        /// Try block.
        /// </summary>
        /// <param name="client">The discord api client.</param>
        /// <param name="msg">The user message send in discord.</param>
        /// <param name="guild">The guild the message is send in.</param>
        /// <param name="channel">The channel the message is send in.</param>
        /// <param name="user">The discord user that send the message.</param>
        /// <param name="moduleName">The name of the module.</param>
        /// <param name="commandName">The name of the command.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<bool> TryBlockLate(DiscordSocketClient client, IUserMessage msg, IGuild guild, IMessageChannel channel, IUser user, string moduleName, string commandName)
        {
            await Task.Yield();
            if (guild == null)
            {
                return false;
            }
            else
            {
                var resetCommand = commandName == "resetperms";

                PermissionCache pc = this.GetCacheFor(guild.Id);
                if (!resetCommand && !pc.Permissions.CheckPermissions(msg, commandName, moduleName, out int index))
                {
                    if (pc.Verbose)
                    {
                        try
                        {
                            await channel.SendErrorAsync(
                                this.strings.GetText(
                                    "trigger",
                                    guild.Id,
                                    "Permissions".ToLowerInvariant(),
                                    index + 1,
                                    Format.Bold(pc.Permissions[index].GetCommand(
                                        this.cmd.GetPrefix(guild),
                                        (SocketGuild)guild))))
                                .ConfigureAwait(false);
                        }
                        catch
                        {
                        }
                    }

                    return true;
                }

                if (moduleName == nameof(Permissions))
                {
                    if (!(user is IGuildUser guildUser))
                    {
                        return true;
                    }

                    if (guildUser.GuildPermissions.Administrator)
                    {
                        return false;
                    }

                    var permRole = pc.PermRole;
                    if (!ulong.TryParse(permRole, out var rid))
                    {
                        rid = 0;
                    }

                    string returnMsg;
                    IRole role;
                    if (string.IsNullOrWhiteSpace(permRole) || (role = guild.GetRole(rid)) == null)
                    {
                        returnMsg = $"You need Admin permissions in order to use permission commands.";
                        if (pc.Verbose)
                        {
                            try
                            {
                                await channel.SendErrorAsync(returnMsg).ConfigureAwait(false);
                            }
                            catch
                            {
                            }
                        }

                        return true;
                    }
                    else if (!guildUser.RoleIds.Contains(rid))
                    {
                        returnMsg = $"You need the {Format.Bold(role.Name)} role in order to use permission commands.";
                        if (pc.Verbose)
                        {
                            try
                            {
                                await channel.SendErrorAsync(returnMsg).ConfigureAwait(false);
                            }
                            catch
                            {
                            }
                        }

                        return true;
                    }

                    return false;
                }
            }

            return false;
        }
    }
}
