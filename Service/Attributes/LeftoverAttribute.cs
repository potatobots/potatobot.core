﻿namespace PotatoBot.Core.Service.Attributes
{
    using Discord.Commands;

    /// <summary>
    /// Leftover attribute.
    /// </summary>
    public class LeftoverAttribute : RemainderAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeftoverAttribute"/> class.
        /// </summary>
        public LeftoverAttribute()
        {
        }
    }
}
