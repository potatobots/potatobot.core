﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;
    using Discord.Commands;

    /// <summary>
    /// Potato bot command attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PotatoBotCommandAttribute : CommandAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PotatoBotCommandAttribute"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        public PotatoBotCommandAttribute(string command)
            : base(command.Trim())
        {
        }
    }
}
