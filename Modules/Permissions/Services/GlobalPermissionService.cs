﻿namespace PotatoBot.Core.Modules.Permissions.Services
{
    using System.Linq;
    using System.Threading.Tasks;
    using Discord;
    using Discord.WebSocket;
    using PotatoBot.Core.Common;
    using PotatoBot.Core.Common.Collections;
    using PotatoBot.Core.Common.ModuleBehaviors;
    using PotatoBot.Core.Service;

    /// <summary>
    /// A service class for global permissions.
    /// </summary>
    public class GlobalPermissionService : ILateBlocker, INService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalPermissionService"/> class.
        /// </summary>
        /// <param name="bc">The bot config provider.</param>
        public GlobalPermissionService(IBotConfigProvider bc)
        {
            this.BlockedModules = new ConcurrentHashSet<string>(bc.BotConfig.BlockedModules.Select(x => x.Name));
            this.BlockedCommands = new ConcurrentHashSet<string>(bc.BotConfig.BlockedCommands.Select(x => x.Name));
        }

        /// <summary>
        /// Gets the blocked module list.
        /// </summary>
        public ConcurrentHashSet<string> BlockedModules { get; }

        /// <summary>
        /// Gets the blocked commands list.
        /// </summary>
        public ConcurrentHashSet<string> BlockedCommands { get; }

        /// <inheritdoc/>
        public async Task<bool> TryBlockLate(DiscordSocketClient client, IUserMessage msg, IGuild guild, IMessageChannel channel, IUser user, string moduleName, string commandName)
        {
            await Task.Yield();
            commandName = commandName.ToLowerInvariant();

            if (commandName != "resetglobalperms" &&
                (this.BlockedCommands.Contains(commandName) ||
                this.BlockedModules.Contains(moduleName.ToLowerInvariant())))
            {
                return true;
            }

            return false;
        }
    }
}
