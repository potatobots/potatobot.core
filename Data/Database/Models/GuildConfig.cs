﻿namespace PotatoBot.Core.Data.Database.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Class containing guild related configurations.
    /// </summary>
    public class GuildConfig : DbEntity
    {
        /// <summary>
        /// Gets or sets the guild id.
        /// </summary>
        public ulong GuildId { get; set; }

        /// <summary>
        /// Gets or sets the prefix.
        /// </summary>
        public string Prefix { get; set; } = null;

        /// <summary>
        /// Gets or sets the locale.
        /// </summary>
        public string Locale { get; set; } = null;

        /// <summary>
        /// Gets or sets the timezone Id.
        /// </summary>
        public string TimeZoneId { get; set; } = null;

        /// <summary>
        /// Gets or sets the root permission.
        /// </summary>
        [ForeignKey("PermissionId")]
        public virtual Permission RootPermission { get; set; } = null;

        /// <summary>
        /// Gets or sets the permission list.
        /// </summary>
        [ForeignKey("PermissionId")]
        public virtual List<Permission> Permissions { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether permissions are verbose.
        /// </summary>
        public bool VerbosePermissions { get; set; } = true;

        /// <summary>
        /// Gets or sets the permission role.
        /// </summary>
        public string PermissionRole { get; set; } = null;
    }
}