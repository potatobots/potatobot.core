﻿namespace PotatoBot.Core.Common.Configs.Credentials
{
    using System.Collections.Immutable;
    using Discord;
    using PotatoBot.Core.Common.Configs.Database;

    /// <summary>
    /// Interface for bot credentials.
    /// </summary>
    public interface IBotCredentials
    {
        /// <summary>
        /// Gets the Client Id.
        /// </summary>
        ulong ClientId { get; }

        /// <summary>
        /// Gets the bot Token.
        /// </summary>
        string Token { get; }

        /// <summary>
        /// Gets the owner ids list.
        /// </summary>
        ImmutableArray<ulong> OwnerIds { get; }

        /// <summary>
        /// Gets the database config.
        /// </summary>
        DbConfig Db { get; }

        /// <summary>
        /// Checks wether <see cref="IUser"/> is the owner.
        /// </summary>
        /// <param name="u">The user in question.</param>
        /// <returns>A boolean indicating if <paramref name="u"/> is the owner.</returns>
        bool IsOwner(IUser u);
    }
}
