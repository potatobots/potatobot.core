﻿namespace PotatoBot.Core.Common.Extensions
{
    using System;
    using System.Linq;

    /// <summary>
    /// Extension methods for <see cref="string"/>.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Trim a string to a certain length.
        /// </summary>
        /// <param name="str">The string that needs to be trimmed.</param>
        /// <param name="maxLength">The maximum length of the string.</param>
        /// <param name="hideDots">Indicator of dots should be hidden.</param>
        /// <returns>The trimmed string.</returns>
        public static string TrimTo(this string str, int maxLength, bool hideDots = false)
        {
            if (maxLength < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(maxLength), $"Argument {nameof(maxLength)} can't be negative.");
            }

            if (maxLength == 0)
            {
                return string.Empty;
            }

            if (maxLength <= 3)
            {
                return string.Concat(str.Select(c => '.'));
            }

            if (str.Length < maxLength)
            {
                return str;
            }

            if (hideDots)
            {
                return string.Concat(str.Take(maxLength));
            }
            else
            {
                return string.Concat(str.Take(maxLength - 3)) + "...";
            }
        }

        /// <summary>
        /// Sanitize mentions by replacing @everyone and @here.
        /// </summary>
        /// <param name="str">The string that will be replaced.</param>
        /// <returns>A sanitized mention.</returns>
        public static string SanitizeMentions(this string str) =>
            str.Replace("@everyone", "@everyοne", StringComparison.InvariantCultureIgnoreCase)
               .Replace("@here", "@һere", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Get initials from string.
        /// </summary>
        /// <param name="txt">The text to get the initials from.</param>
        /// <param name="glue">The separator.</param>
        /// <returns>The initials from the string.</returns>
        public static string GetInitials(this string txt, string glue = "") =>
            string.Join(glue, txt.Split(' ').Select(x => x.FirstOrDefault()));
    }
}
