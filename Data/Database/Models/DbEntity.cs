﻿namespace PotatoBot.Core.Data.Database.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Basic Database entity class.
    /// </summary>
    public class DbEntity
    {
        /// <summary>
        /// Gets or sets the id of the entity.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the date added property.
        /// </summary>
        public DateTime? DateAdded { get; set; } = DateTime.UtcNow;
    }
}
