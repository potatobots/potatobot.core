﻿namespace PotatoBot.Core.Modules.Help.Common
{
    using CommandLine;
    using PotatoBot.Core.Common;

    /// <summary>
    /// Class implementation of the <see cref="IPotatoBotCommandOptions"/>.
    /// </summary>
    public class CommandsOptions : IPotatoBotCommandOptions
    {
        /// <summary>
        /// Enum identifying the different view types.
        /// </summary>
        public enum ViewType
        {
            /// <summary>
            /// Hide.
            /// </summary>
            Hide,

            /// <summary>
            /// Cross.
            /// </summary>
            Cross,

            /// <summary>
            /// All.
            /// </summary>
            All,
        }

        /// <summary>
        /// Gets or sets the view type of the command.
        /// </summary>
        [Option('v', "view", Required = false, Default = ViewType.Hide, HelpText = "Specifies how to output the list of commands. 0 - Hide commands which you can't use, 1 - Cross out commands which you can't use, 2 - Show all.")]
        public ViewType View { get; set; } = ViewType.Hide;

        /// <inheritdoc/>
        public void NormalizeOptions()
        {
        }
    }
}
