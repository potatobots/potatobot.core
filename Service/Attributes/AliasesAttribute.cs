﻿namespace PotatoBot.Core.Service.Attributes
{
    using System;
    using System.Linq;
    using Discord.Commands;

    /// <summary>
    /// Aliases attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class AliasesAttribute : AliasAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AliasesAttribute"/> class.
        /// </summary>
        /// <param name="aliases">The aliases for the command.</param>
        public AliasesAttribute(string aliases)
            : base(FixAlliases(aliases).Skip(1).ToArray())
        {
        }

        /// <summary>
        /// Function to fix the aliases. Without the Skip(1), the bot command register doesn't work.
        /// Adding an empty string on index 0 to avoid this.
        /// </summary>
        /// <param name="aliases">List with aliases.</param>
        /// <returns>A list of fixed aliases.</returns>
        private static string[] FixAlliases(string aliases)
        {
            var list = aliases.Split(' ').ToList();
            list.Insert(0, string.Empty);
            return list.ToArray();
        }
    }
}
